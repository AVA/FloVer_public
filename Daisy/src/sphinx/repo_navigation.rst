.. _repo_navigation:

=========================
Navigating the Repository
=========================

Some of these are rather empty for now; the most interesting folders for
the average developer are listed in bold.

*benchmarking/*: code which benchmarks timing of a few functions written in C

*data/*: some experiments generate data and when this should persist, it belongs in here

*doc/*: documentation

*lib/*: jar files that Daisy requires

*library/*: definition of the 'Real' data type used in the specification language

*project/*: sbt configuration

*rawdata/*: folder where Daisy may spit out outputs (instead of polluting the home directory). In contrast to the data/ folder the files
in rawdata/ are not persistent.

*scripts/*: various bash scripts, used for random experiments

*src/bench/*: performance benchmarks using the ScalaMeter framework

**src/main/**: Daisy's source code

**src/sphinx/**: documentation

**src/test/**: unit tests using the ScalaTest framework

*target/*: sbt-generated folder with class files

**testcases/**: testcases, as the name says

