
=======
Testing
=======

Each function or collection of functions that provide a particular
functionality should be accompanied by at least a unit test.

These are located in src/test/scala/unit and are integrated with sbt with
ScalaTest.

To run all tests (in sbt interactive mode)::

  > test

All tests should pass, if not please report or fix.

To run a specific testcase::

  > test-only daisy.test.NameOfTest


------------
Benchmarking
------------

Daisy is set up to use Scalameter for benchmarking on the JVM. Scalameter
takes care of warming up the JVM and other mundane tasks.
You can run Scalameter benchmarks from src/test, however, then the benchmarking
will also be done each time someone calls > test. This is suboptimal,
because these benchmarking runs can potentially take a very long time.

To avoid this, the project has a configuration called 'bench' which allow
you to run Scalameter tests in the src/bench folder. (Note, by default,
the sbt 'test' command will only search the src/test folder.)

E.g. you can call
> bench:test
which will run all the benchmarks in src/bench.
To run only one benchmark:
> bench:test-only RangeBenchmark