
=========
Utilities
=========

Daisy provides a number of utilities which are useful in their own right.

AffineForm
----------
An implementation of affine arithmetic over rationals.

Interval
--------
A standard implementation of intervals over rationals.

MPFRFloat
---------
A Scala wrapper around the Java wrapper around the C MPFR arbitrary precision
library. (May require the MPFR library to be installed locally.)

Rational
--------
A straight-forward implementation of a rational data type on top of
Java's BigIntegers. The square root operation cannot be computed
exactly in rationals; our implementation provides a (hacky) approximation.

SMTRange
--------
A combination of interval arithmetic and SMT solving to compute tighter
ranges. Requires Z3 to be installed.
