Microbenchmarking
==========

For the case when you want to benchmark or compare code fragments wrt.
performance, you can use the ScalaMeter framework. It is set up separately
from the normal tests. The code goes in src/bench/scala and you can compile
and run the tests with bench:compile and bench:test respectively.