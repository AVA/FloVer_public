.. Daisy documentation master file, created by
   sphinx-quickstart on Wed Sep 23 14:57:41 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Daisy's documentation!
=================================

Contents:

.. toctree::
   :maxdepth: 2

   intro
   repo_navigation
   contributing
   testing
   internals
   utils
..   phases
..   code_optimizations
..   errors
..   dynamic_phase
..   troubleshooting


.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

