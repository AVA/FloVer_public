
=====
Intro
=====

Daisy is (or will be) a framework for verifying, approximating and
synthesizing numerical programs. Currently, it's more of a building site,
with holes everywhere, but with basic functionality already working.

Its goal is to provide a modular framework that allows quick and easy
experimenting with new techniques and ideas. To this end, it provides basic
infrastructure such as parsing and type checking of inputs as well as
basic utilities such as interval arithmetic or interfaces to solvers.

While performance is, of course, a consideration, the first priority is
ease of understanding, reasoning about and extending the code.

------------------
This Documentation
------------------

Daisy uses Sphinx for (currently mostly developer) documentation.
If you want to modify the docs, edit the files in src/sphinx and then
re-generate the the html files.
For this, you will need to install Sphinx; follow these instructions: http://www.sphinx-doc.org/en/stable/tutorial.html.
To build the html version of the documentation, navigate to src/sphinx/ and type::

  $ make html


--------------
Input Programs
--------------
Daisy accepts one input file which must follow the following basic structure::

  import daisy.lang._
  import Real._

  object TestObject {

    def function(x: Real): Real = {
      require(...)

      val z = x + x
      z * x
    }
  }

Input programs are written over a real-valued, not executable data type 'Real'
and consist of a top-level object and a number of function definitions.
Currently, each function is analyzed in isolation, but we hope to change
this in the future and instead allow for a designated 'main' method together
with a number of helper methods.
Function bodies may consist of arithmetic expressions, including square root,
and let-like immutable variable definitions ('val z = ').


-----------------
Daisy's Structure
-----------------
Daisy is intended to work in phases (as much as possible) - similarly to a compiler -
for modularity's sake. Currently, working phases are

* SpecsProcessingPhase: parse the 'require' clause
* RangeErrorPhase: compute ranges and absolute (roundoff) errors of all (intermediate) values
* InfoPhase: print out interesting results about the input program

There is more work in progress, and it will be added here, once it's more mature.

