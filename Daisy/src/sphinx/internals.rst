
===============
Daisy Internals
===============

---------------------------
Intermediate Representation
---------------------------

Daisy's AST is defined in lang/Trees.scala. The trees and their structure is heavily inspired
by Leon (or simply copied in many places). This has been done on purpose so that
Daisy can re-use much of Leon's functionality easily.

Trees are types, and you will find the type in lang/Types.scala. The objects TreeOps and TypeOps
contain a number of useful operations on trees and types, respectively, such as traversal,
size-of etc.

Each variable (which is also a tree node) has a unique (integer) identifier.

Certain (numerical) tree nodes extend the 'NumAnnotation' trait, which allows
the nodes to be annotated with a range and an absolute error. This annotation is
write-once, i.e. each subsequent annotation attempt will result in an exception being thrown.
The reason for using this impure way is mostly to allow for fixed-point code generation
without having to re-compute the range information. (For generating fixed-point code,
the range of values at each intermediate node has to be known.)


.. Adding a new Node
.. ^^^^^^^^^^^^^^^^^
.. If you want to add a new AST node, you have to do these steps:

.. 1:
.. if (real-leon can extract new node)
.. then
..   skip
.. else
..   modify real-leon to do the extraction
..   package real-leon and update the jar files in daisy/lib/

.. 2: add a new node in lang/Trees

.. 3: extract new node from real-leon in frontends/ScalaExtraction

.. 4: update lang/PrettyPrinter

.. 5: update Extractors and check the functions in lang/TreeOps

.. 6: if your tree node is/can be involved in a call to a solver, make sure
.. it's translation o SMTLib is supported (solvers/Solver.scala)



.. Modifying real-leon
.. ---------------------
.. If you want to add a new function to be parsed, say 'asin',
.. then usually you will need to modify
.. frontends.scalac.ASTExtractors
.. frontends.scalac.CodeExtraction
.. purescala.Extractors

.. Try to search for a tree already present, such as 'Sqrt'.

.. To test your modifications:
.. > run --real library/annotation/package.scala library/daisy/Real.scala testcases/RealTest.scala