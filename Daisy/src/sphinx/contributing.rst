
============
Contributing
============

Here is some (random) information that may help you if you want to extend or modify Daisy.

Scala Sequences
---------------
While most of Scala's data structures are immutable by default (i.e. when you do not import any
specific one), this is unfortunately not the case for the 'Seq' type.
Please ::

  import scala.collection.immutable.Seq

if you use Seq's. If you don't, you may get funny and unexpected results because you will
use the mutable version, or your code may simply not compile because of a mismatch
with the other immutable 'Seq's.


Creating a New Phase
--------------------
If you want to create a new phase, you may find the following template useful::

  package daisy
  package analysis

  // default Seq is mutable
  import scala.collection.immutable.Seq
  import lang.Trees.Program

  /**
    ??? Description goes here


    Prerequisites:
      - ...
   */
  object TemplatePhase extends DaisyPhase {

    override val name = ""
    override val description = ""
    override val definedOptions: Set[CmdLineOptionDef[Any]] = Set()

    implicit val debugSection = ???

    var reporter: Reporter = null

    override def run(ctx: Context, prg: Program): (Context, Program) = {
      reporter = ctx.reporter
      reporter.info(s"\nStarting $name")
      val timer = ctx.timers.???.start


      timer.stop
      ctx.reporter.info(s"Finished $name")
      (ctx, prg)
    }
  }