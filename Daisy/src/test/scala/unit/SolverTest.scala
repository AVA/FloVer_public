
package daisy
package test

import scala.collection.immutable.Seq

import lang.Trees._
import tools.Rational
import Rational._

import solvers.Z3Solver
import solvers.DRealSolver

class SolverTest extends UnitSuite {

  val context = Context(new DefaultReporter(Set()), Seq(), Seq())


  test("simple sat") {
    val constr = Equals(x, Plus(y, bigError))

    val solver = new Z3Solver(context)
    solver.assertConstraint(constr)
    val res = solver.checkSat
    assert(res == Some(true))
    solver.free()
  }

  test("simple unsat") {
    val constr = And(GreaterThan(x, RealLiteral(one)),
                      LessThan(Times(x, x), RealLiteral(zero)))

    val solver = new Z3Solver(context)
    solver.assertConstraint(constr)
    val res = solver.checkSat
    assert(res == Some(false))
    solver.free()
  }

  test("nlsat") {
    // x*x == 2  has to return sat for nlsat to be used

    val constr = Equals(Times(x, x), RealLiteral(two))

    val solver = new Z3Solver(context)
    solver.assertConstraint(constr)
    val res = solver.checkSat
    assert(res == Some(true))
    solver.free()

  }

  test("idempotent checks") {
    val solver = new Z3Solver(context)

    solver.assertConstraint(Equals(x, ten))

    solver.assertConstraint(Equals(x, five))
    val res = solver.checkSat
    assert(res == Some(false))

    val res2 = solver.checkSat
    assert(res2 == Some(false))

  }

  ignore("reset") {

    val solver = new Z3Solver(context)

    solver.assertConstraint(Equals(x, ten))

    solver.assertConstraint(Equals(x, five))
    val res = solver.checkSat
    assert(res == Some(false))

    // Unless we do a reset, the constraints remain asserted
    val res2 = solver.checkSat
    assert(res2 == Some(false))


    /*solver.reset
    solver.assertConstraint(Equals(x, five))
    val res3 = solver.checkSat
    assert(res3 == Some(true))
    */
  }



  test("extract simple model") {
    val constr1 = Equals(x, Plus(y, bigError))

    val solver = new Z3Solver(context)
    solver.assertConstraint(constr1)
    val res = solver.checkSat
    assert(res == Some(true))

    val model = solver.getModel
    assert(model.isDefinedAt(x.id))
    assert(model.isDefinedAt(y.id))
    assert(!model.isDefinedAt(z.id))
    // ask for sat
    // ask for model
  }




  ignore("dReal simple sat") {
    val constr = Equals(x, Plus(y, bigError))

    val solver = new DRealSolver(context)
    solver.writeLogic()
    solver.assertConstraint(constr)
    val res = solver.checkSat
    assert(res == Some(true))
    solver.free()
  }

  ignore("dReal simple unsat") {
    val constr = And(GreaterThan(x, RealLiteral(one)),
                      LessThan(Times(x, x), RealLiteral(zero)))

    val solver = new DRealSolver(context)
    solver.writeLogic()
    solver.assertConstraint(constr)
    val res = solver.checkSat
    assert(res == Some(false))
    solver.free()
  }
}
