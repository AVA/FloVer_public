package daisy
package test


import org.scalatest.FunSuite

import tools.{Rational}
import lang.Trees._
import lang.Identifiers._
import lang.Types.{RealType}
import lang.TreeOps.preTraversal
import lang.Trees._


abstract class UnitSuite extends FunSuite {
  // put here anything that can be re-used between tests
  val x = Variable(FreshIdentifier("x", RealType))
  val y = Variable(FreshIdentifier("y", RealType))
  val z = Variable(FreshIdentifier("z", RealType))
  val w = Variable(FreshIdentifier("w", RealType))

  val a = Variable(FreshIdentifier("a", RealType))
  val b = Variable(FreshIdentifier("b", RealType))
  val c = Variable(FreshIdentifier("c", RealType))


  val minFive = RealLiteral(Rational(-5))
  val five = RealLiteral(Rational(5))
  val minTen = RealLiteral(Rational(-10))
  val ten = RealLiteral(Rational(10))

  val bigError = RealLiteral(Rational(5, 10000))
  val smallError = RealLiteral(Rational(10, 1000000))


}