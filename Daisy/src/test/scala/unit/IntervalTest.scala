package daisy
package test

import tools.{Interval, Rational}


class IntervalTest extends UnitSuite {

  test("simple arithmetic") {

    val a = Interval(1, 3)
    val b = Interval(-2, 2)

    val res = (a + b) - (b + a)
    assert(res === Interval(-6, 6))

  }


  test("all arithmetic") {
    val a = Interval(-1, 1)
    val b = Interval(3, 4)
    val c = Interval(-2, 3)

    val res = ((a + b) - (c*c)) / b

    assert(res === Interval(Rational(-7, 3), Rational(11, 3)))

  }






}