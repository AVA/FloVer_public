package daisy
package test

import tools.{Interval, Rational, AffineForm}
import Rational._

class AffineArithmeticTest extends UnitSuite {

  test("foo") {

  }

  test("conversions") {
    val a = Interval(-1, 1)
    val b = Interval(3, 3)
    val c = Interval(2, 3)

    val aform1 = AffineForm(a)
    assert(aform1.toInterval === a)
    assert(aform1.noise.size === 1)

    val aform2 = AffineForm(b)
    assert(aform2.toInterval === b)
    assert(aform2.noise.size === 0)

    val aform3 = AffineForm(c)
    assert(aform3.toInterval === c)
    assert(aform3.noise.size === 1)

    val aform4 = AffineForm(Rational(5))
    assert(aform4.toInterval === Interval(5, 5))
    assert(aform4.noise.size === 0)
  }

  test("simple arithmetic") {

    val a = AffineForm(Interval(1, 3))
    val b = AffineForm(Interval(-2, 2))

    val e0 = a + b
    val e1 = (a + b) - (b + a)

    assert(e0.toInterval === Interval(-1, 5))

    assert(e1.toInterval === Interval(zero, zero))

  }

  test("multiplication") {
    val a = AffineForm(Interval(1, 3))
    val b = AffineForm(Interval(-2, 2))
    val c = AffineForm(Interval(5, 9))

    val e0 = a * a
    assert(e0.toInterval === Interval(0, 9))
    assert(e0.noise.size === 2)

    val e1 = a * b
    assert(e1.toInterval === Interval(-6, 6))
    assert(e1.noise.size === 2)

    val e2 = a * c
    assert(e2.toInterval === Interval(1, 27))
    assert(e2.noise.size === 3)

    val e3 = (a * c) * a
    assert(e3.toInterval === Interval(-18, 81))
    assert(e3.noise.size === 4)

  }


}