package daisy
package test.regression

import scala.collection.immutable.Seq
import org.scalatest.FunSuite
import java.io.FileWriter
import java.io.BufferedWriter
import scala.io.Source

import tools.{AffineForm, SMTRange, Interval}
import lang.Trees.Variable

// testing something that is not used...
class RangeRegressionTest extends FunSuite {

  val inputFiles: List[String] = List("src/test/resources/RangeRegressionFunctions.scala")

  val context = Context(
      reporter = new DefaultReporter(Set()), // no debug sections for test
      options = List(),
      files = inputFiles
    )

  //val inputPrg = frontend.ScalaExtraction.run(context)
  val inputPrg = frontend.ExtractionPhase(context)

  val fstream = new FileWriter("src/test/resources/range_regression_today.txt")
  val out = new BufferedWriter(fstream)
  var map = collection.immutable.Map[String, String]()

  for (fnc <- inputPrg.defs) if (!fnc.precondition.isEmpty && !fnc.body.isEmpty) {

    val (ranges, errors) = analysis.SpecsProcessingPhase.extractPreCondition(fnc.precondition.get)
    val inputRanges = ranges.map( x => (x._1 -> Interval(x._2._1, x._2._2)))

    val affineRange = tools.Evaluators.evalAffine(fnc.body.get,
      inputRanges.map(x => (x._1 -> AffineForm(x._2))))
    val intervalRange = tools.Evaluators.evalInterval(fnc.body.get,
      inputRanges)
    val smtRange = tools.Evaluators.evalSMT(fnc.body.get,
      inputRanges.map({ case (id, int) => (id -> SMTRange(Variable(id), int)) })).toInterval

    out.write(fnc.id + " " + intervalRange + " " + affineRange + " " + smtRange + "\n")

    map = map + (fnc.id.toString -> (intervalRange + " " + affineRange + " " + smtRange))
  }

  out.close



  test("foo") {
    var success = true

    // manual diff
    for (line <- Source.fromFile("src/test/resources/range_regression_baseline.txt").getLines()) {
      val (id, data) = line.splitAt(line.indexOf(" "))

      if (map(id) != data.tail) {  //tail because splitAt leaves an empty space at the beginning
        success = false
        context.reporter.warning("range regression found mismatch for " + id)
        context.reporter.warning("expected: " + data.tail)
        context.reporter.warning("found:    " + map(id))
      }

    }



    assert(success)

    // write a textfile with the results
    // => lets us compare the textfiles for regression,
    // and at the same time we can check the results
  }
}