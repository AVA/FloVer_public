package daisy
package test.regression

import scala.collection.immutable.Seq
import org.scalatest.FunSuite

import lang.Trees.{Expr, Let}

/**
  Regression test for the basic absolute error and range computations.

    Times taken on Eva's Linux desktop (before refactoring | after refactoring):
    int-affine-fixed32 425 413 438 | 361 365
    int-affine-float32 307 304 321 | 281 278
    int-affine-float64 593 590 603 | 505 509

    affine-affine-fixed32 453 434 461 | 382 382
    affine-affine-float32 329 334 347 | 288 285
    affine-affine-float64 625 635 650 | 527 522

    smt-affine-fixed32 47078 46967 47108 | 50006 49934
    smt-affine-float32 46235 46350 46273 | 49413 49287
    smt-affine-float64 46622 46559 46486 | 49481 49474

*/
class AbsErrorAnalysisRegressionTest extends FunSuite {

  val inputFile: String = "src/test/resources/AbsErrorRegressionFunctions.scala"

  val context = Context(
    reporter = new DefaultReporter(Set()),
    options = List(),
    files = List(inputFile)
    )

  val inputPrg = frontend.ExtractionPhase(context)
  val pipeline = analysis.SpecsProcessingPhase andThen analysis.RangeErrorPhase

  // Regression results
  // fnc.id -> (abs error, range)

  val intAA_Fixed32: Map[String, (String, String)] = Map(
    ("doppler", ("1.75791622644632e-06", "[-158.7191444098274, -0.02944244059231351]")),
    ("sine", ("5.172930285837377e-09", "[-2.3011348046703466, 2.3011348046703466]")),
    ("sineOrder3", ("7.483225706087375e-09", "[-2.9419084189651277, 2.9419084189651277]")),
    ("sqroot", ("4.015397284953641e-06", "[-402.125, 68.5]")),
    ("bspline0", ("8.537123605701497e-10", "[0.0, 0.16666666666666666]")),
    ("bspline1", ("3.337239226446336e-09", "[-0.3333333333333333, 1.1666666666666667]")),
    ("bspline2", ("3.104408582665976e-09", "[-0.3333333333333333, 1.1666666666666667]")),
    ("bspline3", ("6.208817165548793e-10", "[-0.16666666666666666, 0.0]")),
    ("rigidBody1", ("1.3485550881126018e-06", "[-705.0, 705.0]")),
    ("rigidBody2", ("0.0001531802118132919", "[-58740.0, 58740.0]")),
    ("verhulst", ("3.009066920607247e-09", "[0.3148936170212766, 1.1008264462809918]")),
    ("predatorPrey", ("1.647071564900709e-09", "[0.03727705922396188, 0.35710168263424846]")),
    ("carbonGas", ("17.69469347003061", "[2097409.2, 3.434323E7]")),
    ("turbine1", ("5.059999504388075e-07", "[-58.32912689020381, -1.5505285721480735]")),
    ("turbine2", ("6.241048750415222e-07", "[-29.43698909090909, 80.993]")),
    ("turbine3", ("3.6766172442271937e-07", "[0.4660958448753463, 40.375126890203816]")),
    ("kepler0", ("4.379078746518522e-07", "[-35.7792, 159.8176]")),
    ("kepler1", ("2.021807432895287e-06", "[-490.320768, 282.739712]")),
    ("kepler2", ("1.037595868540202e-05", "[-871.597824, 1860.323072]")),
    ("himmilbeau", ("9.75281000696604e-06", "[-1630.0, 3050.0]"))
  )

  val intAA_Float32: Map[String, (String, String)] = Map(
    ("doppler", ("0.0002250138141196529", "[-158.7191444098274, -0.02944244059231351]")),
    ("sine", ("6.064885588214318e-07", "[-2.3011348046703466, 2.3011348046703466]")),
    ("sineOrder3", ("7.790389913381473e-07", "[-2.9419084189651277, 2.9419084189651277]")),
    ("sqroot", ("0.00016796589915202306", "[-402.125, 68.5]")),
    ("bspline0", ("8.69234485871102e-08", "[0.0, 0.16666666666666666]")),
    ("bspline1", ("4.2716663302873797e-07", "[-0.3333333333333333, 1.1666666666666667]")),
    ("bspline2", ("3.973643085686263e-07", "[-0.3333333333333333, 1.1666666666666667]")),
    ("bspline3", ("5.712112027822517e-08", "[-0.16666666666666666, 0.0]")),
    ("rigidBody1", ("0.00017261505240639963", "[-705.0, 705.0]")),
    ("rigidBody2", ("0.01960706761337861", "[-58740.0, 58740.0]")),
    ("verhulst", ("3.199529347654797e-07", "[0.3148936170212766, 1.1008264462809918]")),
    ("predatorPrey", ("1.3291118406629398e-07", "[0.03727705922396188, 0.35710168263424846]")),
    ("carbonGas", ("22.995233482473232", "[2097409.2, 3.434323E7]")),
    ("turbine1", ("5.095029323813888e-05", "[-58.32912689020381, -1.5505285721480735]")),
    ("turbine2", ("7.46823932469574e-05", "[-29.43698909090909, 80.993]")),
    ("turbine3", ("3.797059491586537e-05", "[0.4660958448753463, 40.375126890203816]")),
    ("kepler0", ("5.605220905522401e-05", "[-35.7792, 159.8176]")),
    ("kepler1", ("0.0002587913631247376", "[-490.320768, 282.739712]")),
    ("kepler2", ("0.0013281227815583414", "[-871.597824, 1860.323072]")),
    ("himmilbeau", ("0.0012483597718073725", "[-1630.0, 3050.0]"))
  )
  val intAA_Float64: Map[String, (String, String)] = Map(
    ("doppler", ("4.1911988101104756e-13", "[-158.7191444098274, -0.02944244059231351]")),
    ("sine", ("1.1296729607621835e-15", "[-2.3011348046703466, 2.3011348046703466]")),
    ("sineOrder3", ("1.4510731312549944e-15", "[-2.9419084189651277, 2.9419084189651277]")),
    ("sqroot", ("3.1286084833936916e-13", "[-402.125, 68.5]")),
    ("bspline0", ("1.6190752442450204e-16", "[0.0, 0.16666666666666666]")),
    ("bspline1", ("7.956598343146956e-16", "[-0.3333333333333333, 1.1666666666666667]")),
    ("bspline2", ("7.401486830834377e-16", "[-0.3333333333333333, 1.1666666666666667]")),
    ("bspline3", ("1.0639637319324417e-16", "[-0.16666666666666666, 0.0]")),
    ("rigidBody1", ("3.2152058793144533e-13", "[-705.0, 705.0]")),
    ("rigidBody2", ("3.652100843964945e-11", "[-58740.0, 58740.0]")),
    ("verhulst", ("5.959587110472846e-16", "[0.3148936170212766, 1.1008264462809918]")),
    ("predatorPrey", ("2.4756633989832674e-16", "[0.03727705922396188, 0.35710168263424846]")),
    ("carbonGas", ("4.2831938128927477e-08", "[2097409.2, 3.434323E7]")),
    ("turbine1", ("9.490226544267962e-14", "[-58.32912689020381, -1.5505285721480735]")),
    ("turbine2", ("1.3910672706969435e-13", "[-29.43698909090909, 80.993]")),
    ("turbine3", ("7.072570725135768e-14", "[0.4660958448753463, 40.375126890203816]")),
    ("kepler0", ("1.0440537323574973e-13", "[-35.7792, 159.8176]")),
    ("kepler1", ("4.820364551960666e-13", "[-490.320768, 282.739712]")),
    ("kepler2", ("2.4738213255659507e-12", "[-871.597824, 1860.323072]")),
    ("himmilbeau", ("2.3252511027749283e-12", "[-1630.0, 3050.0]"))

  )

  val aaAA_Fixed32: Map[String, (String, String)] = Map(
    ("doppler", ("5.390735253968273e-06", "[-282.4762120545625, 133.53913086064713]")),
    ("sine", ("3.5841039195923494e-09", "[-1.6540002686363373, 1.6540002686363373]")),
    ("sineOrder3", ("6.536979105441602e-09", "[-1.909859317102744, 1.909859317102744]")),
    ("sqroot", ("4.004454244701909e-06", "[-365.875, 195.04296875]")),
    ("bspline0", ("8.537123605701497e-10", "[-0.0625, 0.16666666666666666]")),
    ("bspline1", ("3.162616243705934e-09", "[-0.08333333333333333, 0.9791666666666666]")),
    ("bspline2", ("2.8521753853742843e-09", "[-0.020833333333333332, 0.9166666666666666]")),
    ("bspline3", ("6.208817165548793e-10", "[-0.16666666666666666, 0.0625]")),
    ("rigidBody1", ("1.3485550881126018e-06", "[-705.0, 705.0]")),
    ("rigidBody2", ("0.0001531802118132919", "[-57390.0, 58740.0]")),
    ("verhulst", ("2.7762362769533775e-09", "[0.35217660784145, 0.9891620430819574]")),
    ("predatorPrey", ("1.6642254365978505e-09", "[-0.0021029110350001693, 0.34533842311741914]")),
    ("carbonGas", ("17.682124422310654", "[-6856426.768, 3.159230584E7]")),
    ("turbine1", ("5.106469765682726e-07", "[-55.55054339020381, 38.36885388122605]")),
    ("turbine2", ("6.326289514425209e-07", "[-80.18793076923077, 63.665174692307694]")),
    ("turbine3", ("3.7230875055215864e-07", "[-27.227529265841437, 38.719203390203816]")),
    ("kepler0", ("3.8016587502569254e-07", "[3.0664, 102.8708]")),
    ("kepler1", ("1.842993498569115e-06", "[-288.15184, 89.927712]")),
    ("kepler2", ("9.235736732009992e-06", "[-337.61856, 850.310096]")),
    ("himmilbeau", ("4.194676880030224e-06", "[-775.0, 890.0]"))
  )

  val aaAA_Float32: Map[String, (String, String)] = Map(
    ("doppler", ("0.0006900171033357679", "[-282.4762120545625, 133.53913086064713]")),
    ("sine", ("4.031187839420683e-07", "[-1.6540002686363373, 1.6540002686363373]")),
    ("sineOrder3", ("6.579194264554884e-07", "[-1.909859317102744, 1.909859317102744]")),
    ("sqroot", ("0.00016656518999980138", "[-365.875, 195.04296875]")),
    ("bspline0", ("8.69234485871102e-08", "[-0.0625, 0.16666666666666666]")),
    ("bspline1", ("4.048148912379665e-07", "[-0.08333333333333333, 0.9791666666666666]")),
    ("bspline2", ("3.6507845931528976e-07", "[-0.020833333333333332, 0.9166666666666666]")),
    ("bspline3", ("5.712112027822517e-08", "[-0.16666666666666666, 0.0625]")),
    ("rigidBody1", ("0.00017261505240639963", "[-705.0, 705.0]")),
    ("rigidBody2", ("0.01960706761337861", "[-57390.0, 58740.0]")),
    ("verhulst", ("2.901506123777844e-07", "[0.35217660784145, 0.9891620430819574]")),
    ("predatorPrey", ("1.342975706563773e-07", "[-0.0021029110350001693, 0.34533842311741914]")),
    ("carbonGas", ("21.386395374318933", "[-6856426.768, 3.159230584E7]")),
    ("turbine1", ("5.1545112652219064e-05", "[-55.55054339020381, 38.36885388122605]")),
    ("turbine2", ("7.577347502729098e-05", "[-80.18793076923077, 63.665174692307694]")),
    ("turbine3", ("3.8565414329523206e-05", "[-27.227529265841437, 38.719203390203816]")),
    ("kepler0", ("4.866123310307557e-05", "[3.0664, 102.8708]")),
    ("kepler1", ("0.00023590317953098758", "[-288.15184, 89.927712]")),
    ("kepler2", ("0.0011821743715241617", "[-337.61856, 850.310096]")),
    ("himmilbeau", ("0.0005369187050519044", "[-775.0, 890.0]"))
  )

  val aaAA_Float64: Map[String, (String, String)] = Map(
    ("doppler", ("1.2852513956990104e-12", "[-282.4762120545625, 133.53913086064713]")),
    ("sine", ("7.508672360829453e-16", "[-1.6540002686363373, 1.6540002686363373]")),
    ("sineOrder3", ("1.2254703612493457e-15", "[-1.909859317102744, 1.909859317102744]")),
    ("sqroot", ("3.1025182423150005e-13", "[-365.875, 195.04296875]")),
    ("bspline0", ("1.6190752442450204e-16", "[-0.0625, 0.16666666666666666]")),
    ("bspline1", ("7.540264708912522e-16", "[-0.08333333333333333, 0.9791666666666666]")),
    ("bspline2", ("6.800116025829084e-16", "[-0.020833333333333332, 0.9166666666666666]")),
    ("bspline3", ("1.0639637319324417e-16", "[-0.16666666666666666, 0.0625]")),
    ("rigidBody1", ("3.2152058793144533e-13", "[-705.0, 705.0]")),
    ("rigidBody2", ("3.652100843964945e-11", "[-57390.0, 58740.0]")),
    ("verhulst", ("5.404475598160268e-16", "[0.35217660784145, 0.9891620430819574]")),
    ("predatorPrey", ("2.5014868553833655e-16", "[-0.0021029110350001693, 0.34533842311741914]")),
    ("carbonGas", ("3.983524363087597e-08", "[-6856426.768, 3.159230584E7]")),
    ("turbine1", ("9.601020280849505e-14", "[-55.55054339020381, 38.36885388122605]")),
    ("turbine2", ("1.4113902525335888e-13", "[-80.18793076923077, 63.665174692307694]")),
    ("turbine3", ("7.183364461717313e-14", "[-27.227529265841437, 38.719203390203816]")),
    ("kepler0", ("9.063860773039779e-14", "[3.0664, 102.8708]")),
    ("kepler1", ("4.394038910504606e-13", "[-288.15184, 89.927712]")),
    ("kepler2", ("2.201971227577815e-12", "[-337.61856, 850.310096]")),
    ("himmilbeau", ("1.0000889005823412e-12", "[-775.0, 890.0]"))
  )


  val smtAA_Fixed32: Map[String, (String, String)] = Map(
    ("doppler", ("1.75791622644632e-06", "[-137.64316836703838, -0.02944244059231351]")),
    ("sine", ("3.350232349298144e-09", "[-1.0043283753002443, 1.0043283753002443]")),
    ("sineOrder3", ("6.536979105441602e-09", "[-1.0055351041384715, 1.0055351041384715]")),
    ("sqroot", ("3.998633478610562e-06", "[-334.7934013614431, 1.6171710207127035]")),
    ("bspline0", ("8.537123605701497e-10", "[0.0, 0.16666666666666666]")),
    ("bspline1", ("2.949188153689887e-09", "[0.16617838541666666, 0.6671549479166666]")),
    ("bspline2", ("2.599942188082592e-09", "[0.16666666666666666, 0.6668650309244791]")),
    ("bspline3", ("6.208817165548793e-10", "[-0.16666666666666666, 0.0]")),
    ("rigidBody1", ("1.3485550881126018e-06", "[-705.0, 705.0]")),
    ("rigidBody2", ("0.0001531802118132919", "[-56049.1667175293, 58740.0]")),
    ("verhulst", ("2.7762362769533775e-09", "[0.3640144188500088, 0.9473239405662036]")),
    ("predatorPrey", ("1.647071564900709e-09", "[0.03727705922396188, 0.33711264367110555]")),
    ("carbonGas", ("17.67125597003061", "[4301713.35625, 1.6740286809375E7]")),
    ("turbine1", ("4.799229183495741e-07", "[-18.536291471169722, -1.986569412987437]")),
    ("turbine2", ("5.570496496692077e-07", "[-28.564652726384942, 3.8296552477598733]")),
    ("turbine3", ("3.3413411173656214e-07", "[0.5626821330545027, 11.434005458108444]")),
    ("kepler0", ("3.8016587502569254e-07", "[20.802505677616598, 95.98217598131895]")),
    ("kepler1", ("1.7833888537937244e-06", "[-278.40975267291554, -31.98171302891623]")),
    ("kepler2", ("9.42228436899577e-06", "[-645.8671575, 1288.34451875]")),
    ("himmilbeau", ("4.194676880030224e-06", "[-0.1416015625, 890.0]"))
  )


  val smtAA_Float32: Map[String, (String, String)] = Map(
    ("doppler", ("0.0002250138141196529", "[-137.64316836703838, -0.02944244059231351]")),
    ("sine", ("3.7318322294440997e-07", "[-1.0043283753002443, 1.0043283753002443]")),
    ("sineOrder3", ("6.579194264554884e-07", "[-1.0055351041384715, 1.0055351041384715]")),
    ("sqroot", ("0.000165820131940109", "[-334.7934013614431, 1.6171710207127035]")),
    ("bspline0", ("8.69234485871102e-08", "[0.0, 0.16666666666666666]")),
    ("bspline1", ("3.774960957159124e-07", "[0.16617838541666666, 0.6671549479166666]")),
    ("bspline2", ("3.3030908319631186e-07", "[0.16666666666666666, 0.6668650309244791]")),
    ("bspline3", ("5.712112027822517e-08", "[-0.16666666666666666, 0.0]")),
    ("rigidBody1", ("0.00017261505240639963", "[-705.0, 705.0]")),
    ("rigidBody2", ("0.01960706761337861", "[-56049.1667175293, 58740.0]")),
    ("verhulst", ("2.901506123777844e-07", "[0.3640144188500088, 0.9473239405662036]")),
    ("predatorPrey", ("1.3291118406629398e-07", "[0.03727705922396188, 0.33711264367110555]")),
    ("carbonGas", ("19.995233482473232", "[4301713.35625, 1.6740286809375E7]")),
    ("turbine1", ("4.7612433130717006e-05", "[-18.536291471169722, -1.986569412987437]")),
    ("turbine2", ("6.609932439930116e-05", "[-28.564652726384942, 3.8296552477598733]")),
    ("turbine3", ("3.367906049203724e-05", "[0.5626821330545027, 11.434005458108444]")),
    ("kepler0", ("4.866123310307557e-05", "[20.802505677616598, 95.98217598131895]")),
    ("kepler1", ("0.00021301499593723758", "[-248.33198742510353, -31.775362835450746]")),
    ("kepler2", ("0.0012060524690583414", "[-645.8671575, 1288.34451875]")),
    ("himmilbeau", ("0.0005369187050519044", "[-0.1416015625, 890.0]"))
  )

  val smtAA_Float64: Map[String, (String, String)] = Map(
    ("doppler", ("4.1911988101104756e-13", "[-137.64316836703838, -0.02944244059231351]")),
    ("sine", ("6.951079086011496e-16", "[-1.0043283753002443, 1.0043283753002443]")),
    ("sineOrder3", ("1.2254703612493457e-15", "[-1.0055351041384715, 1.0055351041384715]")),
    ("sqroot", ("3.088640454507186e-13", "[-334.7934013614431, 1.6171710207127035]")),
    ("bspline0", ("1.6190752442450204e-16", "[0.0, 0.16666666666666666]")),
    ("bspline1", ("7.031412489292659e-16", "[0.16617838541666666, 0.6671549479166666]")),
    ("bspline2", ("6.152485928131076e-16", "[0.16666666666666666, 0.6668650309244791]")),
    ("bspline3", ("1.0639637319324417e-16", "[-0.16666666666666666, 0.0]")),
    ("rigidBody1", ("3.2152058793144533e-13", "[-705.0, 705.0]")),
    ("rigidBody2", ("3.652100843964945e-11", "[-56049.1667175293, 58740.0]")),
    ("verhulst", ("5.404475598160268e-16", "[0.3640144188500088, 0.9473239405662036]")),
    ("predatorPrey", ("2.4756633989832674e-16", "[0.03727705922396188, 0.33711264367110555]")),
    ("carbonGas", ("3.7244002681234605e-08", "[4301713.35625, 1.6740286809375E7]")),
    ("turbine1", ("8.868501650477874e-14", "[-18.536291471169722, -1.986569412987437]")),
    ("turbine2", ("1.231195155150921e-13", "[-28.564652726384942, 3.8296552477598733]")),
    ("turbine3", ("6.273210147405655e-14", "[0.5626821330545027, 11.434005458108444]")),
    ("kepler0", ("9.063860773039779e-14", "[20.802505677616598, 95.98217598131895]")),
    ("kepler1", ("4.251930363352586e-13", "[-278.40975267291554, -31.96861380296662]")),
    ("kepler2", ("2.2464476501227186e-12", "[-645.8671575, 1288.34451875]")),
    ("himmilbeau", ("1.0000889005823412e-12", "[-0.1416015625, 890.0]"))
  )




  test("warm-up") {
    val ctx = context.copy(options=Seq(ChoiceOption("rangeMethod", "interval")))
    val (_, program) = pipeline.run(ctx, inputPrg.deepCopy)
    ctx.reporter.info("time taken (for all benchmarks): " + ctx.timers.analysis)

  }

  ignore("interval-interval") {
    //TODO
  }

  test("interval-affine-fixed32") {
    val ctx = context.copy(options=Seq(ChoiceOption("rangeMethod", "interval"),
      ChoiceOption("precision", "Fixed32")))
    val (resCtx, program) = pipeline.run(ctx, inputPrg.deepCopy)
    ctx.reporter.info("time taken (for all benchmarks): " + ctx.timers.analysis)

    for (fnc <- program.defs) {
      val absError = resCtx.resultAbsoluteErrors(fnc.id)
      val range = resCtx.resultRealRanges(fnc.id)

      assert(intAA_Fixed32(fnc.id.toString)._1 === absError.toString, fnc.id)
      assert(intAA_Fixed32(fnc.id.toString)._2 === range.toString, fnc.id)
    }
  }

  test("interval-affine-float32") {
    val ctx = context.copy(options=Seq(ChoiceOption("rangeMethod", "interval"),
      ChoiceOption("precision", "Float32")))
    val (resCtx, program) = pipeline.run(ctx, inputPrg.deepCopy)
    ctx.reporter.info("time taken (for all benchmarks): " + ctx.timers.analysis)

    for (fnc <- program.defs) {
      val absError = resCtx.resultAbsoluteErrors(fnc.id)
      val range = resCtx.resultRealRanges(fnc.id)
      assert(intAA_Float32(fnc.id.toString)._1 === absError.toString, fnc.id)
      assert(intAA_Float32(fnc.id.toString)._2 === range.toString, fnc.id)
    }
  }

  test("interval-affine-float64") {
    val ctx = context.copy(options=Seq(ChoiceOption("rangeMethod", "interval"),
      ChoiceOption("precision", "Float64")))
    val (resCtx, program) = pipeline.run(ctx, inputPrg.deepCopy)
    ctx.reporter.info("time taken (for all benchmarks): " + ctx.timers.analysis)

    for (fnc <- program.defs) {
      val absError = resCtx.resultAbsoluteErrors(fnc.id)
      val range = resCtx.resultRealRanges(fnc.id)

      assert(intAA_Float64(fnc.id.toString)._1 === absError.toString, fnc.id)
      assert(intAA_Float64(fnc.id.toString)._2 === range.toString, fnc.id)
    }

  }

  test("affine-affine-fixed32") {
    val ctx = context.copy(options=Seq(ChoiceOption("rangeMethod", "affine"),
      ChoiceOption("precision", "Fixed32")))
    val (resCtx, program) = pipeline.run(ctx, inputPrg.deepCopy)
    ctx.reporter.info("time taken (for all benchmarks): " + ctx.timers.analysis)

    for (fnc <- program.defs) {
      val absError = resCtx.resultAbsoluteErrors(fnc.id)
      val range = resCtx.resultRealRanges(fnc.id)

      assert(aaAA_Fixed32(fnc.id.toString)._1 === absError.toString, fnc.id)
      assert(aaAA_Fixed32(fnc.id.toString)._2 === range.toString, fnc.id)
    }
  }

  test("affine-affine-float32") {
    val ctx = context.copy(options=Seq(ChoiceOption("rangeMethod", "affine"),
      ChoiceOption("precision", "Float32")))
    val (resCtx, program) = pipeline.run(ctx, inputPrg.deepCopy)
    ctx.reporter.info("time taken (for all benchmarks): " + ctx.timers.analysis)

    for (fnc <- program.defs) {
      val absError = resCtx.resultAbsoluteErrors(fnc.id)
      val range = resCtx.resultRealRanges(fnc.id)

      assert(aaAA_Float32(fnc.id.toString)._1 === absError.toString, fnc.id)
      assert(aaAA_Float32(fnc.id.toString)._2 === range.toString, fnc.id)
    }
  }

  test("affine-affine-float64") {
    val ctx = context.copy(options=Seq(ChoiceOption("rangeMethod", "affine"),
      ChoiceOption("precision", "Float64")))
    val (resCtx, program) = pipeline.run(ctx, inputPrg.deepCopy)
    ctx.reporter.info("time taken (for all benchmarks): " + ctx.timers.analysis)

    for (fnc <- program.defs) {
      val absError = resCtx.resultAbsoluteErrors(fnc.id)
      val range = resCtx.resultRealRanges(fnc.id)
      assert(aaAA_Float64(fnc.id.toString)._1 === absError.toString, fnc.id)
      assert(aaAA_Float64(fnc.id.toString)._2 === range.toString, fnc.id)
    }
  }

  test("smt-affine-fixed32") {
    val ctx = context.copy(options=Seq(ChoiceOption("rangeMethod", "smt"),
      ChoiceOption("precision", "Fixed32")))
    val (resCtx, program) = pipeline.run(ctx, inputPrg.deepCopy)
    ctx.reporter.info("time taken (for all benchmarks): " + ctx.timers.analysis)

    for (fnc <- program.defs) {
      // kepler1 sometimes returns a different result, presumably because of a Z3 timeout
      if (fnc.id.toString != "kepler1") {
        val absError = resCtx.resultAbsoluteErrors(fnc.id)
        val range = resCtx.resultRealRanges(fnc.id)

        assert(smtAA_Fixed32(fnc.id.toString)._1 === absError.toString, fnc.id)
        assert(smtAA_Fixed32(fnc.id.toString)._2 === range.toString, fnc.id)
      }
    }
  }


  test("smt-affine-float32") {
    val ctx = context.copy(options=Seq(ChoiceOption("rangeMethod", "smt"),
      ChoiceOption("precision", "Float32")))
    val (resCtx, program) = pipeline.run(ctx, inputPrg.deepCopy)
    ctx.reporter.info("time taken (for all benchmarks): " + ctx.timers.analysis)

    for (fnc <- program.defs) {
      // kepler1 sometimes returns a different result, presumably because of a Z3 timeout
      if (fnc.id.toString != "kepler1") {
        val absError = resCtx.resultAbsoluteErrors(fnc.id)
        val range = resCtx.resultRealRanges(fnc.id)
        assert(smtAA_Float32(fnc.id.toString)._1 === absError.toString, fnc.id)
        assert(smtAA_Float32(fnc.id.toString)._2 === range.toString, fnc.id)
      }
    }
  }


  test("smt-affine-float64") {
    val ctx = context.copy(options=Seq(ChoiceOption("rangeMethod", "smt"),
      ChoiceOption("precision", "Float64")))
    val (resCtx, program) = pipeline.run(ctx, inputPrg.deepCopy)
    ctx.reporter.info("time taken (for all benchmarks): " + ctx.timers.analysis)

    for (fnc <- program.defs) {
      // kepler1 sometimes returns a different result, presumably because of a Z3 timeout
      if (fnc.id.toString != "kepler1") {
        val absError = resCtx.resultAbsoluteErrors(fnc.id)
        val range = resCtx.resultRealRanges(fnc.id)
        assert(smtAA_Float64(fnc.id.toString)._1 === absError.toString, fnc.id)
        assert(smtAA_Float64(fnc.id.toString)._2 === range.toString, fnc.id)
      }
    }
  }

  ignore("jet engine") {

    val jetFile: String = "src/test/resources/AbsErrorRegressionJetEngine.scala"
    val jetContext = Context(
      reporter = new DefaultReporter(Set()), options = List(), files = List(jetFile)
    )
    val inputPrgJet = frontend.ExtractionPhase(jetContext)

    // with intervals this should crash
    intercept[daisy.tools.package$DivisionByZeroException] {
      val ctx = jetContext.copy(options=Seq(ChoiceOption("rangeMethod", "interval")))
      val (_, _) = pipeline.run(ctx, inputPrgJet.deepCopy)
    }

    {
      val ctx = jetContext.copy(options=Seq(ChoiceOption("rangeMethod", "affine")))
      val (resCtx, prg) = pipeline.run(ctx, inputPrgJet.deepCopy)
      for (fnc <- prg.defs) {
        val absError = resCtx.resultAbsoluteErrors(fnc.id)
        val range = resCtx.resultRealRanges(fnc.id)
          assert("4.088949761228701e-08" === absError.toString)
          assert("[-2236030.8006656803, 2243586.0576923075]" === range.toString)
      }
    }

    {
      val ctx = jetContext.copy(options=Seq(ChoiceOption("rangeMethod", "smt")))
      val (resCtx, prg) = pipeline.run(ctx, inputPrgJet.deepCopy)
      for (fnc <- prg.defs) {
        val absError = resCtx.resultAbsoluteErrors(fnc.id)
        val range = resCtx.resultRealRanges(fnc.id)
        assert("1.1589127503449215e-08" === absError.toString)
        assert("[-1781.3056274865728, 4820.245415085443]" === range.toString)
      }
    }

  }

  // private def getLastExpression(e: Expr): Expr = e match {
  //   case Let(_, _, body) => getLastExpression(body)
  //   case _ => e
  // }

}
