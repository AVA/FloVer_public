
import org.scalameter.api._
import org.scalameter.execution._

import org.scalameter.picklers.Implicits._
import org.scalameter.picklers.Pickler

import scala.util.Random

import daisy.utils.Rational
import Rational._

object AffineFormBench extends Bench[Double] {//.ForkedTime {
  /* configuration */
  def warmer: Warmer = new Warmer.Default
  def measurer: Measurer[Double] = new Measurer.Default
  def aggregator: Aggregator[Double] = Aggregator.average

  def executor: Executor[Double] = new SeparateJvmsExecutor(
    warmer,
    aggregator,
    measurer
  )

  def persistor: Persistor = Persistor.None
  def reporter: Reporter[Double] = new LoggingReporter

  /*lazy val aggregator: Aggregator[Double] = Aggregator.min

  lazy val measurer: Measurer[Double] = new Measurer.IgnoringGC
    with Measurer.PeriodicReinstantiation[Double] {
    override val defaultFrequency = 12
    override val defaultFullGC = true
  }*/

  /*lazy val executor = LocalExecutor(
    new Executor.Warmer.Default,
    Aggregator[Double].average,
    new Measurer.Default)
  lazy val reporter = new LoggingReporter
  lazy val persistor = Persistor.None
  */

  /*def executor = SeparateJvmsExecutor(
    new Executor.Warmer.Default,
    Aggregator.average[Double],
    new Measurer.Default)

  def reporter = new LoggingReporter
  def persistor = Persistor.None
  */
  /* Inputs */
  val sizes = Gen.range("size")(100, 10001, 200)

  val seqs = for {
    size <- sizes
  } yield {
    Seq.fill(size)((Rational.fromDouble(Random.nextDouble), Random.nextInt))
  }

  /* Tests */

  performance of "AffineForm" in {
    measure method "sumQueue-iterator" in {
      using(seqs) in {
        s => {
          var sum = zero
          val iter = s.iterator
          while(iter.hasNext) {
            sum += abs(iter.next._1)
          }
          sum
        }
      }
    }

    measure method "sumQueue-basic" in {
      using(seqs) in {
        s => {
          var sum = zero
          var i = 0
          val length = s.length
          while(i < length) {
            sum += abs(s(i)._1)
            i = i + 1
          }
          sum
        }
      }
    }

    measure method "sumQueue-functionally" in {
      using(seqs) in {
        s => {

          s.foldLeft(zero)((sum, elem) =>
            sum + abs(elem._1))

        }
      }
    }
  }

  /*performance of "AffineForm" in {

  }

  performance of "AffineForm" in {

  }*/


}