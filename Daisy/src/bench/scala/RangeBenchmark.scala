
import org.scalameter.api._

import org.scalameter.picklers.Implicits._
import org.scalameter.picklers.Pickler

object RangeBenchmark extends Bench[Double] {

  /* configuration */

  def aggregator: Aggregator[Double] = Aggregator.min
  def measurer: Measurer[Double] = new Measurer.Default

  lazy val executor = LocalExecutor(
    new Executor.Warmer.Default,
    aggregator,
    measurer)
  def persistor: Persistor = Persistor.None
  def reporter: Reporter[Double] = new LoggingReporter

  /* inputs */

  val sizes = Gen.range("size")(300000, 1500000, 300000)

  val ranges = for {
    size <- sizes
  } yield 0 until size

  /* tests */

  performance of "Range" in {
    measure method "map" in {
      using(ranges) in {
        r => r.map(_ + 1)
      }
    }
  }
}






/*package org.scalameter.examples

import org.scalameter.api._

object RangeBenchmark extends Bench.LocalTime {
  val sizes = Gen.range("size")(300000, 1500000, 300000)

  val ranges = for {
    size <- sizes
  } yield 0 until size

  performance of "Range" in {
    measure method "map" in {
      using(ranges) in {
        r => r.map(_ + 1)
      }
    }
  }
}*/