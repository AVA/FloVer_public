# FloVer

## Checking Coq certificates

To check the Coq certificate, you need to enter the `coq` directory and then run
```bash
$ ./configure_coq.sh
$ make
```

Note that compiling FloVer requires the Flocq library to be installed.

This will compile all coq files and then in the end the certificate in the output directory.
Note that the development is known to compile only with Coq Version 8.6

The coq binary is build in the directory `coq/binary` and you can compile it by
running `make native` in the directory.

A certificate build for Coq can be checked by placing it in the output folder under the coq directory and running the coq compiler on it.
```bash
$ coqc -R ./ FloVer output/CERTIFICATE_FILE.v
```

The generated binary can be run with `coq_checker_native CERTIFICATE_FILE`.
where `CERTITICATE_FILE` must be in the format for the binary.

## Checking HOL4 certificates

FloVer should compile with the latest version of HOL4. Please make sure to use an up-to-date version from git, if something does not work.

Note that there is currently a bug related to pointer equalities in HOL4, which may make checking a HOL4 file fail, although it should be compiled.
See: https://sourceforge.net/p/hol/mailman/message/36064682/
If checking of a FloVer file fails, rerunning `Holmake` on the file that failed may help.
An alternative fix is to run  Holmake using only one thread `Holmake -j1` or run the file interactively i.e. in emacs or vim.

If the problem persists, please contact one of the authors of FloVer.

Then, back in the FloVer home directory, if you haven't done so before:
```bash
$ git submodule init
```
(If successfull, you should see a message like ```Submodule 'hol4/cakeml' (https://github.com/CakeML/cakeml.git) registered for path 'hol4/cakeml'```)

Then, initialize the CakeML submodule and start compilation:
```bash
$ git submodule update --recursive --remote
$ cd hol4/
$ Holmake
```
Note that this may take some time, since it also builds all files from CakeML on which the binary code extraction depends.
If you don't want to wait for so long, you can cheat the compilation:
```bash
$ Holmake --fast
```

To check the HOL4 certificates, enter the `hol4/output` directory and run
```bash
$ Holmake ${CERTIFICATE_FILE/Script.sml/Theory.sig}
```
The HOL4 binary is build by entering the directory `hol4/binary`. Then one needs to run
```bash
$ Holmake checkerBinaryTheory.sig
$ Holmake
```
The generated file `cake_checker` is the binary.
It can be run by
```bash
$ ./cake_checker < CERTIFICATE_FILE
```

## Compiling Daisy

Daisy is set up to work with the [simple build tool (sbt)](http://www.scala-sbt.org/).
Once you have sbt, type in daisy's home directory:
```
$ sbt
```
This will run sbt in interactive mode. Note, if you run sbt for the first time,
*this operation may take a substantial amount of time* (heaven knows why). SBT helpfully
prints what it is doing in the process. If you feel like nothing has happened for an unreasonable
amount of time, abort and retry. This usually fixes the problem, otherwise try the old trick: restart.

To compile daisy:
```bash
> compile
```

To Daisy run an example:
```
> run testcases/rosa/Doppler.scala
```
Note that Daisy currently supports only one input file at a time.
The above command should produce an output such as (your own timing information will naturally vary):
```
Extracting program
[  Info  ]
[  Info  ] Starting specs preprocessing phase
[  Info  ] Finished specs preprocessing phase
[  Info  ]
[  Info  ]
[  Info  ] Starting range-error phase
[  Info  ] Finished range-error phase
[  Info  ]
[  Info  ] Starting info phase
[  Info  ] doppler
[  Info  ] error: 4.1911988101104756e-13, range: [-158.7191444098274, -0.02944244059231351]
[  Info  ] Finished info phase
[  Info  ] time:
[  Info  ] info:      6 ms, rangeError:    360 ms, analysis:      6 ms, frontend:   2902 ms,
```


To see all command-line options:
```
> run --help
```

If you don't want to run in interactive mode, you can also call all of the above
commands simply with 'sbt' prefixed, e.g. $ sbt compile.

You can also run Daisy outside of sbt. For this use '$ sbt script' which will
generate a script called 'daisy' which includes all the necessary files (and then some).
You can then run Daisy on an input file as follows:
```bash
$ ./daisy testcases/rosa/Doppler.scala
```

## Generating Error Bound Certificates

If you want to produce certificates to check them in either of the supported backends,
you have to call Daisy as with
```bash
$ ./daisy file --certificate=coq
```
or
```bash
$ ./daisy file --certificate=hol4
```
or for one of the binaries
```bash
$ ./daisy file --certificate=binary
```
The certificate can then be found in the folder `coq/output`, `hol4/output` or `output` for binary certificates.

## Additional Software

Some features of Daisy require additional software to be installed.
Currently, this is

* Z3: if you want to compute ranges with the help of the SMT solver Z3, you need to
[install it first on your machine](https://github.com/Z3Prover/z3).

* MPFR: Daisy uses a [Java binding](https://github.com/kframework/mpfr-java).
(TODO: figure out whether we used the static or dynamic binding)

Acknowledgements
----

A big portion of the infrastructure has been inspired by and sometimes
directly taken from the Leon project (see the LEON_LICENSE).

Especially the files in frontend, lang, solvers and utils bear more than
a passing resemblance to Leon's code.
Leon version used: 978a08cab28d3aa6414a47997dde5d64b942cd3e