(**
  Some simplification properties of real numbers, not proven in the Standard Library
**)

Require Import Coq.Reals.Reals Coq.micromega.Psatz Coq.Setoids.Setoid.

(** Define the maxAbs function on R and prove some minor properties of it.
TODO: Make use of IVLO and IVhi
 **)
Definition RmaxAbsFun (iv:R * R) :=
  Rmax (Rabs (fst iv)) (Rabs (snd iv)).

Definition RminAbsFun (iv: R * R) :=
  Rmin (Rabs (fst iv)) (Rabs (snd iv)).

Lemma Rsub_eq_Ropp_Rplus (a:R) (b:R) :
  (a - b = a + (- b))%R.
Proof.
  field_simplify; reflexivity.
Qed.

Lemma Rabs_simplify (a b: R) :
  (Rabs a <= b <-> ((a <= 0) /\ a >= - b) \/ (a >= 0 /\ a <= b))%R.
Proof.
  split.
  - intros abs.
    unfold Rabs in abs.
    destruct Rcase_abs in abs; lra.
  - intros cases_abs.
    unfold Rabs.
    destruct Rcase_abs; lra.
Qed.

Lemma bound_flip_sub:
  forall a b e,
    (Rabs (a - b) <= e ->
     Rabs ( - a - (- b)) <= e)%R.
Proof.
  intros a b e abs_less.
  rewrite Rsub_eq_Ropp_Rplus.
  rewrite <- Ropp_plus_distr.
  rewrite Rabs_Ropp.
  rewrite <- Rsub_eq_Ropp_Rplus; auto.
Qed.

Lemma plus_bounds_simplify:
  forall a b c d e, (a + b + ( c + d + e) = (a + c) + (b + d) + e)%R.
Proof.
  intros.
  repeat rewrite <- Rplus_assoc.
  setoid_rewrite Rplus_comm at 4.
  setoid_rewrite Rplus_assoc at 3.
  setoid_rewrite Rplus_comm at 3; auto.
Qed.

Lemma Rabs_err_simpl:
  forall a b,
    (Rabs (a - (a * (1 + b))) = Rabs (a * b))%R.
Proof.
  intros a b.
  rewrite Rmult_plus_distr_l.
  rewrite Rmult_1_r.
  rewrite Rsub_eq_Ropp_Rplus.
  rewrite Ropp_plus_distr.
  assert (- a + (- (a * b)) = ( - (a * b) + - a))%R by (rewrite Rplus_comm; auto).
  rewrite H.
  rewrite <- Rsub_eq_Ropp_Rplus.
  rewrite Rplus_minus.
  rewrite Rabs_Ropp; reflexivity.
Qed.

(**
  Prove that using eps = 0 makes the evaluation deterministic
 **)
Lemma Rabs_0_impl_eq (d:R):
  Rle (Rabs d) R0 -> d = R0.
Proof.
  intros abs_leq_0.
  pose proof (Rabs_pos d) as abs_geq_0.
  pose proof (Rle_antisym (Rabs d) R0 abs_leq_0 abs_geq_0) as Rabs_eq.
  rewrite <- Rabs_R0 in Rabs_eq.
  apply Rsqr_eq_asb_1 in Rabs_eq.
  rewrite Rsqr_0 in Rabs_eq.
  apply Rsqr_0_uniq in Rabs_eq; assumption.
Qed.

Lemma RmaxAbs_peel_Rabs a b:
  Rmax (Rabs a) (Rabs b) = Rabs (Rmax (Rabs a) (Rabs b)).
Proof.
  apply Rmax_case_strong; intros; rewrite Rabs_Rabsolu; auto.
Qed.

Lemma equal_naming a b c d:
  (b = 0 -> False)%R ->
  (d = 0 -> False)%R ->
  (a / b + c / d = (a * d + c * b) / (b * d))%R.
Proof.
  intros b_neq_zero d_neq_zero.
  rewrite Rdiv_plus_distr.
  unfold Rdiv.
  repeat (rewrite Rinv_mult_distr; try auto).
  setoid_rewrite (Rmult_comm (/b) (/d)) at 1.
  repeat rewrite <- Rmult_assoc.
  repeat (rewrite Rinv_r_simpl_l; try auto).
Qed.

Lemma inv_eq_1_div a:
  (/ a = 1 / a)%R.
Proof.
  lra.
Qed.

Lemma equal_naming_inv a b:
  (a = 0 -> False)%R ->
  (b = 0 -> False)%R ->
  (/ a + / b = (a + b) / (a * b))%R.
Proof.
  repeat rewrite inv_eq_1_div.
  intros; rewrite equal_naming; auto.
  lra.
Qed.

Lemma Rmult_0_lt_preserving a b:
  (0 < a)%R ->
  (0 < b)%R ->
  (0 < a * b)%R.
Proof.
  intros; rewrite <- (Rmult_0_l 0); apply Rmult_le_0_lt_compat; lra.
Qed.

Lemma Rmult_lt_0_inverting a b:
  (a < 0)%R ->
  (b < 0)%R ->
  (0 < a * b)%R.
Proof.
  intros.
  rewrite <- (Ropp_involutive (a * b)).
  rewrite Ropp_mult_distr_r, Ropp_mult_distr_l.
  rewrite <- (Rmult_0_l 0).
  apply Rmult_le_0_lt_compat; lra.
Qed.

Lemma Rabs_case_inverted a b c:
  (Rabs (a - b) <= c)%R <-> ((a - b > 0 /\ a - b <= c) \/ ( a - b <= 0 /\ - (a - b) <= c))%R.
Proof.
  split.
  - intros rabs_leq.
    unfold Rabs in *; destruct Rcase_abs in rabs_leq; lra.
  - intros [ abs_hi | abs_lo]; unfold Rabs; destruct Rcase_abs; try lra.
Qed.

Lemma lt_or_gt_neq_zero_hi a b:
  (b < 0 \/ 0 < a)%R ->
  (a <= b)%R ->
  (b = 0)%R -> False.
Proof.
  intros neq_zero a_leq_b b_zero.
  destruct neq_zero; subst; lra.
Qed.

Lemma lt_or_gt_neq_zero_lo a b:
  (b < 0 \/ 0 < a)%R ->
  (a <= b)%R ->
  (a = 0)%R -> False.
Proof.
  intros neq_zero a_leq_b a_zero.
  destruct neq_zero; subst; lra.
Qed.