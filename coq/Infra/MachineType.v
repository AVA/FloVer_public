(**
  Implementation of machine-precision as a datatype for mixed-precision computations

  @author: Raphael Monat
  @maintainer: Heiko Becker
 **)

Require Import Coq.QArith.QArith Coq.QArith.Qminmax Coq.QArith.Qabs
               Coq.QArith.Qreals Coq.Reals.Reals Coq.micromega.Psatz.
Require Import FloVer.Infra.RealRationalProps.
(**
   Define machine precision as datatype
 **)
Inductive mType: Type := M0 | M16 | M32 | M64. (*| M128 | M256*)

(**
  Injection of machine types into Q
**)
Definition mTypeToQ (e:mType) :Q :=
  match e with
  | M0 => 0
  | M16 => (Qpower (2#1) (Zneg 11))
  | M32 => (Qpower (2#1) (Zneg 24))
  | M64 => (Qpower (2#1) (Zneg 53))
  (*
  (* the epsilons below match what is used internally in flover,
  although these value do not match the IEEE standard *)
  | M128 => (Qpower (2#1) (Zneg 105))
  | M256 => (Qpower (2#1) (Zneg 211)) *)
  end.

Arguments mTypeToQ _/.

Lemma mTypeToQ_pos_Q:
  forall e, 0 <= mTypeToQ e.
Proof.
  intro e.
  case_eq e; intro;
    simpl mTypeToQ; apply Qle_bool_iff; auto.
Qed.

Lemma mTypeToQ_pos_R :
  forall e, (0 <= Q2R (mTypeToQ e))%R.
Proof.
  intro.
  rewrite <- Q2R0_is_0.
  apply Qle_Rle.
  apply mTypeToQ_pos_Q.
Qed.

Definition mTypeEq (m1:mType) (m2:mType) :=
  match m1, m2 with
  | M0, M0 => true
  | M16, M16 => true
  | M32, M32 => true
  | M64, M64 => true
  (* | M128, M128 => true *)
  (* | M256, M256 => true *)
  | _, _ => false
  end.

Lemma mTypeEq_compat_eq(m1:mType) (m2:mType):
  mTypeEq m1 m2 = true <-> m1 = m2.
Proof.
  split; intros eq_case;
    case_eq m1; intros m1_case;
      case_eq m2; intros m2_case; subst;
        try congruence; try auto;
          cbv in eq_case; inversion eq_case.
Qed.

Lemma mTypeEq_refl (m:mType):
  mTypeEq m m = true.
Proof.
  intros. rewrite mTypeEq_compat_eq; auto.
Qed.

Lemma mTypeEq_compat_eq_false (m1:mType) (m2:mType):
  mTypeEq m1 m2 = false <-> ~(m1 = m2).
Proof.
  split; intros eq_case.
  - hnf; intros; subst. rewrite mTypeEq_refl in eq_case.
    congruence.
  - case_eq m1; intros; case_eq m2; intros; subst; cbv; congruence.
Qed.

Ltac type_conv :=
  repeat
    (match goal with
     | [H : mTypeEq _ _ = true |- _] => rewrite mTypeEq_compat_eq in H; subst
     | [H : mTypeEq _ _ = false |- _] => rewrite mTypeEq_compat_eq_false in H
     end).

Lemma mTypeEq_sym (m1:mType) (m2:mType):
  forall b, mTypeEq m1 m2 = b -> mTypeEq m2 m1 = b.
Proof.
  intros.
  destruct b;
    [rewrite mTypeEq_compat_eq in * | rewrite mTypeEq_compat_eq_false in *];
    auto.
Qed.

(**
  Check if machine precision m1 is more precise than machine precision m2.
  M0 is real-valued evaluation, hence the most precise.
  All others are compared by
    mTypeToQ m1 <= mTypeToQ m2
 **)
Definition isMorePrecise (m1:mType) (m2:mType) :=
  Qle_bool (mTypeToQ m1) (mTypeToQ m2).

Lemma isMorePrecise_refl (m:mType) :
  isMorePrecise m m = true.
Proof.
  unfold isMorePrecise; simpl.
  apply Qle_bool_iff; lra.
Qed.

Lemma M0_least_precision (m:mType) :
  isMorePrecise m M0 = true -> m = M0.
Proof.
  intro m_morePrecise.
  unfold isMorePrecise in *.
  destruct m; cbv in m_morePrecise; congruence.
Qed.

Lemma M0_lower_bound (m:mType) :
  isMorePrecise M0 m = true.
Proof.
  destruct m; unfold isMorePrecise; cbv; auto.
Qed.

(**
  Function computing the join of two precisions, this is the most precise type,
  in which evaluation has to be performed, e.g. addition of 32 and 64 bit floats
  has to happen in 64 bits
**)
Definition join (m1:mType) (m2:mType) :=
  if (isMorePrecise m1 m2) then m1 else m2.

(* Lemma M0_join_is_M0 m1 m2: *)
(*   join m1 m2 = M0 -> m1 = M0 /\ m2 = M0. *)
(* Proof. *)
(*   unfold join. *)
(*   intros. *)
(*   destruct m1, m2; simpl in *; cbv in *; try congruence; try auto. *)
(* Qed. *)

Definition maxExponent (m:mType) :positive :=
  match m with
  | M0 => 1
  | M16 => 15
  | M32 => 127
  | M64 => 1023
  end.

Definition minExponentPos (m:mType) :positive :=
  match m with
  | M0 => 1
  | M16 => 14
  | M32 => 126
  | M64 => 1022
  end.

(**
Goldberg - Handbook of Floating-Point Arithmetic: (p.183)
(𝛃 - 𝛃^(1 - p)) * 𝛃^(e_max)

which simplifies to 2^(e_max) for base 2
**)
Definition maxValue (m:mType) :=
 (Z.pow_pos 2 (maxExponent m)) # 1.

Definition minValue (m:mType) :=
  Qinv ((Z.pow_pos 2 (minExponentPos m)) # 1).

(** Goldberg - Handbook of Floating-Point Arithmetic: (p.183)
  𝛃^(e_min -p + 1) = 𝛃^(e_min -1) for base 2
**)
Definition minDenormalValue (m:mType) :=
  Qinv (Z.pow_pos 2 (minExponentPos m - 1) # 1).

Definition normal (v:Q) (m:mType) :=
  Qle_bool (minValue m) (Qabs v) && Qle_bool (Qabs v) (maxValue m).

Definition denormal (v:Q) (m:mType) :=
  Qle_bool (Qabs v) (minValue m) && negb (Qeq_bool v 0).

Definition Normal (v:R) (m:mType) :=
  (Q2R (minValue m) <= (Rabs v) /\ (Rabs v) <= Q2R (maxValue m))%R.

Definition Denormal (v:R) (m:mType) :=
  ((Rabs v) < Q2R (minValue m) /\ ~ (v = 0))%R.
(**
  Predicate that is true if and only if the given value v is a valid
  floating-point value according to the the type m.
  Since we use the 1 + 𝝳 abstraction, the value must either be
  in normal range or 0.
**)
Definition validFloatValue (v:R) (m:mType) :=
  match m with
  | M0 => True
  | _ => Normal v m \/ Denormal v m \/ v = 0%R
  end.

Definition validValue (v:Q) (m:mType) :=
  match m with
  | M0 => true
  | _ => Qle_bool (Qabs v) (maxValue m)
  end.