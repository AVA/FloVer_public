(**
  Some abbreviations that require having defined expressions beforehand
  If we would put them in the Abbrevs file, this would create a circular dependency which Coq cannot resolve.
**)
Require Import Coq.QArith.QArith Coq.Reals.Reals Coq.QArith.Qreals.
Require Export FloVer.Infra.Abbrevs FloVer.Expressions.

(**
We treat a function mapping an expression arguing on fractions as value type
to pairs of intervals on rationals and rational errors as the analysis result
**)
Definition analysisResult :Type := exp Q -> intv * error.