Require Import Coq.Reals.Reals Coq.micromega.Psatz Coq.QArith.QArith Coq.QArith.Qreals Coq.MSets.MSets.
Require Import FloVer.Infra.RealRationalProps FloVer.Expressions FloVer.Infra.Ltacs FloVer.Commands FloVer.ssaPrgs FloVer.Infra.RationalSimps.
Require Export FloVer.Infra.Abbrevs FloVer.Infra.RealSimps FloVer.Infra.NatSet FloVer.IntervalArithQ FloVer.IntervalArith FloVer.Infra.MachineType.

Fixpoint typeExpression (V:Type) (Gamma:nat -> option mType) (e:exp V) : option mType :=
  match e with
  | Var _ v => Gamma v
  | Const m n => Some m
  | Unop u e1 => typeExpression Gamma e1
  | Binop b e1 e2 =>
    let tm1 := typeExpression Gamma e1 in
    let tm2 := typeExpression Gamma e2 in
    match tm1, tm2 with
    | Some m1, Some m2 => Some (join m1 m2)
    | _, _=> None
    end
  | Downcast m e1 =>
    let tm1 := typeExpression Gamma e1 in
    match tm1 with
    |Some m1 => if (isMorePrecise m1 m) then Some m else None
    |_ => None
    end
  end.

Fixpoint typeMap (Gamma:nat -> option mType) (e:exp Q) (e': exp Q) : option mType :=
  match e with
  | Var _ v => if expEq e e' then Gamma v else None
  | Const m n => if expEq e e' then Some m else None
  | Unop u e1 => if expEq e e' then typeExpression Gamma e else typeMap Gamma e1 e'
  | Binop b e1 e2 => if expEq e e' then typeExpression Gamma e
                    else
                      match (typeMap Gamma e1 e'), (typeMap Gamma e2 e')  with
                      | Some m1, Some m2 => if (mTypeEq m1 m2) then Some m1 else None
                      | Some m1, None => Some m1
                      | None, Some m2 => Some m2
                      | None, None => None
                      end
  | Downcast m e1 => if expEq e e' then typeExpression Gamma (Downcast m e1) else typeMap Gamma e1 e'
  end.

Fixpoint typeCmd (Gamma:nat -> option mType) (f:cmd Q): option mType :=
  match f with
  |Let m n e c => match typeExpression Gamma e with
                 |Some m' => if isMorePrecise m m' then typeCmd Gamma c
                            else None
                 |None => None
                 end
  |Ret e => typeExpression Gamma e
  end.

Fixpoint typeMapCmd (Gamma:nat -> option mType) (f:cmd Q) (f':exp Q) : option mType :=
  match f with
  |Let m n e c => if expEq f' (Var Q n) then
                   match Gamma n with
                   | Some m' => if isMorePrecise m m' then Some m else None
                   | None => None
                   end
                  else
                    let te := typeMap Gamma e in
                    let tc := typeMapCmd Gamma c in
                    match (te f'), (tc f') with
                    |Some m1, Some m2 => if mTypeEq m1 m2 then Some m1 else None
                    |Some m1, None => Some m1
                    |None, Some m2 => Some m2
                    |None, None => None
                    end
  |Ret e => (typeMap Gamma e) f'
  end.

Fixpoint typeCheck (e:exp Q) (Gamma:nat -> option mType) (tMap: exp Q -> option mType) : bool :=
  match e with
  | Var _ v => match tMap e, Gamma v with
              | Some m, Some m' => mTypeEq m m'
              | _, _ => false
             end
  | Const m n => match tMap e with
               | Some m' => mTypeEq m m'
               | None => false
               end
  | Unop u e1 => match tMap e with
                | Some m => match tMap e1 with
                           | Some m' => mTypeEq m m' && typeCheck e1 Gamma tMap
                           | None => false
                           end
                | None => false
                end
  | Binop b e1 e2 => match tMap e, tMap e1, tMap e2 with
                    | Some m, Some m1, Some m2 =>
                      mTypeEq m (join m1 m2)
                                  && typeCheck e1 Gamma tMap
                                  && typeCheck e2 Gamma tMap
                    | _, _, _ => false
                    end
  | Downcast m e1 => match tMap e, tMap e1 with
                    | Some m', Some m1 => mTypeEq m m' && isMorePrecise m1 m && typeCheck e1 Gamma tMap
                    | _, _ => false
                    end
  end.

Fixpoint typeCheckCmd (c:cmd Q) (Gamma:nat -> option mType) (tMap:exp Q -> option mType) : bool :=
  match c with
  | Let m x e g => if typeCheck e Gamma tMap
                  then
                    match tMap e, tMap (Var Q x) with
                    | Some me, Some mx =>  mTypeEq me m && mTypeEq m mx
                                          && typeCheckCmd g (updDefVars x me Gamma) tMap
                    | _, _ => false
                    end
                  else
                    false
  | Ret e => typeCheck e Gamma tMap
  end.

(* Fixpoint typeCheckCmd (c:cmd Q) (Gamma:nat -> option mType) (tMap:exp Q -> option mType) : bool := *)
(*   match c with *)
(*   | Let m x e g => match tMap (Var Q x), tMap e with *)
(*                   | Some mx, Some me => (mTypeEq mx m) && (isMorePrecise mx me) && typeCheck e Gamma tMap && typeCheckCmd g Gamma tMap *)
(*                   | _, _ => false *)
(*                   end *)
(*   | Ret e => typeCheck e Gamma tMap *)
(*   end. *)

(* Lemma eqTypeExpression e m Gamma: *)
(*   typeMap Gamma e e = Some m <-> typeExpression Gamma e = Some m. *)
(* Proof. *)
(*   revert m Gamma; induction e; intros. *)
(*   - split; intros. *)
(*     + simpl in *. *)
(*       rewrite <- beq_nat_refl in H; simpl in H; auto. *)
(*     + simpl in *. *)
(*       rewrite <- beq_nat_refl; simpl; auto. *)
(*   - split; intros; simpl in *. *)
(*     + rewrite mTypeEq_refl,Qeq_bool_refl in H; simpl in H; auto. *)
(*     + rewrite mTypeEq_refl,Qeq_bool_refl; simpl; auto. *)
(*   - split; intros; simpl in *. *)
(*     + rewrite unopEqBool_refl,expEq_refl in H; simpl in H; auto. *)
(*     + rewrite unopEqBool_refl,expEq_refl; simpl; auto. *)
(*   - split; intros; simpl in *. *)
(*     + rewrite binopEqBool_refl,expEq_refl,expEq_refl in H; simpl in H; auto. *)
(*     + rewrite binopEqBool_refl,expEq_refl,expEq_refl; simpl; auto. *)
(*   - split; intros; simpl in *. *)
(*     + rewrite mTypeEq_refl,expEq_refl in H; simpl in H; auto. *)
(*     + rewrite mTypeEq_refl,expEq_refl; simpl; auto. *)
(* Qed. *)

(* Definition Gamma_stronger (Gamma1 Gamma2: exp Q -> option mType):= *)
(*   (forall e m, Gamma1 e = Some m -> Gamma2 e = Some m). *)

(* Lemma Gamma_stronger_trans Gamma1 Gamma2 Gamma3 : *)
(*   Gamma_stronger Gamma1 Gamma2 -> *)
(*   Gamma_stronger Gamma2 Gamma3 -> *)
(*   Gamma_stronger Gamma1 Gamma3. *)
(* Proof. *)
(*   intros g12 g23 e m hyp. *)
(*   unfold Gamma_stronger in g12,g23. *)
(*   apply g23; apply g12; auto. *)
(* Qed. *)


(* Lemma Gamma_strengthening e Gamma1 Gamma2 Gamma: *)
(*   Gamma_stronger Gamma1 Gamma2 -> *)
(*   typeCheck e Gamma Gamma1 = true -> *)
(*   typeCheck e Gamma Gamma2 = true. *)
(* Proof. *)
(*   revert Gamma1 Gamma2; induction e; intros. *)
(*   - simpl in *. *)
(*     case_eq (Gamma1 (Var Q n)); intros; rewrite H1 in H0; [ | inversion H0 ]. *)
(*     specialize (H _ _ H1); rewrite H. *)
(*     auto. *)
(*   - simpl in *. *)
(*     case_eq (Gamma1 (Const m v)); intros; rewrite H1 in H0; [ | inversion H0 ]. *)
(*     specialize (H _ _ H1); rewrite H. *)
(*     auto. *)
(*   - simpl in *. *)
(*     case_eq (Gamma1 (Unop u e)); intros; rewrite H1 in H0; [ | inversion H0 ]. *)
(*     rewrite (H _ _ H1). *)
(*     case_eq (Gamma1 e); intros; rewrite H2 in H0; [ | inversion H0 ]. *)
(*     apply andb_true_iff in H0; destruct H0. *)
(*     apply mTypeEq_compat_eq in H0; subst. *)
(*     rewrite (H _ _ H2). *)
(*     rewrite mTypeEq_refl; simpl. *)
(*     eapply IHe; eauto. *)
(*   - simpl in *. *)
(*     case_eq (Gamma1 (Binop b e1 e2)); intros; rewrite H1 in H0; [ | inversion H0 ]. *)
(*     case_eq (Gamma1 e1); intros; rewrite H2 in H0; [ | inversion H0 ]. *)
(*     case_eq (Gamma1 e2); intros; rewrite H3 in H0; [ | inversion H0 ]. *)
(*     rewrite (H _ _ H1), (H _ _ H2), (H _ _ H3). *)
(*     andb_to_prop H0. *)
(*     rewrite L0; simpl. *)
(*     apply andb_true_iff; split. *)
(*     + eapply IHe1; eauto. *)
(*     + eapply IHe2; eauto. *)
(*   - simpl in *. *)
(*     case_eq (Gamma1 (Downcast m e)); intros; rewrite H1 in H0; [ | inversion H0 ]. *)
(*     case_eq (Gamma1 e); intros; rewrite H2 in H0; [ | inversion H0 ]. *)
(*     rewrite (H _ _ H1), (H _ _ H2). *)
(*     andb_to_prop H0. *)
(*     rewrite L0, R0; simpl. *)
(*     eapply IHe; eauto. *)
(* Qed. *)


Theorem typingSoundnessExp Gamma E:
  forall e v m expTypes,
    typeCheck e Gamma expTypes = true ->
    eval_exp E Gamma (toRExp e) v m ->
    expTypes e = Some m.
Proof.
  induction e; intros * typechecks evals.
  - simpl in typechecks.
    inversion evals; subst.
    rewrite H0 in typechecks.
    case_eq (expTypes (Var Q n)); intros; rewrite H in typechecks; [ | inversion typechecks ].
    apply mTypeEq_compat_eq in typechecks; subst; auto.
  - simpl in typechecks.
    inversion evals; subst.
    case_eq (expTypes (Const m0 v)); intros; rewrite H in typechecks; [ | inversion typechecks ].
    apply mTypeEq_compat_eq in typechecks; subst; auto.
  - simpl in typechecks.
    case_eq (expTypes (Unop u e)); intros; rewrite H in typechecks; [ | inversion typechecks ].
    case_eq (expTypes e); intros; rewrite H0 in typechecks; [ | inversion typechecks ].
    apply andb_true_iff in typechecks; destruct typechecks.
    apply mTypeEq_compat_eq in H1; subst.
    inversion evals; subst.
    + rewrite <- H0.
      eapply IHe; eauto.
    + rewrite <- H0.
      eapply IHe; eauto.
  - inversion evals; subst.
    simpl in typechecks.
    case_eq (expTypes (Binop b e1 e2)); intros; rewrite H in typechecks; [ | inversion typechecks ].
    case_eq (expTypes e1); intros; rewrite H0 in typechecks; [ | inversion typechecks ].
    case_eq (expTypes e2); intros; rewrite H1 in typechecks; [ | inversion typechecks ].
    andb_to_prop typechecks.
    apply mTypeEq_compat_eq in L0; subst.
    assert (expTypes e1 = Some m1) by (eapply IHe1; eauto).
    assert (expTypes e2 = Some m2) by (eapply IHe2; eauto).
    rewrite H0 in H4. rewrite H1 in H5.
    inversion H4; inversion H5; subst; auto.
  - simpl in typechecks.
    case_eq (expTypes (Downcast m e)); intros; rewrite H in typechecks; [ | inversion typechecks ].
    case_eq (expTypes e); intros; rewrite H0 in typechecks; [ | inversion typechecks ].
    andb_to_prop typechecks.
    apply mTypeEq_compat_eq in L0; subst.
    inversion evals; subst.
    auto.
Qed.

Theorem typingSoundnessCmd c Gamma E v m expTypes:
  typeCheckCmd c Gamma expTypes = true ->
  bstep (toRCmd c) E Gamma v m ->
  expTypes (getRetExp c) = Some m.
Proof.
  revert Gamma E expTypes; induction c; intros * tc bc.
  - simpl in tc.
    inversion bc; subst.
    andb_to_prop tc.
    assert (expTypes e = Some m0)
      as e_type_m0
        by (eapply typingSoundnessExp; eauto).
    specialize (IHc (updDefVars n m0 Gamma) (updEnv n v0 E)).
    simpl.
    rewrite e_type_m0 in R.
    destruct (expTypes (Var Q n)) eqn:?; try congruence.
    andb_to_prop R.
    apply IHc; auto.
  - simpl in *.
    inversion bc; subst.
    eapply typingSoundnessExp; eauto.
Qed.