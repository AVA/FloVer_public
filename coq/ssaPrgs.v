(**
  We define a pseudo SSA predicate.
  The formalization is similar to the renamedApart property in the LVC framework by Schneider, Smolka and Hack
  http://dblp.org/rec/conf/itp/SchneiderSH15
  Our predicate is not as fully fledged as theirs, but we especially borrow the idea of annotating
  the program with the predicate with the set of free and defined variables
**)
Require Import Coq.QArith.QArith Coq.QArith.Qreals Coq.Reals.Reals.
Require Import Coq.micromega.Psatz.
Require Import FloVer.Infra.RealRationalProps FloVer.Infra.Ltacs.
Require Export FloVer.Commands.

Lemma validVars_add V (e:exp V) Vs n:
  NatSet.Subset (usedVars e) Vs ->
  NatSet.Subset (usedVars e) (NatSet.add n Vs).
Proof.
  induction e; try auto.
  - intros valid_subset.
    hnf. intros a in_singleton.
    specialize (valid_subset a in_singleton).
    rewrite NatSet.add_spec; right; auto.
  - intros vars_binop.
    simpl in *.
    intros a in_empty.
    inversion in_empty.
  - simpl; intros in_vars.
    intros a in_union.
    rewrite NatSet.union_spec in in_union.
    destruct in_union.
    + apply IHe1; try auto.
      hnf; intros.
      apply in_vars.
      rewrite NatSet.union_spec; auto.
    + apply IHe2; try auto.
      hnf; intros.
      apply in_vars.
      rewrite NatSet.union_spec; auto.
Qed.

(*
Lemma validVars_non_stuck (e:exp Q) inVars E:
 NatSet.Subset (usedVars e) inVars ->
  (forall v, NatSet.In v (usedVars e) ->
        exists r, (toREvalEnv E) v = Some (r, M0))%R ->
  exists vR, eval_exp (toREvalEnv E) (toREval (toRExp e)) vR M0.
Proof.
    revert inVars E; induction e;
    intros inVars E vars_valid fVars_live.
  - simpl in *.
    assert (NatSet.In n (NatSet.singleton n)) as in_n by (rewrite NatSet.singleton_spec; auto).
    specialize (fVars_live n in_n).
    destruct fVars_live as [vR E_def].
    exists vR; econstructor; eauto.
  - exists (perturb (Q2R v) 0); constructor.
    simpl (meps M0); rewrite Rabs_R0; rewrite Q2R0_is_0; lra.
  - assert (exists vR, eval_exp (toREvalEnv E) (toREval (toRExp e)) vR M0)
      as eval_e_def by (eapply IHe; eauto).
    destruct eval_e_def as [ve eval_e_def].
    case_eq u; intros; subst.
    + exists (evalUnop Neg ve); constructor; auto.
    + exists (perturb (evalUnop Inv ve) 0); constructor; auto.
      simpl (meps M0); rewrite Q2R0_is_0; rewrite Rabs_R0; lra.
  - repeat rewrite NatSet.subset_spec in *; simpl in *.
    assert (exists vR1, eval_exp (toREvalEnv E) (toREval (toRExp e1)) vR1 M0) as eval_e1_def.
    + eapply IHe1; eauto.
      * hnf; intros.
        apply vars_valid.
        rewrite NatSet.union_spec; auto.
      * intros.
        destruct (fVars_live v) as [vR E_def]; try eauto.
        apply NatSet.union_spec; auto.
    + assert (exists vR2, eval_exp (toREvalEnv E) (toREval (toRExp e2)) vR2 M0) as eval_e2_def.
      * eapply IHe2; eauto.
        { hnf; intros.
        apply vars_valid.
        rewrite NatSet.union_spec; auto. }
        { intros.
          destruct (fVars_live v) as [vR E_def]; try eauto.
          apply NatSet.union_spec; auto. }
      * destruct eval_e1_def as [vR1 eval_e1_def];
          destruct eval_e2_def as [vR2 eval_e2_def].
        exists (perturb (evalBinop b vR1 vR2) 0).
        replace M0 with (computeJoin M0 M0) by auto.
        constructor; auto.
        simpl meps; rewrite Q2R0_is_0. rewrite Rabs_R0; lra.
  - assert (exists vR, eval_exp (toREvalEnv E) (toREval (toRExp e))  vR M0) as eval_r_def by (eapply IHe; eauto).
    destruct eval_r_def as [vr eval_r_def].
    exists vr.
    simpl toREval.
    auto.
Qed. *)


Inductive ssa (V:Type): (cmd V) -> (NatSet.t) -> (NatSet.t) -> Prop :=
 ssaLet m x e s inVars Vterm:
   NatSet.Subset (usedVars e) inVars->
   NatSet.mem x inVars = false ->
   ssa s (NatSet.add x inVars) Vterm ->
   ssa (Let m x e s) inVars Vterm
|ssaRet e inVars Vterm:
   NatSet.Subset (usedVars e) inVars ->
   NatSet.Equal inVars Vterm ->
   ssa (Ret e) inVars Vterm.


Lemma ssa_subset_freeVars V (f:cmd V) inVars outVars:
  ssa f inVars outVars ->
  NatSet.Subset (freeVars f) inVars.
Proof.
  intros ssa_f; induction ssa_f.
  - simpl in *. hnf; intros a in_fVars.
    rewrite NatSet.remove_spec, NatSet.union_spec in in_fVars.
    destruct in_fVars as [in_union not_eq].
    destruct in_union; try auto.
    specialize (IHssa_f a H1).
    rewrite NatSet.add_spec in IHssa_f.
    destruct IHssa_f; subst; try auto.
    exfalso; apply not_eq; auto.
  - hnf; intros.
    simpl in H1.
    apply H; auto.
Qed.


Lemma ssa_equal_set V (f:cmd V) inVars inVars' outVars:
  NatSet.Equal inVars' inVars ->
  ssa f inVars outVars ->
  ssa f inVars' outVars.
Proof.
  intros set_eq ssa_f.
  revert set_eq; revert inVars'.
  induction ssa_f.
  - constructor.
    + rewrite set_eq; auto.
    + case_eq (NatSet.mem x inVars'); intros case_mem; try auto.
      rewrite NatSet.mem_spec in case_mem.
      rewrite set_eq in case_mem.
      rewrite <- NatSet.mem_spec in case_mem.
      rewrite case_mem in *; congruence.
    + apply IHssa_f; auto.
      apply NatSetProps.Dec.F.add_m; auto.
  - constructor.
    + rewrite set_eq; auto.
    + rewrite set_eq; auto.
Qed.


Fixpoint validSSA (f:cmd Q) (inVars:NatSet.t) :=
  match f with
  |Let m x e g =>
    andb (andb (negb (NatSet.mem x inVars)) (NatSet.subset (usedVars e) inVars)) (validSSA g (NatSet.add x inVars))
  |Ret e => NatSet.subset (usedVars e) inVars
  end.

Lemma validSSA_sound f inVars:
  validSSA f inVars = true ->
  exists outVars, ssa f inVars outVars.
Proof.
  revert inVars; induction f.
  - intros inVars validSSA_let.
    simpl in *.
    andb_to_prop validSSA_let.
    specialize (IHf (NatSet.add n inVars) R).
    destruct IHf as [outVars IHf].
    exists outVars.
    constructor; eauto.
    + rewrite <- NatSet.subset_spec; auto.
    + rewrite negb_true_iff in L0. auto.
  - intros inVars validSSA_ret.
    simpl in *.
    exists inVars.
    constructor; auto.
    + rewrite <- NatSet.subset_spec; auto.
    + hnf; intros; split; auto.
Qed.

Lemma ssa_shadowing_free m x y v v' e f inVars outVars E defVars:
  ssa (Let m x (toREval (toRExp e)) (toREvalCmd (toRCmd f))) inVars outVars ->
  NatSet.In y inVars ->
  eval_exp E defVars (toREval (toRExp e)) v M0 ->
  forall E n, updEnv x v (updEnv y v' E) n = updEnv y v' (updEnv x v E) n.
Proof.
  intros ssa_let y_free eval_e.
  inversion ssa_let; subst.
  intros E' n; unfold updEnv.
  case_eq (n =? x).
  - intros n_eq_x.
    rewrite Nat.eqb_eq in n_eq_x.
    case_eq (n =? y); try auto.
    intros n_eq_y.
    rewrite Nat.eqb_eq in n_eq_y.
    rewrite n_eq_x in n_eq_y.
    rewrite <- n_eq_y in y_free.
    rewrite <- NatSet.mem_spec in y_free.
    rewrite y_free in H6. inversion H6.
  - intros n_neq_x.
    case_eq (n =? y); auto.
Qed.

(*
Lemma shadowing_free_rewriting_exp e v E1 E2 defVars:
  (forall n, E1 n = E2 n)->
  eval_exp E1 defVars (toREval e) v M0 <->
  eval_exp E2 defVars (toREval e) v M0.
Proof.
  revert v E1 E2.
  induction e; intros v' E1 E2 agree_on_vars.
  - split; intros eval_Var.
    + inversion eval_Var; subst.
      rewrite agree_on_vars in H1.
      constructor; auto.
    + inversion eval_Var; subst.
      rewrite <- agree_on_vars in H1.
      eapply Var_load; eauto.
  - split; intros eval_Const; inversion eval_Const; subst; econstructor; auto.
  - split; intros eval_Unop; inversion eval_Unop; subst; econstructor; try auto.
    + erewrite IHe; eauto.
    + erewrite IHe; eauto.
    + erewrite <- IHe; eauto.
    + erewrite <- IHe; eauto.
  - split; intros eval_Binop; inversion eval_Binop; subst; econstructor; eauto.
    destruct m1; destruct m2; inversion H2.
      try (erewrite IHe1; eauto);
      try (erewrite IHe2; eauto); auto.
  - split; intros eval_Downcast; simpl; simpl in eval_Downcast; erewrite IHe; eauto.
Qed.

Lemma shadowing_free_rewriting_cmd f E1 E2 vR defVars:
  (forall n, E1 n = E2 n) ->
  bstep (toREvalCmd f) E1 defVars vR M0 <->
  bstep (toREvalCmd f) E2 defVars vR M0.
Proof.
revert E1 E2 defVars vR.
  induction f; intros E1 E2 vR agree_on_vars.
  - split; intros bstep_Let; inversion bstep_Let; subst.
    + erewrite shadowing_free_rewriting_exp in H8; auto.
      econstructor; eauto.
      rewrite <- IHf.
      apply H10.
      intros n'; unfold updEnv.
      case_eq (n' =? n); auto.
    + erewrite <- shadowing_free_rewriting_exp in H8; auto.
      econstructor; eauto.
      rewrite IHf.
      apply H10.
      intros n'; unfold updEnv.
      case_eq (n' =? n); auto.
  - split; intros bstep_Ret; inversion bstep_Ret; subst.
    + erewrite shadowing_free_rewriting_exp in H1; auto.
      constructor; auto.
    + erewrite <- shadowing_free_rewriting_exp in H1; auto.
      constructor; auto.
Qed.

Lemma dummy_bind_ok e v x v' inVars E defVars:
  NatSet.Subset (usedVars e) inVars ->
  NatSet.mem x inVars = false ->
  eval_exp E defVars (toREval (toRExp e)) v M0 ->
  eval_exp (updEnv x v' E) defVars (toREval (toRExp e)) v M0.
Proof.
  revert v x v' inVars E.
  induction e; intros v1 x v2 inVars E valid_vars x_not_free eval_e.
  - inversion eval_e; subst.
    assert ((updEnv x v2 E) n = E n).
    + unfold updEnv.
      case_eq (n =? x); try auto.
      intros n_eq_x.
      rewrite Nat.eqb_eq in n_eq_x.
      subst; simpl in *.
      hnf in valid_vars.
      assert (NatSet.mem x (NatSet.singleton x) = true)
        as in_singleton by (rewrite NatSet.mem_spec, NatSet.singleton_spec; auto).
      rewrite NatSet.mem_spec in *.
      specialize (valid_vars x in_singleton).
      rewrite <- NatSet.mem_spec in valid_vars.
      rewrite valid_vars in *; congruence.
    + econstructor; eauto.
      rewrite H; auto.
  - inversion eval_e; subst; constructor; auto.
  - inversion eval_e; subst; econstructor; eauto.
  - simpl in valid_vars.
    inversion eval_e; subst; econstructor; eauto;
    destruct m1; destruct m2; inversion H2;
      subst.
    + eapply IHe1; eauto.
      hnf; intros a in_e1.
      apply valid_vars;
        rewrite NatSet.union_spec; auto.
    + eapply IHe2; eauto.
      hnf; intros a in_e2.
      apply valid_vars;
        rewrite NatSet.union_spec; auto.
  - apply (IHe v1 x v2 inVars E); auto.
Qed.

Fixpoint exp_subst (e:exp Q) x e_new :=
  match e with
  |Var _ v => if v =? x then e_new else Var Q v
  |Unop op e1 => Unop op (exp_subst e1 x e_new)
  |Binop op e1 e2 => Binop op (exp_subst e1 x e_new) (exp_subst e2 x e_new)
  |Downcast m e1 => Downcast m (exp_subst e1 x e_new)
  | e => e
  end.
*)
(* Lemma exp_subst_correct e e_sub E x v v_res defVars: *)
(*   eval_exp E defVars (toREval (toRExp e_sub)) v M0 -> *)
(*   eval_exp (updEnv x v E) defVars (toREval (toRExp e)) v_res M0 <-> *)
(*   eval_exp E defVars (toREval (toRExp (exp_subst e x e_sub))) v_res M0. *)
(* Proof. *)
(*   intros eval_e. *)
(*   revert v_res. *)
(*   induction e. *)
(*   - intros v_res; split; [intros eval_upd | intros eval_subst]. *)
(*     + unfold updEnv in eval_upd. simpl in *. *)
(*       inversion eval_upd; subst. *)
(*       case_eq (n =? x); intros; try auto. *)
(*       * rewrite H in H1. *)
(*         inversion H1; subst; auto. *)
(*       * rewrite H in H1.  *)
(*         eapply Var_load; eauto. *)
(*     + simpl in eval_subst. *)
(*       case_eq (n =? x); intros n_eq_case; *)
(*         rewrite n_eq_case in eval_subst. *)
(*       * simpl. *)
(*         assert (updEnv x v E n = Some v_res). *)
(*         { unfold updEnv; rewrite n_eq_case. *)
(*           f_equal. assert (v = v_res) by (eapply meps_0_deterministic; eauto). rewrite H. auto. } *)
(*         { econstructor; eauto. *)

(*           rewrite n_eq_case; auto. }  *)
(*       * simpl. inversion eval_subst; subst. *)
(*         assert (E n = updEnv x v E n). *)
(*         { unfold updEnv; rewrite n_eq_case; reflexivity. } *)
(*         { rewrite H in H1. eapply Var_load; eauto. rewrite n_eq_case. auto. } *)
(*   - intros v_res; split; [intros eval_upd | intros eval_subst]. *)
(*     + inversion eval_upd; constructor; auto. *)
(*     + inversion eval_subst; constructor; auto. *)
(*   - split; [intros eval_upd | intros eval_subst]. *)
(*     + inversion eval_upd; econstructor; auto; *)
(*         rewrite <- IHe; auto. *)
(*     + inversion eval_subst; constructor; try auto; *)
(*         rewrite IHe; auto. *)
(*   - intros v_res; split; [intros eval_upd | intros eval_subst]. *)
(*     + inversion eval_upd; econstructor; auto; *)
(*       destruct m1; destruct m2; inversion H2. *)
(*       * rewrite <- IHe1; auto. *)
(*       * rewrite <- IHe2; auto. *)
(*     + inversion eval_subst; econstructor; auto; *)
(*         destruct m1; destruct m2; inversion H2. *)
(*       * rewrite IHe1; auto. *)
(*       * rewrite IHe2; auto. *)
(*   - split; [intros eval_upd | intros eval_subst]. *)
(*     + rewrite <- IHe; auto. *)
(*     + rewrite IHe; auto. *)
(* Qed. *)

(* Fixpoint map_subst (f:cmd Q) x e := *)
(*   match f with *)
(*   |Let m y e_y g => Let m y (exp_subst e_y x e) (map_subst g x e) *)
(*   |Ret e_r => Ret (exp_subst e_r x e) *)
(*   end. *)

(* Lemma stepwise_substitution x e v f E vR inVars outVars defVars: *)
(*   ssa (toREvalCmd (toRCmd f)) inVars outVars -> *)
(*   NatSet.In x inVars -> *)
(*   NatSet.Subset (usedVars e) inVars -> *)
(*   eval_exp E defVars (toREval (toRExp e)) v M0 -> *)
(*   bstep (toREvalCmd (toRCmd f)) (updEnv x v E) (fun n => if (n =? x) then Some M0 else defVars n) vR M0 <-> *)
(*   bstep (toREvalCmd (toRCmd (map_subst f x e))) E defVars vR M0. *)
(* Proof. *)
(*   revert E e x vR inVars outVars defVars. *)
(*   induction f; intros E e0 x vR inVars outVars defVars ssa_f x_free valid_vars_e eval_e. *)
(*   - split; [ intros bstep_let | intros bstep_subst]. *)
(*     + inversion bstep_let; subst. *)
(*       inversion ssa_f; subst. *)
(*       assert (forall E var, updEnv n v0 (updEnv x v E) var = updEnv x v (updEnv n v0 E) var). *)
(*       * eapply ssa_shadowing_free. *)
(*         apply ssa_f. *)
(*         apply x_free. *)
(*         eauto. *)
(*       * erewrite (shadowing_free_rewriting_cmd _ _ _ _) in H8; try eauto. *)
(*         simpl in *. *)
(*         econstructor; eauto. *)
(*         { rewrite <- exp_subst_correct; eauto. } *)
(*         { rewrite <- IHf; eauto. *)
(*           admit. *)
(*           rewrite NatSet.add_spec; right; auto. *)
(*           apply validVars_add; auto. *)
(*           eapply dummy_bind_ok; eauto. } *)
(*     + inversion bstep_subst; subst. *)
(*       simpl in *. *)
(*       inversion ssa_f; subst. *)
(*       econstructor; try auto. *)
(*       rewrite exp_subst_correct; eauto. *)
(*       rewrite <- IHf in H8; eauto. *)
(*       * rewrite <- shadowing_free_rewriting_cmd in H8; eauto. *)
(*         admit. *)
(*         (* eapply ssa_shadowing_free; eauto. *) *)
(*         (* rewrite <- exp_subst_correct in H7; eauto. *) *)
(*       * rewrite NatSet.add_spec; auto. *)
(*       * apply validVars_add; auto. *)
(*       * eapply dummy_bind_ok; eauto. *)
(*   - split; [intros bstep_let | intros bstep_subst]. *)
(*     + inversion bstep_let; subst. *)
(*       simpl in *. *)
(*       rewrite exp_subst_correct in H0; eauto. *)
(*       constructor. assumption. *)
(*     + inversion bstep_subst; subst. *)
(*       simpl in *. *)
(*       rewrite <- exp_subst_correct in H0; eauto. *)
(*       econstructor; eauto. *)
(* Admitted. *)

(* Fixpoint let_subst (f:cmd Q) := *)
(*   match f with *)
(*   | Let m x e1 g => *)
(*     exp_subst (let_subst g) x e1 *)
(*   | Ret e1 => e1 *)
(*   end. *)

(* Lemma eval_subst_subexp E e' n e vR defVars: *)
(*   NatSet.In n (usedVars e) -> *)
(*   eval_exp E defVars (toREval (toRExp (exp_subst e n e'))) vR M0 -> *)
(*   exists v, eval_exp E defVars (toREval (toRExp e')) v M0. *)
(* Proof. *)
(*   revert E e' n vR. *)
(*   induction e; *)
(*   intros E e' n' vR n_fVar eval_subst; simpl in *; try eauto. *)
(*   - case_eq (n =? n'); intros case_n; rewrite case_n in *; eauto. *)
(*     rewrite NatSet.singleton_spec in n_fVar. *)
(*     exfalso. *)
(*     rewrite Nat.eqb_neq in case_n. *)
(*     apply case_n; auto. *)
(*   - inversion n_fVar. *)
(*   - inversion eval_subst; subst; *)
(*       eapply IHe; eauto. *)
(*   - inversion eval_subst; subst. *)
(*     rewrite NatSet.union_spec in n_fVar. *)
(*     destruct m1; destruct m2; inversion H2. *)
(*     destruct n_fVar as [n_fVar_e1 | n_fVare2]; *)
(*       [eapply IHe1; eauto | eapply IHe2; eauto]. *)
(* Qed. *)

(* Lemma bstep_subst_subexp_any E e x f vR defVars: *)
(*   NatSet.In x (liveVars f) -> *)
(*   bstep (toREvalCmd (toRCmd (map_subst f x e))) E defVars vR M0 -> *)
(*   exists E' v, eval_exp E' defVars (toREval (toRExp e)) v M0. *)
(* Proof. *)
(*   revert E e x vR defVars; *)
(*     induction f; *)
(*     intros E e' x vR defVars x_live eval_f. *)
(*   - inversion eval_f; subst. *)
(*     simpl in x_live. *)
(*     rewrite NatSet.union_spec in x_live. *)
(*     destruct x_live as [x_used | x_live]. *)
(*     + exists E. eapply eval_subst_subexp; eauto. *)
(*     + eapply IHf; eauto. *)
(* Admitted. *)
(*   - simpl in *. *)
(*     inversion eval_f; subst. *)
(*     exists E. eapply eval_subst_subexp; eauto. *)
(* Qed. *)

(* Lemma bstep_subst_subexp_ret E e x e' vR defVars: *)
(*   NatSet.In x (liveVars (Ret e')) -> *)
(*   bstep (toREvalCmd (toRCmd (map_subst (Ret e') x e))) E defVars vR M0 -> *)
(*   exists v, eval_exp E defVars (toREval (toRExp e)) v M0. *)
(* Proof. *)
(*   simpl; intros x_live bstep_ret. *)
(*   inversion bstep_ret; subst. *)
(*   eapply eval_subst_subexp; eauto. *)
(* Qed. *)

(* Lemma no_forward_refs V (f:cmd V) inVars outVars: *)
(*   ssa f inVars outVars -> *)
(*   forall v, NatSet.In v (definedVars f) -> *)
(*        NatSet.mem v inVars = false. *)
(* Proof. *)
(*   intros ssa_f; induction ssa_f; simpl. *)
(*   - intros v v_defVar. *)
(*     rewrite NatSet.add_spec in v_defVar. *)
(*     destruct v_defVar as [v_x | v_defVar]. *)
(*     + subst; auto. *)
(*     + specialize (IHssa_f v v_defVar). *)
(*       case_eq (NatSet.mem v inVars); intros mem_inVars; try auto. *)
(*       assert (NatSet.mem v (NatSet.add x inVars) = true) by (rewrite NatSet.mem_spec, NatSet.add_spec, <- NatSet.mem_spec; auto). *)
(*       congruence. *)
(*   - intros v v_in_empty; inversion v_in_empty. *)
(* Qed. *)

(* Lemma bstep_subst_subexp_let E e x y e' g vR m defVars: *)
(*   NatSet.In x (liveVars (Let m y e' g)) -> *)
(*   (forall x, NatSet.In x (usedVars e) -> *)
(*         exists v, E x = v) -> *)
(*   bstep (toREvalCmd (toRCmd (map_subst (Let m y e' g) x e))) E defVars vR M0 -> *)
(*   exists v, eval_exp E defVars (toREval (toRExp e)) v M0. *)
(* Proof. *)
(*   revert E e x y e' vR; *)
(*     induction g; *)
(*     intros E e0 x y e' vR x_live uedVars_def bstep_f. *)
(*   - simpl in *. *)
(*     inversion bstep_f; subst. *)
(*     specialize (IHg (updEnv y m v E) e0 x n e). *)
(*     rewrite NatSet.union_spec in x_live. *)
(*     destruct x_live as [x_used | x_live]. *)
(*     + eapply eval_subst_subexp; eauto. *)
(*     + edestruct IHg as [v0 eval_v0]; eauto. *)
(* Admitted. *)

(* Theorem let_free_agree f E vR inVars outVars e defVars: *)
(*   ssa f inVars outVars -> *)
(*   (forall v, NatSet.In v (definedVars f) -> *)
(*         NatSet.In v (liveVars f)) -> *)
(*   let_subst f = e -> *)
(*   bstep (toREvalCmd (toRCmd (Ret e))) E defVars vR M0 -> *)
(*   bstep (toREvalCmd (toRCmd f)) E defVars vR M0. *)
(* Proof. *)
(*   intros ssa_f. *)
(*   revert E vR e; *)
(*     induction ssa_f; *)
(*     intros E vR e_subst dVars_live subst_step bstep_e; *)
(*     simpl in *. *)
(*   (* Let Case, prove that the value of the let binding must be used somewhere *) *)
(*   - case_eq (let_subst s). *)
(*     + intros e0 subst_s; rewrite subst_s in *. *)
(* Admitted. *)
(*       (*inversion subst_step; subst. *)
(*       clear subst_s subst_step. *)
(*       inversion bstep_e; subst. *)
(*       specialize (dVars_live x). *)
(*       rewrite NatSet.add_spec in dVars_live. *)
(*       assert (NatSet.In x (NatSet.union (usedVars e) (liveVars s))) *)
(*         as x_used_or_live *)
(*           by (apply dVars_live; auto). *)
(*       rewrite NatSet.union_spec in x_used_or_live. *)
(*       destruct x_used_or_live as [x_used | x_live]. *)
(*       * specialize (H x x_used). *)
(*         rewrite <- NatSet.mem_spec in H; congruence. *)
(*       * *)

(*     eapply let_b. *)
(*     Focus 2. *)
(*     eapply IHssa_f; try auto. *) *)

(* Theorem let_free_form f E vR inVars outVars e defVars: *)
(*   ssa f inVars outVars -> *)
(*   bstep (toREvalCmd (toRCmd f)) E defVars vR M0 -> *)
(*   let_subst f = e -> *)
(*   bstep (toREvalCmd (toRCmd (Ret e))) E defVars vR M0. *)
(* Proof. *)
(*     revert E vR inVars outVars e; *)
(*     induction f; *)
(*     intros E vR inVars outVars e_subst ssa_f bstep_f subst_step. *)
(*   - simpl. *)
(*     inversion bstep_f; subst. *)
(*     inversion ssa_f; subst. *)
(* Admitted. *)
(* (* case_eq (let_subst f). *)
(*     + intros f_subst subst_f_eq. *)
(*       specialize (IHf (updEnv n M0 v E) vR (NatSet.add n inVars) outVars f_subst H9 H7 subst_f_eq). *)
(*       rewrite subst_f_eq in subst_step. *)
(*       inversion IHf; subst. *)
(*       constructor. *)
(*       inversion subst_step. *)
(*       subst. *)
(*       rewrite <- exp_subst_correct; eauto. *)
(*     + intros subst_eq; rewrite subst_eq in subst_step; inversion subst_step. *)
(*   - inversion bstep_f; subst. *)
(*     constructor. *)
(*     simpl in *. *)
(*     inversion subst_step; subst. *)
(*     assumption. *)
(* Qed. *)
(*  *) *)

(* (* *)
(* Lemma ssa_non_stuck (f:cmd Q) (inVars outVars:NatSet.t) (VarEnv:var_env) *)
(*       (ParamEnv:param_env) (P:precond) (eps:R): *)
(*   ssa Q f inVars outVars -> *)
(*   (forall v, NatSet.In v (freeVars e) -> *)
(*         (Q2R (ivlo (P v)) <= ParamEnv v <= Q2R (ivhi (P v))))%R -> *)
(*   (forall v, NatSet.In v inVars -> *)
(*         exists r, VarEnv v = Some r) -> *)
(*   exists vR, *)
(*     bstep (toRCmd f) VarEnv ParamEnv P eps (Nop R) vR. *)
(* Proof. *)
(*   intros ssa_f; revert VarEnv ParamEnv P eps. *)
(*   induction ssa_f; *)
(*     intros VarEnv ParamEnv P eps fVars_live. *)
(*   - *)
(*  *) *)