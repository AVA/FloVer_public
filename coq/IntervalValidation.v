(**
    Interval arithmetic checker and its soundness proof.
    The function validIntervalbounds checks wether the given analysis result is
    a valid range arithmetic for each sub term of the given expression e.
    The computation is done using our formalized interval arithmetic.
    The function is used in CertificateChecker.v to build the full checker.
**)
Require Import Coq.QArith.QArith Coq.QArith.Qreals QArith.Qminmax Coq.Lists.List Coq.micromega.Psatz.
Require Import FloVer.Infra.Abbrevs FloVer.Infra.RationalSimps FloVer.Infra.RealRationalProps.
Require Import FloVer.Infra.Ltacs FloVer.Infra.RealSimps FloVer.Typing.
Require Export FloVer.IntervalArithQ FloVer.IntervalArith FloVer.ssaPrgs.

Fixpoint validIntervalbounds (e:exp Q) (absenv:analysisResult) (P:precond) (validVars:NatSet.t) :=
  let (intv, _) := absenv e in
    match e with
    | Var _ v =>
      if NatSet.mem v validVars
      then true
      else isSupersetIntv (P v) intv && (Qleb (ivlo (P v)) (ivhi (P v)))
    | Const _ n => isSupersetIntv (n,n) intv
    | Unop o f =>
      if validIntervalbounds f absenv P validVars
      then
        let (iv, _) := absenv f in
        match o with
        | Neg =>
          let new_iv := negateIntv iv in
          isSupersetIntv new_iv intv
        | Inv =>
          if (((Qleb (ivhi iv) 0) && (negb (Qeq_bool (ivhi iv) 0))) ||
              ((Qleb 0 (ivlo iv)) && (negb (Qeq_bool (ivlo iv) 0))))
          then
            let new_iv := invertIntv iv in
            isSupersetIntv new_iv intv
          else false
        end
      else false
    | Binop op f1 f2 =>
      if ((validIntervalbounds f1 absenv P validVars) &&
          (validIntervalbounds f2 absenv P validVars))
      then
        let (iv1,_) := absenv f1 in
        let (iv2,_) := absenv f2 in
          match op with
          | Plus =>
            let new_iv := addIntv iv1 iv2 in
            isSupersetIntv new_iv intv
          | Sub =>
            let new_iv := subtractIntv iv1 iv2 in
            isSupersetIntv new_iv intv
          | Mult =>
            let new_iv := multIntv iv1 iv2 in
            isSupersetIntv new_iv intv
          | Div =>
            if (((Qleb (ivhi iv2) 0) && (negb (Qeq_bool (ivhi iv2) 0))) ||
                ((Qleb 0 (ivlo iv2)) && (negb (Qeq_bool (ivlo iv2) 0))))
            then
              let new_iv := divideIntv iv1 iv2 in
              isSupersetIntv new_iv intv
            else false
          end
      else false
    | Downcast _ f1 =>
      if (validIntervalbounds f1 absenv P validVars)
      then
        let (iv1, _) := absenv f1 in
        (* TODO: intv = iv1 might be a hard constraint... *)
        (isSupersetIntv intv iv1) && (isSupersetIntv iv1 intv)
      else
        false
    end.

Fixpoint validIntervalboundsCmd (f:cmd Q) (absenv:analysisResult) (P:precond) (validVars:NatSet.t) :bool:=
  match f with
  | Let m x e g =>
    if (validIntervalbounds e absenv P validVars &&
        Qeq_bool (fst (fst (absenv e))) (fst (fst (absenv (Var Q x)))) &&
        Qeq_bool (snd (fst (absenv e))) (snd (fst (absenv (Var Q x)))))
    then validIntervalboundsCmd g absenv P (NatSet.add x validVars)
    else false
  |Ret e =>
   validIntervalbounds e absenv P validVars
  end.

Theorem ivbounds_approximatesPrecond_sound f absenv P V:
  validIntervalbounds f absenv P V = true ->
  forall v, NatSet.In v (NatSet.diff (Expressions.usedVars f) V) ->
         Is_true(isSupersetIntv (P v) (fst (absenv (Var Q v)))).
Proof.
  induction f; unfold validIntervalbounds.
  - simpl. intros approx_true v v_in_fV; simpl in *.
    rewrite NatSet.diff_spec in v_in_fV.
    rewrite NatSet.singleton_spec in v_in_fV;
      destruct v_in_fV; subst.
    destruct (absenv (Var Q n)); simpl in *.
    case_eq (NatSet.mem n V); intros case_mem;
      rewrite case_mem in approx_true; simpl in *.
    + rewrite NatSet.mem_spec in case_mem.
      contradiction.
    + apply Is_true_eq_left in approx_true.
      apply andb_prop_elim in approx_true.
      destruct approx_true; auto.
  - intros approx_true v0 v_in_fV; simpl in *.
    inversion v_in_fV.
  - intros approx_unary_true v v_in_fV; simpl in *.
    apply Is_true_eq_left in approx_unary_true.
    simpl in *.
    destruct (absenv (Unop u f)); destruct (absenv f); simpl in *.
    apply andb_prop_elim in approx_unary_true.
    destruct approx_unary_true.
    apply IHf; try auto.
    apply Is_true_eq_true; auto.
  - intros approx_bin_true v v_in_fV.
    simpl in v_in_fV.
    rewrite NatSet.diff_spec in v_in_fV.
    destruct v_in_fV as [ v_in_fV v_not_in_V].
    rewrite NatSet.union_spec in v_in_fV.
    apply Is_true_eq_left in approx_bin_true.
    destruct (absenv (Binop b f1 f2)); destruct (absenv f1);
      destruct (absenv f2); simpl in *.
    apply andb_prop_elim in approx_bin_true.
    destruct approx_bin_true.
    apply andb_prop_elim in H.
    destruct H.
    destruct v_in_fV.
    + apply IHf1; try auto.
      * apply Is_true_eq_true; auto.
      * rewrite NatSet.diff_spec; split; auto.
    + apply IHf2; try auto.
      * apply Is_true_eq_true; auto.
      * rewrite NatSet.diff_spec; split; auto.
  - intros approx_rnd_true v v_in_fV.
    simpl in *; destruct (absenv (Downcast m f)); destruct (absenv f).
    apply Is_true_eq_left in approx_rnd_true.
    apply andb_prop_elim in approx_rnd_true.
    destruct approx_rnd_true.
    apply IHf; auto.
    apply Is_true_eq_true in H; try auto.
Qed.

Corollary Q2R_max4 a b c d:
  Q2R (IntervalArithQ.max4 a b c d) = max4 (Q2R a) (Q2R b) (Q2R c) (Q2R d).
Proof.
  unfold IntervalArithQ.max4, max4; repeat rewrite Q2R_max; auto.
Qed.

Corollary Q2R_min4 a b c d:
  Q2R (IntervalArithQ.min4 a b c d) = min4 (Q2R a) (Q2R b) (Q2R c) (Q2R d).
Proof.
  unfold IntervalArith.min4, min4; repeat rewrite Q2R_min; auto.
Qed.

Ltac env_assert absenv e name :=
  assert (exists iv err, absenv e = (iv,err)) as name by (destruct (absenv e); repeat eexists; auto).


Lemma validBoundsDiv_uneq_zero e1 e2 absenv P V ivlo_e2 ivhi_e2 err:
  absenv e2 = ((ivlo_e2,ivhi_e2), err) ->
  validIntervalbounds (Binop Div e1 e2) absenv P V = true ->
  (ivhi_e2 < 0) \/ (0 < ivlo_e2).
Proof.
  intros absenv_eq validBounds.
  unfold validIntervalbounds in validBounds.
  env_assert absenv (Binop Div e1 e2) abs_div; destruct abs_div as [iv_div [err_div abs_div]].
  env_assert absenv e1 abs_e1; destruct abs_e1 as [iv_e1 [err_e1 abs_e1]].
  rewrite abs_div, abs_e1, absenv_eq in validBounds.
  repeat (rewrite <- andb_lazy_alt in validBounds).
  apply Is_true_eq_left in validBounds.
  apply andb_prop_elim in validBounds.
  destruct validBounds as [_ validBounds]; apply andb_prop_elim in validBounds.
  destruct validBounds as [nodiv0 _].
  apply Is_true_eq_true in nodiv0.
  unfold isSupersetIntv in *; simpl in *.
  apply le_neq_bool_to_lt_prop; auto.
Qed.

Theorem validIntervalbounds_sound (f:exp Q) (absenv:analysisResult) (P:precond)
        fVars dVars (E:env) Gamma:
    validIntervalbounds f absenv P dVars = true ->
    (forall v, NatSet.mem v dVars = true ->
          exists vR, E v = Some vR /\
                (Q2R (fst (fst (absenv (Var Q v)))) <= vR <= Q2R (snd (fst (absenv (Var Q v)))))%R) ->
    NatSet.Subset (NatSet.diff (Expressions.usedVars f) dVars) fVars ->
    (forall v, NatSet.mem v fVars = true ->
          exists vR, E v = Some vR /\
                (Q2R (fst (P v)) <= vR <= Q2R (snd (P v)))%R) ->
  (forall v, NatSet.mem v (NatSet.union fVars dVars) = true ->
        exists m :mType, Gamma v = Some m) ->
    exists vR, eval_exp E (toRMap Gamma) (toREval (toRExp f)) vR M0 /\
          (Q2R (fst (fst (absenv f))) <= vR <= Q2R (snd (fst (absenv f))))%R.
Proof.
  induction f;
    intros valid_bounds valid_definedVars usedVars_subset valid_freeVars types_defined;
    simpl in *.
  - env_assert absenv (Var Q n) absenv_var.
    destruct absenv_var as [iv_n [err_n absenv_var]].
    case_eq (NatSet.mem n dVars); intros dVars_case;
      rewrite dVars_case, absenv_var in valid_bounds; simpl in *.
    + destruct (valid_definedVars n) as [vR [env_valid bounds_valid]]; try auto.
      eexists; split; simpl; try eauto.
      eapply Var_load; simpl; try auto.
      destruct (types_defined n) as [m type_m];
        set_tac.
      match_simpl; auto.
    + destruct (valid_freeVars n) as [vR [env_valid bounds_valid]]; set_tac; try auto.
      assert (NatSet.In n fVars) by set_tac.
      eexists; split; [econstructor | ]; eauto.
      * destruct (types_defined n) as [m type_m]; set_tac.
        match_simpl; auto.
      * andb_to_prop valid_bounds.
        unfold Qleb in *.
        canonize_hyps.
        rewrite absenv_var.
        simpl in *.
        lra.
  - exists (perturb (Q2R v) 0).
    split; [econstructor; eauto; apply Rabs_0_equiv | ].
    env_assert absenv (Const m v) absenv_const;
      destruct absenv_const as [iv_v [err_v absenv_const]].
    rewrite absenv_const in *; simpl in *.
    andb_to_prop valid_bounds.
    canonize_hyps.
    simpl in *; unfold perturb; lra.
  - env_assert absenv (Unop u f) absenv_unop;
      destruct absenv_unop as [iv_u [err_u absenv_unop]].
    rewrite absenv_unop in *; simpl in *.
    case_eq (validIntervalbounds f absenv P dVars);
      intros case_bounds; rewrite case_bounds in *; try congruence.
    env_assert absenv f absenv_f;
      destruct absenv_f as [iv_f [ err_f absenv_f]]; rewrite absenv_f in *; simpl in *.
    destruct IHf as [vF [eval_f valid_bounds_f]]; try auto.
    destruct u.
    + exists (evalUnop Neg vF); split; [econstructor; auto | ].
      (* TODO: Make lemma *)
      unfold isSupersetIntv in valid_bounds;
        andb_to_prop valid_bounds.
      canonize_hyps.
      simpl in *.
      rewrite Q2R_opp in *.
      lra.
    + andb_to_prop valid_bounds.
      rename L into nodiv0.
      apply le_neq_bool_to_lt_prop in nodiv0.
      exists (perturb (evalUnop Inv vF) 0); split; [econstructor; eauto; try apply Rabs_0_equiv | ].
      * (* Absence of division by zero *)
        hnf. destruct nodiv0 as [nodiv0 | nodiv0]; apply Qlt_Rlt in nodiv0;
               rewrite Q2R0_is_0 in nodiv0; lra.
      * canonize_hyps.
        pose proof (interval_inversion_valid (Q2R (fst iv_f),(Q2R (snd iv_f))) (a :=vF)) as inv_valid.
         unfold invertInterval in inv_valid; simpl in *.
         rewrite delta_0_deterministic; [| rewrite Rbasic_fun.Rabs_R0; lra].
         destruct inv_valid; try auto.
         { destruct nodiv0; rewrite <- Q2R0_is_0; [left | right]; apply Qlt_Rlt; auto. }
         { split; eapply Rle_trans.
           - apply L0.
           - rewrite Q2R_inv; try auto.
             destruct nodiv0; try lra.
             hnf; intros.
             assert (0 < Q2R (snd iv_f))%R.
             { eapply Rlt_le_trans.
               apply Qlt_Rlt in H1.
               rewrite <- Q2R0_is_0.
               apply H1. lra.
             }
             rewrite <- Q2R0_is_0 in H3.
             apply Rlt_Qlt in H3.
             lra.
           - apply H0.
           - rewrite <- Q2R_inv; try auto.
             hnf; intros.
             destruct nodiv0; try lra.
             assert (Q2R (fst iv_f) < 0)%R.
             { eapply Rle_lt_trans.
               Focus 2.
               rewrite <- Q2R0_is_0;
                 apply Qlt_Rlt in H2; apply H2.
               lra.
             }
             rewrite <- Q2R0_is_0 in H3.
             apply Rlt_Qlt in H3. lra. }
  - env_assert absenv (Binop b f1 f2) absenv_b;
      destruct absenv_b as [iv_b [err_b absenv_b]].
    env_assert absenv f1 absenv_f1;
      destruct absenv_f1 as [iv_f1 [err_f1 absenv_f1]].
    env_assert absenv f2 absenv_f2;
      destruct absenv_f2 as [iv_f2 [err_f2 absenv_f2]].
    rewrite absenv_b in *; simpl in *.
    andb_to_prop valid_bounds.
    destruct IHf1 as [vF1 [eval_f1 valid_f1]]; try auto; set_tac.
    destruct IHf2 as [vF2 [eval_f2 valid_f2]]; try auto; set_tac.
    rewrite absenv_f1, absenv_f2 in *; simpl in *.
    assert (M0 = join M0 M0) as M0_join by (cbv; auto);
      rewrite M0_join.
    exists (perturb (evalBinop b vF1 vF2) 0);
      destruct b; simpl in *.
    * split;
        [econstructor; try congruence; apply Rabs_0_equiv | ].
      pose proof (interval_addition_valid (Q2R (fst iv_f1),Q2R (snd iv_f1)) (Q2R (fst iv_f2), Q2R (snd iv_f2))) as valid_add.
      specialize (valid_add vF1 vF2 valid_f1 valid_f2).
      unfold isSupersetIntv in R.
      andb_to_prop R.
      canonize_hyps.
      simpl in *.
      repeat rewrite <- Q2R_plus in *;
        rewrite <- Q2R_min4, <- Q2R_max4 in *.
      unfold perturb.
      lra.
    * split;
        [econstructor; try congruence; apply Rabs_0_equiv |].
      pose proof (interval_subtraction_valid (Q2R (fst iv_f1),Q2R (snd iv_f1)) (Q2R (fst iv_f2), Q2R (snd iv_f2))) as valid_sub.
      specialize (valid_sub vF1 vF2 valid_f1 valid_f2).
      unfold isSupersetIntv in R.
      andb_to_prop R.
      canonize_hyps.
      simpl in *.
      repeat rewrite <- Q2R_opp in *;
        repeat rewrite <- Q2R_plus in *;
        rewrite <- Q2R_min4, <- Q2R_max4 in *.
      unfold perturb.
      lra.
    * split;
        [ econstructor; try congruence; apply Rabs_0_equiv |].
      pose proof (interval_multiplication_valid (Q2R (fst iv_f1),Q2R (snd iv_f1)) (Q2R (fst iv_f2), Q2R (snd iv_f2))) as valid_mul.
      specialize (valid_mul vF1 vF2 valid_f1 valid_f2).
      unfold isSupersetIntv in R.
      andb_to_prop R.
      canonize_hyps.
      simpl in *.
      repeat rewrite <- Q2R_mult in *;
        rewrite <- Q2R_min4, <- Q2R_max4 in *.
      unfold perturb.
      lra.
    * andb_to_prop R.
      canonize_hyps.
      apply le_neq_bool_to_lt_prop in L.
      split;
        [ econstructor; try congruence; try apply Rabs_0_equiv | ].
      (* No division by zero proof *)
      { hnf; intros.
        destruct L as [L | L]; apply Qlt_Rlt in L; rewrite Q2R0_is_0 in L; lra. }
      { pose proof (interval_division_valid (a:=vF1) (b:=vF2) (Q2R (fst iv_f1), Q2R (snd iv_f1)) (Q2R (fst iv_f2),Q2R (snd iv_f2))) as valid_div.
        simpl in *.
        destruct valid_div; try auto.
        - destruct L; rewrite <- Q2R0_is_0; [left | right]; apply Qlt_Rlt; auto.
        - assert (~ snd iv_f2 == 0).
          { hnf; intros. destruct L; try lra.
            assert (0 < snd iv_f2) by (apply Rlt_Qlt; apply Qlt_Rlt in H2; lra).
            lra. }
          assert (~ fst iv_f2 == 0).
          { hnf; intros; destruct L; try lra.
            assert (fst iv_f2 < 0) by (apply Rlt_Qlt; apply Qlt_Rlt in H3; lra).
            lra. }
          repeat (rewrite <- Q2R_inv in *; try auto).
          repeat rewrite <- Q2R_mult in *.
          rewrite <- Q2R_min4, <- Q2R_max4 in *.
          unfold perturb.
          lra.
      }
  - env_assert absenv (Downcast m f) absenv_d;
      destruct absenv_d as [iv_d [err_d absenv_d]];
      env_assert absenv f absenv_f;
      destruct absenv_f as [iv_f [err_f absenv_f]];
      rewrite absenv_d, absenv_f in *; simpl in *.
    andb_to_prop valid_bounds.
    destruct IHf as [vF [eval_f valid_f]]; try auto.
    exists (perturb vF 0).
    split; [try econstructor; try eauto; try apply Rabs_0_equiv; unfold isMorePrecise; auto |].
    canonize_hyps.
    unfold perturb; simpl in *.
    lra.
Qed.

Lemma Rmap_updVars_comm Gamma n m:
  forall x,
    updDefVars n M0 (toRMap Gamma) x = toRMap (updDefVars n m Gamma) x.
Proof.
  unfold updDefVars, toRMap; simpl.
  intros x; destruct (x =? n); try auto.
Qed.

(* Lemma eval_exp_Rmap_updVars_comm e n Gamma E v: *)
(*   eval_exp E (toRMap (updDefVars n M0 Gamma)) (toREval (toRExp e)) v M0 -> *)
(*   eval_exp E (updDefVars n M0 (toRMap Gamma)) (toREval (toRExp e)) v M0. *)
(* Proof. *)
(*   revert v; *)
(*     induction e; intros * eval_e; inversion eval_e; subst; simpl in *; *)
(*       auto. *)
(*   - constructor; try auto. *)
(*     erewrite test3; eauto. *)
(*   - rewrite H2 in *. *)
(*     apply M0_join_is_M0 in H2. *)
(*     destruct H2; subst. *)
(*     eauto. *)
(*   - apply M0_least_precision in H1; subst. *)
(*     econstructor; try eauto. *)
(*     apply isMorePrecise_refl. *)
(* Qed. *)

Lemma swap_Gamma_eval_exp e E vR m Gamma1 Gamma2:
  (forall n, Gamma1 n = Gamma2 n) ->
  eval_exp E Gamma1 e vR m ->
  eval_exp E Gamma2 e vR m.
Proof.
  revert E vR Gamma1 Gamma2 m;
    induction e; intros * Gamma_eq eval_e;
      inversion eval_e; subst; simpl in *;
        try eauto.
  apply Var_load; try auto.
  rewrite <- Gamma_eq; auto.
Qed.

Lemma swap_Gamma_bstep f E vR m Gamma1 Gamma2 :
  (forall n, Gamma1 n = Gamma2 n) ->
  bstep f E Gamma1 vR m ->
  bstep f E Gamma2 vR m.
Proof.
  revert E Gamma1 Gamma2;
  induction f; intros * Gamma_eq eval_f.
  - inversion eval_f; subst.
    econstructor; try eauto.
    +  eapply swap_Gamma_eval_exp; eauto.
    + apply (IHf _ (updDefVars n m0 Gamma1) _); try eauto.
      intros n1.
      unfold updDefVars.
      case (n1 =? n); auto.
  - inversion eval_f; subst.
    econstructor; try eauto.
    eapply swap_Gamma_eval_exp; eauto.
Qed.

Theorem validIntervalboundsCmd_sound (f:cmd Q) (absenv:analysisResult) Gamma:
  forall E fVars dVars outVars elo ehi err P,
    ssa f (NatSet.union fVars dVars) outVars ->
    (forall v, NatSet.mem v dVars = true ->
          exists vR, E v = Some vR /\
            (Q2R (fst (fst (absenv (Var Q v)))) <= vR <= Q2R (snd (fst (absenv (Var Q v)))))%R) ->
    (forall v, NatSet.mem v fVars = true ->
          exists vR, E v = Some vR /\
            (Q2R (fst (P v)) <= vR <= Q2R (snd (P v)))%R) ->
  (forall v, NatSet.mem v (NatSet.union fVars dVars) = true ->
        exists m :mType, Gamma v = Some m) ->
    NatSet.Subset (NatSet.diff (Commands.freeVars f) dVars) fVars ->
    validIntervalboundsCmd f absenv P dVars = true ->
    absenv (getRetExp f) = ((elo, ehi), err) ->
    exists vR,
      bstep (toREvalCmd (toRCmd f)) E (toRMap Gamma) vR M0 /\
      (Q2R elo <=  vR <= Q2R ehi)%R.
Proof.
  revert Gamma.
  induction f;
    intros *  ssa_f dVars_sound fVars_valid types_valid usedVars_subset valid_bounds_f absenv_f.
  - inversion ssa_f; subst.
    unfold validIntervalboundsCmd in valid_bounds_f.
    andb_to_prop valid_bounds_f.
    inversion ssa_f; subst.
    pose proof (validIntervalbounds_sound e absenv P E Gamma (fVars:=fVars) L) as validIV_e.
    destruct validIV_e as [v [eval_e valid_bounds_e]]; try auto.
    { simpl in usedVars_subset.
      hnf. intros a in_usedVars.
      apply usedVars_subset.
      rewrite NatSet.diff_spec, NatSet.remove_spec, NatSet.union_spec.
      rewrite NatSet.diff_spec in in_usedVars.
      destruct in_usedVars as [ in_usedVars not_dVar].
      repeat split; try auto.
      hnf; intros; subst.
      set_tac. }
    specialize (IHf (updDefVars n M0 Gamma) (updEnv n v E) fVars (NatSet.add n dVars)).
    assert (NatSet.Equal (NatSet.add n (NatSet.union fVars dVars)) (NatSet.union fVars (NatSet.add n dVars))).
    + hnf. intros a; split; intros in_set.
      * rewrite NatSet.add_spec, NatSet.union_spec in in_set.
        rewrite NatSet.union_spec, NatSet.add_spec.
        destruct in_set as [P1 | [ P2 | P3]]; auto.
      * rewrite NatSet.add_spec, NatSet.union_spec.
        rewrite NatSet.union_spec, NatSet.add_spec in in_set.
        destruct in_set as [P1 | [ P2 | P3]]; auto.
    + edestruct IHf as [vR [eval_f valid_bounds_f]]; try eauto.
       eapply ssa_equal_set. symmetry in H. apply H. apply H7.
      * intros v0 mem_v0.
        unfold updEnv.
        case_eq (v0 =? n); intros v0_eq.
        { rename R1 into eq_lo;
            rename R0 into eq_hi.
          apply Qeq_bool_iff in eq_lo;
            apply Qeq_eqR in eq_lo.
          apply Qeq_bool_iff in eq_hi;
            apply Qeq_eqR in eq_hi.
          rewrite Nat.eqb_eq in v0_eq; subst.
          rewrite <- eq_lo, <- eq_hi.
          exists v; split; auto. }
        { apply dVars_sound. rewrite NatSet.mem_spec.
          rewrite NatSet.mem_spec in mem_v0.
          rewrite NatSet.add_spec in mem_v0.
          destruct mem_v0; try auto.
          rewrite Nat.eqb_neq in v0_eq.
          exfalso; apply v0_eq; auto. }
      * intros v0 mem_fVars.
        unfold updEnv.
        case_eq (v0 =? n); intros case_v0; auto.
        rewrite Nat.eqb_eq in case_v0; subst.
        assert (NatSet.mem n (NatSet.union fVars dVars) = true) as in_union.
        { rewrite NatSet.mem_spec, NatSet.union_spec; rewrite <- NatSet.mem_spec; auto. }
        { rewrite in_union in *; congruence. }
      * intros x x_contained.
        set_tac.
        rewrite NatSet.union_spec in x_contained.
        destruct x_contained as [x_free | x_def].
        { destruct (types_valid x) as [m_x Gamma_x]; set_tac.
          exists m_x.
          unfold updDefVars. case_eq (x =? n); intros eq_case; try auto.
          rewrite Nat.eqb_eq in eq_case.
          subst.
          exfalso; apply H6; set_tac. }
        { rewrite NatSet.add_spec in x_def.
          destruct x_def as [x_n | x_def]; subst.
          - exists M0; unfold updDefVars; rewrite Nat.eqb_refl; auto.
          - destruct (types_valid x) as [m_x Gamma_x]; set_tac.
            exists m_x.
            unfold updDefVars; case_eq (x =? n); intros eq_case; try auto.
            rewrite Nat.eqb_eq in eq_case; subst.
            exfalso; apply H6; set_tac. }
      * clear L R1 R0 R IHf.
        hnf. intros a a_freeVar.
        rewrite NatSet.diff_spec in a_freeVar.
        destruct a_freeVar as [a_freeVar a_no_dVar].
        apply usedVars_subset.
        simpl.
        rewrite NatSet.diff_spec, NatSet.remove_spec, NatSet.union_spec.
        repeat split; try auto.
        { hnf; intros; subst.
          apply a_no_dVar.
          rewrite NatSet.add_spec; auto. }
        { hnf; intros a_dVar.
          apply a_no_dVar.
          rewrite NatSet.add_spec; auto. }
      * simpl. exists vR; split; [econstructor; eauto | auto].
        eapply swap_Gamma_bstep with (Gamma1 := toRMap (updDefVars n M0 Gamma)) ; try eauto.
        intros n1; erewrite Rmap_updVars_comm; eauto.
  - unfold validIntervalboundsCmd in valid_bounds_f.
    pose proof (validIntervalbounds_sound e absenv P E Gamma valid_bounds_f dVars_sound usedVars_subset) as valid_iv_e.
    destruct valid_iv_e as [vR [eval_e valid_e]]; try auto.
    simpl in *; rewrite absenv_f in *; simpl in *.
    exists vR; split; [econstructor; eauto | auto].
Qed.