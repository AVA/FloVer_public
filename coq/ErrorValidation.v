(**
   This file contains the coq implementation of the error bound validator as
   well as its soundness proof. The function validErrorbound is the Error bound
   validator from the certificate checking process. Under the assumption that a
   valid range arithmetic result has been computed, it can validate error bounds
   encoded in the analysis result. The validator is used in the file
   CertificateChecker.v to build the complete checker.
 **)
Require Import Coq.QArith.QArith Coq.QArith.Qminmax Coq.QArith.Qabs
               Coq.QArith.Qreals Coq.micromega.Psatz Coq.Reals.Reals.
Require Import FloVer.Infra.Abbrevs FloVer.Infra.RationalSimps
               FloVer.Infra.RealRationalProps FloVer.Infra.RealSimps
               FloVer.Infra.Ltacs FloVer.Environments
               FloVer.IntervalValidation FloVer.Typing FloVer.ErrorBounds.

(** Error bound validator **)
Fixpoint validErrorbound (e:exp Q) (* analyzed expression *)
         (typeMap:exp Q -> option mType) (* derived types for e *)
         (A:analysisResult) (* encoded result of FloVer *)
         (dVars:NatSet.t) (* let-bound variables encountered previously *):=
  let (intv, err) := (A e) in
  let mopt := typeMap e in
  match mopt with
  | None => false
  | Some m =>
    if (Qleb 0 err) (* encoding soundness: errors are positive *)
    then
      match e with (* case analysis for the expression *)
      |Var _ v =>
       if (NatSet.mem v dVars)
       then true (* defined variables are checked at definition point *)
       else (Qleb (maxAbs intv * (mTypeToQ m)) err)
      |Const _ n =>
       Qleb (maxAbs intv * (mTypeToQ m)) err
      |Unop Neg e1 =>
       if (validErrorbound e1 typeMap A dVars)
       then Qeq_bool err (snd (A e1))
       else false
      |Unop Inv e1 => false
      |Binop b e1 e2 =>
       if ((validErrorbound e1 typeMap A dVars)
             && (validErrorbound e2 typeMap A dVars))
       then
         let (ive1, err1) := A e1 in
         let (ive2, err2) := A e2 in
         let errIve1 := widenIntv ive1 err1 in
         let errIve2 := widenIntv ive2 err2 in
         let upperBoundE1 := maxAbs ive1 in
         let upperBoundE2 := maxAbs ive2 in
         match b with
         | Plus =>
           Qleb (err1 + err2 + (maxAbs (addIntv errIve1 errIve2)) * (mTypeToQ m)) err
         | Sub =>
           Qleb (err1 + err2 + (maxAbs (subtractIntv errIve1 errIve2)) * (mTypeToQ m)) err
         | Mult =>
           Qleb ((upperBoundE1 * err2 + upperBoundE2 * err1 + err1 * err2)  + (maxAbs (multIntv errIve1 errIve2)) * (mTypeToQ m)) err
         | Div =>
           if (((Qleb (ivhi errIve2) 0) && (negb (Qeq_bool (ivhi errIve2) 0))) ||
               ((Qleb 0 (ivlo errIve2)) && (negb (Qeq_bool (ivlo errIve2) 0))))
           then
             let upperBoundInvE2 := maxAbs (invertIntv ive2) in
             let minAbsIve2 := minAbs (errIve2) in
             let errInv := (1 / (minAbsIve2 * minAbsIve2)) * err2 in
             Qleb ((upperBoundE1 * errInv + upperBoundInvE2 * err1 + err1 * errInv) + (maxAbs (divideIntv errIve1 errIve2)) * (mTypeToQ m)) err
           else false
         end
       else false
      |Downcast m1 e1 =>
       if validErrorbound e1 typeMap A dVars
       then
         let (ive1, err1) := A e1 in
         let errIve1 := widenIntv ive1 err1 in
         (Qleb (err1 + maxAbs errIve1 * (mTypeToQ m1)) err)
       else
         false
      end
    else false
  end.

(** Error bound command validator **)
Fixpoint validErrorboundCmd (f:cmd Q) (* analyzed cmd with let's *)
         (typeMap:exp Q -> option mType) (* inferred types *)
         (A:analysisResult) (* FloVer's encoded result *)
         (dVars:NatSet.t) (* defined variables *)
         : bool :=
  match f with
  |Let m x e g =>
   if ((validErrorbound e typeMap A dVars) && (Qeq_bool (snd (A e)) (snd (A (Var Q x)))))
   then validErrorboundCmd g typeMap A (NatSet.add x dVars)
   else false
  |Ret e => validErrorbound e typeMap A dVars
  end.

(* Hide mTypeToQ from simplification since it will blow up the goal buffer *)
Arguments mTypeToQ _ :simpl nomatch.

(**
    Since errors are intervals with 0 as center, we encode them as single values.
    This lemma enables us to deduce from each run of the validator the invariant
    that when it succeeds, the errors must be positive.
**)
Lemma err_always_positive e tmap (absenv:analysisResult) iv err dVars:
  validErrorbound e tmap absenv dVars = true ->
  (iv,err) = absenv e ->
  (0 <= Q2R err)%R.
Proof.
  destruct e; intros validErrorbound_e absenv_e;
    unfold validErrorbound in validErrorbound_e;
    rewrite <- absenv_e in validErrorbound_e; simpl in *;
  repeat match goal with
  | [H: context [match ?E with | Some _ => _ |None => _ end] |- _ ] => destruct (E) eqn:?; try congruence
  | [H: context [if ?c then _ else _ ] |- _] => destruct (c) eqn:?; try congruence end;
    canonize_hyps; auto.
Qed.

Lemma validErrorboundCorrectVariable_eval:
  forall E1 E2 absenv (v:nat) e nR nlo nhi P fVars dVars Gamma expTypes,
    eval_exp E1 (toRMap Gamma) (toREval (toRExp (Var Q v))) nR M0 ->
    typeCheck (Var Q v) Gamma expTypes = true ->
    approxEnv E1 Gamma absenv fVars dVars E2 ->
    validIntervalbounds (Var Q v) absenv P dVars = true ->
    validErrorbound (Var Q v) expTypes absenv dVars = true ->
    NatSet.Subset (NatSet.diff (usedVars (Var Q v)) dVars) fVars ->
    (forall v1,
        NatSet.mem v1 dVars = true ->
        exists r, E1 v1 = Some r /\
             (Q2R (fst (fst (absenv (Var Q v1)))) <= r <= Q2R (snd (fst (absenv (Var Q v1)))))%R) ->
    (forall v1, NatSet.mem v1 fVars= true ->
          exists r, E1 v1 = Some r /\
                (Q2R (fst (P v1)) <= r <= Q2R (snd (P v1)))%R) ->
    (forall v1 : NatSet.elt,
        NatSet.mem v1 (NatSet.union fVars dVars) = true ->
        exists m0 : mType, Gamma v1 = Some m0) ->
    absenv (Var Q v) = ((nlo, nhi), e) ->
    exists nF m,
    eval_exp E2 Gamma (toRExp (Var Q v)) nF m.
Proof.
  intros * eval_real typing_ok approxCEnv bounds_valid error_valid v_sound
                     dVars_sound P_valid types_valid absenv_var.
  eapply validIntervalbounds_sound in bounds_valid; eauto.
  destruct bounds_valid as [nR2 [eval_real2 bounds_valid]].
  assert (nR2 = nR) by (eapply meps_0_deterministic; eauto);
    subst.
  simpl in eval_real; inversion eval_real; subst.
  rename H1 into E1_v.
  simpl in *.
  assert (exists m, Gamma v = Some m) as v_has_type.
  { destruct (expTypes (Var Q v)); try congruence.
    case_eq (Gamma v); intros * gamma_v;
      rewrite gamma_v in *; eauto. }
  destruct v_has_type as [m type_v].
  destruct (approxEnv_gives_value approxCEnv E1_v); try eauto.
  set_tac.
  case_eq (NatSet.mem v dVars); intros v_case; set_tac.
  left; apply v_sound.
  apply NatSetProps.FM.diff_3; set_tac.
Qed.

Lemma validErrorboundCorrectVariable:
  forall E1 E2 absenv (v:nat) e nR nF mF nlo nhi P fVars dVars Gamma expTypes,
    eval_exp E1 (toRMap Gamma) (toREval (toRExp (Var Q v))) nR M0 ->
    eval_exp E2 Gamma (toRExp (Var Q v)) nF mF ->
    typeCheck (Var Q v) Gamma expTypes = true ->
    approxEnv E1 Gamma absenv fVars dVars E2 ->
    validIntervalbounds (Var Q v) absenv P dVars = true ->
    validErrorbound (Var Q v) expTypes absenv dVars = true ->
    NatSet.Subset (NatSet.diff (usedVars (Var Q v)) dVars) fVars ->
    (forall v1,
        NatSet.mem v1 dVars = true ->
        exists r, E1 v1 = Some r /\
             (Q2R (fst (fst (absenv (Var Q v1)))) <= r <= Q2R (snd (fst (absenv (Var Q v1)))))%R) ->
    (forall v1, NatSet.mem v1 fVars= true ->
          exists r, E1 v1 = Some r /\
                (Q2R (fst (P v1)) <= r <= Q2R (snd (P v1)))%R) ->
    (forall v1 : NatSet.elt,
        NatSet.mem v1 (NatSet.union fVars dVars) = true ->
        exists m0 : mType, Gamma v1 = Some m0) ->
    absenv (Var Q v) = ((nlo, nhi), e) ->
    (Rabs (nR - nF) <= (Q2R e))%R.
Proof.
  intros * eval_real eval_float typing_ok approxCEnv bounds_valid error_valid
                     v_sound dVars_sound P_valid types_valid absenv_var.
  eapply validIntervalbounds_sound in bounds_valid; eauto.
  destruct bounds_valid as [nR2 [eval_real2 bounds_valid]].
  assert (nR2 = nR) by (eapply meps_0_deterministic; eauto);
    subst.
  simpl in *;
    inversion eval_real;
    inversion eval_float;
    subst.
  rename H1 into E1_v.
  simpl in *.
  rewrite absenv_var in error_valid; simpl in error_valid.
  case_eq (expTypes (Var Q v));
    intros * expType_v; rewrite expType_v in *; try congruence.
  match goal with
  | [ H: Gamma v = Some _ |- _] => rewrite H in *
  end; simpl in *.
  type_conv.
  andb_to_prop error_valid.
  rename L into error_pos.
  rename R into error_valid.
  case_eq (v mem dVars); [intros v_dVar | intros v_fVar].
  - rewrite v_dVar in *; simpl in *.
    rewrite NatSet.mem_spec in v_dVar.
    eapply approxEnv_dVar_bounded; eauto.
    rewrite absenv_var; auto.
  - rewrite v_fVar in *; simpl in *.
    apply not_in_not_mem in v_fVar.
    assert (v ∈ fVars) as v_in_fVars by set_tac.
    pose proof (approxEnv_fVar_bounded approxCEnv E1_v H6 v_in_fVars H5).
    eapply Rle_trans.
    apply H.
    canonize_hyps.
    rewrite Q2R_mult in error_valid.
    rewrite <- maxAbs_impl_RmaxAbs in error_valid.
    eapply Rle_trans; eauto.
    eapply Rmult_le_compat_r.
    + apply mTypeToQ_pos_R.
    + rewrite absenv_var in *; simpl in *.
      destruct bounds_valid as [valid_lo valid_hi].
      apply RmaxAbs; eauto.
Qed.

Lemma validErrorboundCorrectConstant_eval E1 E2 absenv m n nR e nlo nhi dVars Gamma defVars:
    eval_exp E1 (toRMap defVars) (toREval (toRExp (Const m n))) nR M0 ->
    typeCheck (Const m n) defVars Gamma = true ->
    validErrorbound (Const m n) Gamma absenv dVars = true ->
    (Q2R nlo <= nR <= Q2R nhi)%R ->
    absenv (Const m n) = ((nlo,nhi),e) ->
    exists nF m',
      eval_exp E2 defVars (toRExp (Const m n)) nF m'.
Proof.
  intros eval_real subexpr_ok error_valid intv_valid absenv_const.
  repeat eexists.
  econstructor.
  rewrite Rabs_eq_Qabs.
  apply Qle_Rle.
  rewrite Qabs_pos; try lra.
  apply mTypeToQ_pos_Q. lra.
Qed.

Lemma validErrorboundCorrectConstant E1 E2 absenv m n nR nF e nlo nhi dVars Gamma defVars:
    eval_exp E1 (toRMap defVars) (toREval (toRExp (Const m n))) nR M0 ->
    eval_exp E2 defVars (toRExp (Const m n)) nF m ->
    typeCheck (Const m n) defVars Gamma = true ->
    validErrorbound (Const m n) Gamma absenv dVars = true ->
    (Q2R nlo <= nR <= Q2R nhi)%R ->
    absenv (Const m n) = ((nlo,nhi),e) ->
    (Rabs (nR - nF) <= (Q2R e))%R.
Proof.
  intros eval_real eval_float subexpr_ok error_valid intv_valid absenv_const.
  eapply Rle_trans.
  simpl in eval_real,eval_float.
  eapply const_abs_err_bounded; eauto.
  unfold validErrorbound in error_valid.
  rewrite absenv_const in *; simpl in *.
  case_eq (Gamma (Const m n)); intros * type_const;
    rewrite type_const in error_valid; try congruence.
  andb_to_prop error_valid.
  rename R into error_valid.
  inversion eval_real; subst.
  simpl in *; rewrite Q2R0_is_0 in *.
  rewrite delta_0_deterministic in intv_valid; auto.
  apply Qle_bool_iff in error_valid.
  apply Qle_Rle in error_valid.
  destruct intv_valid.
  eapply Rle_trans.
  - eapply Rmult_le_compat_r.
    apply mTypeToQ_pos_R.
    apply RmaxAbs; eauto.
  - rewrite Q2R_mult in error_valid.
    rewrite <- maxAbs_impl_RmaxAbs in error_valid; auto.
    inversion subexpr_ok; subst.
    rewrite type_const in *.
    apply mTypeEq_compat_eq in H3; subst; auto.
Qed.

Lemma validErrorboundCorrectAddition E1 E2 absenv
      (e1:exp Q) (e2:exp Q) (nR nR1 nR2 nF nF1 nF2 :R) (e err1 err2 :error)
      (alo ahi e1lo e1hi e2lo e2hi:Q) dVars m m1 m2 Gamma defVars:
  m = join m1 m2 ->
  eval_exp E1 (toRMap defVars) (toREval (toRExp e1)) nR1 M0 ->
  eval_exp E1 (toRMap defVars) (toREval (toRExp e2)) nR2 M0 ->
  eval_exp E1 (toRMap defVars) (toREval (toRExp (Binop Plus e1 e2))) nR M0 ->
  eval_exp E2 defVars (toRExp e1) nF1 m1 ->
  eval_exp E2 defVars (toRExp e2) nF2 m2 ->
  eval_exp (updEnv 2 nF2 (updEnv 1 nF1 emptyEnv))
           (updDefVars 2 m2 (updDefVars 1 m1 defVars))
           (toRExp (Binop Plus (Var Q 1) (Var Q 2))) nF m ->
  typeCheck (Binop Plus e1 e2) defVars Gamma = true ->
  validErrorbound (Binop Plus e1 e2) Gamma absenv dVars = true ->
  (Q2R e1lo <= nR1 <= Q2R e1hi)%R ->
  (Q2R e2lo <= nR2 <= Q2R e2hi)%R ->
  absenv e1 = ((e1lo,e1hi),err1) ->
  absenv e2 = ((e2lo, e2hi),err2) ->
  absenv (Binop Plus e1 e2) = ((alo,ahi),e)->
  (Rabs (nR1 - nF1) <= (Q2R err1))%R ->
  (Rabs (nR2 - nF2) <= (Q2R err2))%R ->
  (Rabs (nR - nF) <= (Q2R e))%R.
Proof.
  intros mIsJoin e1_real e2_real eval_real e1_float e2_float eval_float
         subexpr_ok valid_error valid_intv1 valid_intv2 absenv_e1 absenv_e2 absenv_add
         err1_bounded err2_bounded.
  eapply Rle_trans.
  eapply
    (add_abs_err_bounded e1 e2);
    try eauto.
  unfold validErrorbound in valid_error.
  rewrite absenv_add,absenv_e1,absenv_e2 in valid_error.
  case_eq (Gamma (Binop Plus e1 e2)); intros; rewrite H in valid_error; [ | inversion valid_error ].
  simpl in subexpr_ok; rewrite H in subexpr_ok.
  case_eq (Gamma e1); intros; rewrite H0 in subexpr_ok; [ | inversion subexpr_ok ].
  case_eq (Gamma e2); intros; rewrite H1 in subexpr_ok; [ | inversion subexpr_ok ].
  andb_to_prop subexpr_ok.
  apply mTypeEq_compat_eq in L0; subst.
  pose proof (typingSoundnessExp _ _ R0 e1_float).
  pose proof (typingSoundnessExp _ _ R e2_float).
  rewrite H0 in H2; rewrite H1 in H3; inversion H2; inversion H3; subst.
  clear H2 H3 H0 H1.
  andb_to_prop valid_error.
  rename R2 into valid_error.
  eapply Rle_trans.
  apply Rplus_le_compat_l.
  eapply Rmult_le_compat_r.
  apply mTypeToQ_pos_R.
  Focus 2.
  rewrite Qle_bool_iff in valid_error.
  apply Qle_Rle in valid_error.
  repeat rewrite Q2R_plus in valid_error.
  repeat rewrite Q2R_mult in valid_error.
  repeat rewrite Q2R_plus in valid_error.
  repeat rewrite <- Rabs_eq_Qabs in valid_error.
  repeat rewrite Q2R_plus in valid_error.
  repeat rewrite <- maxAbs_impl_RmaxAbs in valid_error.
  apply valid_error.
  clear L R.
  remember (addIntv (widenIntv (e1lo, e1hi) err1) (widenIntv (e2lo, e2hi) err2)) as iv.
  iv_assert iv iv_unf.
  destruct iv_unf as [ivl [ivh iv_unf]].
  rewrite iv_unf.
  rewrite <- maxAbs_impl_RmaxAbs.
  assert (ivlo iv = ivl) by (rewrite iv_unf; auto).
  assert (ivhi iv = ivh) by (rewrite iv_unf; auto).
  rewrite <- H0, <- H1.
  assert (contained nR1 (Q2R e1lo, Q2R e1hi)) as contained_intv1 by auto.
  pose proof (distance_gives_iv (a:=nR1) _ (Q2R e1lo, Q2R e1hi) contained_intv1 err1_bounded).
  assert (contained nR2 (Q2R e2lo, Q2R e2hi)) as contained_intv2 by auto.
  pose proof (distance_gives_iv (a:=nR2) _ (Q2R e2lo, Q2R e2hi) contained_intv2 err2_bounded).
  pose proof (IntervalArith.interval_addition_valid _ _ H2 H3).
  simpl in *.
  destruct H4.
  subst; simpl in *.
  apply RmaxAbs; simpl.
  - rewrite Q2R_min4.
    repeat rewrite Q2R_plus;
      repeat rewrite Q2R_minus; auto.
  - rewrite Q2R_max4.
    repeat rewrite Q2R_plus;
      repeat rewrite Q2R_minus; auto.
Qed.

Lemma validErrorboundCorrectSubtraction E1 E2 absenv
      (e1:exp Q) (e2:exp Q) (nR nR1 nR2 nF nF1 nF2 :R) (e err1 err2 :error)
      (alo ahi e1lo e1hi e2lo e2hi:Q) dVars (m m1 m2:mType) Gamma defVars:
  m = join m1 m2 ->
  eval_exp E1 (toRMap defVars) (toREval (toRExp e1)) nR1 M0 ->
  eval_exp E1 (toRMap defVars) (toREval (toRExp e2)) nR2 M0 ->
  eval_exp E1 (toRMap defVars) (toREval (toRExp (Binop Sub e1 e2))) nR M0 ->
  eval_exp E2 defVars (toRExp e1) nF1 m1->
  eval_exp E2 defVars (toRExp e2) nF2 m2 ->
  eval_exp (updEnv 2 nF2 (updEnv 1 nF1 emptyEnv))
           (updDefVars 2 m2 (updDefVars 1 m1 defVars))
           (toRExp (Binop Sub (Var Q 1) (Var Q 2))) nF m ->
  typeCheck (Binop Sub e1 e2) defVars Gamma = true ->
  validErrorbound (Binop Sub e1 e2) Gamma absenv dVars = true ->
  (Q2R e1lo <= nR1 <= Q2R e1hi)%R ->
  (Q2R e2lo <= nR2 <= Q2R e2hi)%R ->
  absenv e1 = ((e1lo,e1hi),err1) ->
  absenv e2 = ((e2lo, e2hi),err2) ->
  absenv (Binop Sub e1 e2) = ((alo,ahi),e)->
  (Rabs (nR1 - nF1) <= (Q2R err1))%R ->
  (Rabs (nR2 - nF2) <= (Q2R err2))%R ->
  (Rabs (nR - nF) <= (Q2R e))%R.
Proof.
  intros mIsJoin e1_real e2_real eval_real e1_float e2_float eval_float
         subexpr_ok valid_error valid_intv1 valid_intv2 absenv_e1 absenv_e2 absenv_sub
         err1_bounded err2_bounded.
  eapply Rle_trans.
  eapply (subtract_abs_err_bounded e1 e2); try eauto.
  unfold validErrorbound in valid_error.
  rewrite absenv_sub,absenv_e1,absenv_e2 in valid_error.
  case_eq (Gamma (Binop Sub e1 e2)); intros; rewrite H in valid_error; [ | inversion valid_error ].
  simpl in subexpr_ok; rewrite H in subexpr_ok.
  case_eq (Gamma e1); intros; rewrite H0 in subexpr_ok; [ | inversion subexpr_ok ].
  case_eq (Gamma e2); intros; rewrite H1 in subexpr_ok; [ | inversion subexpr_ok ].
  andb_to_prop subexpr_ok.
  apply mTypeEq_compat_eq in L0; subst.
  pose proof (typingSoundnessExp _ _ R0 e1_float).
  pose proof (typingSoundnessExp _ _ R e2_float).
  rewrite H0 in H2; rewrite H1 in H3; inversion H2; inversion H3; subst.
  clear H2 H3 H0 H1.
  andb_to_prop valid_error.
  rename R2 into valid_error.
  apply Qle_bool_iff in valid_error.
  apply Qle_Rle in valid_error.
  repeat rewrite Q2R_plus in valid_error.
  repeat rewrite Q2R_mult in valid_error.
  repeat rewrite Q2R_plus in valid_error.
  repeat rewrite <- Rabs_eq_Qabs in valid_error.
  repeat rewrite Q2R_plus in valid_error.
  repeat rewrite <- maxAbs_impl_RmaxAbs in valid_error.
  eapply Rle_trans.
  apply Rplus_le_compat_l.
  eapply Rmult_le_compat_r.
  apply mTypeToQ_pos_R.
  Focus 2.
  apply valid_error.
  remember (subtractIntv (widenIntv (e1lo, e1hi) err1) (widenIntv (e2lo, e2hi) err2)) as iv.
  iv_assert iv iv_unf.
  destruct iv_unf as [ivl [ivh iv_unf]].
  rewrite iv_unf.
  rewrite <- maxAbs_impl_RmaxAbs.
  assert (ivlo iv = ivl) by (rewrite iv_unf; auto).
  assert (ivhi iv = ivh) by (rewrite iv_unf; auto).
  rewrite <- H0, <- H1.
    assert (contained nR1 (Q2R e1lo, Q2R e1hi)) as contained_intv1 by auto.
  pose proof (distance_gives_iv (a:=nR1) _ _ contained_intv1 err1_bounded).
  assert (contained nR2 (Q2R e2lo, Q2R e2hi)) as contained_intv2 by auto.
  pose proof (distance_gives_iv (a:=nR2) _ _ contained_intv2 err2_bounded).
  pose proof (IntervalArith.interval_subtraction_valid _ _ H2 H3).
  simpl in *.
  destruct H4.
  subst; simpl in *.
  apply RmaxAbs; simpl.
  - rewrite Q2R_min4.
    repeat rewrite Q2R_plus;
      repeat rewrite Q2R_minus;
      repeat rewrite Q2R_opp;
      repeat rewrite Q2R_plus;
      repeat rewrite Q2R_minus; auto.
  - rewrite Q2R_max4.
    repeat rewrite Q2R_plus;
      repeat rewrite Q2R_minus;
      repeat rewrite Q2R_opp;
      repeat rewrite Q2R_plus;
      repeat rewrite Q2R_minus; auto.
Qed.

Lemma validErrorboundCorrectMult E1 E2 absenv
      (e1:exp Q) (e2:exp Q) (nR nR1 nR2 nF nF1 nF2 :R) (e err1 err2 :error)
      (alo ahi e1lo e1hi e2lo e2hi:Q) dVars (m m1 m2:mType) Gamma defVars:
  m = join m1 m2 ->
  eval_exp E1 (toRMap defVars) (toREval (toRExp e1)) nR1 M0 ->
  eval_exp E1 (toRMap defVars) (toREval (toRExp e2)) nR2 M0 ->
  eval_exp E1 (toRMap defVars) (toREval (toRExp (Binop Mult e1 e2))) nR M0 ->
  eval_exp E2 defVars (toRExp e1) nF1 m1 ->
  eval_exp E2 defVars (toRExp e2) nF2 m2 ->
  eval_exp (updEnv 2 nF2 (updEnv 1 nF1 emptyEnv))
           (updDefVars 2 m2 (updDefVars 1 m1 defVars))
           (toRExp (Binop Mult (Var Q 1) (Var Q 2))) nF m ->
  typeCheck (Binop Mult e1 e2) defVars Gamma = true ->
  validErrorbound (Binop Mult e1 e2) Gamma absenv dVars = true ->
  (Q2R e1lo <= nR1 <= Q2R e1hi)%R ->
  (Q2R e2lo <= nR2 <= Q2R e2hi)%R ->
  absenv e1 = ((e1lo,e1hi),err1) ->
  absenv e2 = ((e2lo, e2hi),err2) ->
  absenv (Binop Mult e1 e2) = ((alo,ahi),e)->
  (Rabs (nR1 - nF1) <= (Q2R err1))%R ->
  (Rabs (nR2 - nF2) <= (Q2R err2))%R ->
  (Rabs (nR - nF) <= (Q2R e))%R.
Proof.
  intros mIsJoin e1_real e2_real eval_real e1_float e2_float eval_float
         subexpr_ok valid_error valid_e1 valid_e2 absenv_e1 absenv_e2 absenv_mult
         err1_bounded err2_bounded.
  eapply Rle_trans.
  eapply (mult_abs_err_bounded e1 e2); eauto.
  unfold validErrorbound in valid_error.
  rewrite absenv_mult,absenv_e1,absenv_e2 in valid_error.
  case_eq (Gamma (Binop Mult e1 e2)); intros; rewrite H in valid_error; [ | inversion valid_error ].
  simpl in subexpr_ok; rewrite H in subexpr_ok.
  case_eq (Gamma e1); intros; rewrite H0 in subexpr_ok; [ | inversion subexpr_ok ].
  case_eq (Gamma e2); intros; rewrite H1 in subexpr_ok; [ | inversion subexpr_ok ].
  andb_to_prop subexpr_ok.
  apply mTypeEq_compat_eq in L0; subst.
  pose proof (typingSoundnessExp _ _ R0 e1_float).
  pose proof (typingSoundnessExp _ _ R e2_float).
  rewrite H0 in H2; rewrite H1 in H3; inversion H2; inversion H3; subst.
  clear H2 H3 H0 H1.
  andb_to_prop valid_error.
  rename R2 into valid_error.
  assert (0 <= Q2R err1)%R as err1_pos by (eapply (err_always_positive e1 Gamma absenv dVars); eauto).
  assert (0 <= Q2R err2)%R as err2_pos by (eapply (err_always_positive e2 Gamma absenv dVars); eauto).
  clear R1 L1.
  apply Qle_bool_iff in valid_error.
  apply Qle_Rle in valid_error.
  repeat rewrite Q2R_plus in valid_error.
  repeat rewrite Q2R_mult in valid_error.
  repeat rewrite Q2R_plus in valid_error.
  repeat rewrite <- Rabs_eq_Qabs in valid_error.
  repeat rewrite Q2R_plus in valid_error.
  repeat rewrite <- maxAbs_impl_RmaxAbs in valid_error.
  eapply Rle_trans.
  Focus 2.
  apply valid_error.
  apply Rplus_le_compat.
  - unfold Rabs in err1_bounded.
    unfold Rabs in err2_bounded.
    (* Before doing case distinction, prove bounds that will be used many times: *)
    assert (nR1 <= RmaxAbsFun (Q2R e1lo, Q2R e1hi))%R
      as upperBound_nR1
        by (apply contained_leq_maxAbs_val; auto).
    assert (nR2 <= RmaxAbsFun (Q2R e2lo, Q2R e2hi))%R
      as upperBound_nR2
        by (apply contained_leq_maxAbs_val; auto).
    assert (-nR1 <= RmaxAbsFun (Q2R e1lo, Q2R e1hi))%R
      as upperBound_Ropp_nR1
        by (apply contained_leq_maxAbs_neg_val; auto).
    assert (- nR2 <= RmaxAbsFun (Q2R e2lo, Q2R e2hi))%R
      as upperBound_Ropp_nR2
        by (apply contained_leq_maxAbs_neg_val; auto).
    assert (nR1 * Q2R err2 <= RmaxAbsFun (Q2R e1lo, Q2R e1hi) * Q2R err2)%R
      as bound_nR1 by (apply Rmult_le_compat_r; auto).
    assert (- nR1 * Q2R err2 <= RmaxAbsFun (Q2R e1lo, Q2R e1hi) * Q2R err2)%R
      as bound_neg_nR1 by (apply Rmult_le_compat_r; auto).
    assert (nR2 * Q2R err1 <= RmaxAbsFun (Q2R e2lo, Q2R e2hi) * Q2R err1)%R
      as bound_nR2 by (apply Rmult_le_compat_r; auto).
    assert (- nR2 * Q2R err1 <= RmaxAbsFun (Q2R e2lo, Q2R e2hi) * Q2R err1)%R
      as bound_neg_nR2 by (apply Rmult_le_compat_r; auto).
    assert (- (Q2R err1 * Q2R err2) <= Q2R err1 * Q2R err2)%R as err_neg_bound
        by  (rewrite Ropp_mult_distr_l; apply Rmult_le_compat_r; lra).
    assert (0 <= RmaxAbsFun (Q2R e1lo, Q2R e1hi) * Q2R err2)%R
      as zero_up_nR1 by lra.
    assert (RmaxAbsFun (Q2R e1lo, Q2R e1hi) * Q2R err2 <= RmaxAbsFun (Q2R e1lo, Q2R e1hi) * Q2R err2 + RmaxAbsFun (Q2R e2lo, Q2R e2hi) * Q2R err1)%R
      as nR1_to_sum by lra.
    assert (RmaxAbsFun (Q2R e1lo, Q2R e1hi) * Q2R err2 + RmaxAbsFun (Q2R e2lo, Q2R e2hi) * Q2R err1 <=  RmaxAbsFun (Q2R e1lo, Q2R e1hi) * Q2R err2 + RmaxAbsFun (Q2R e2lo, Q2R e2hi) * Q2R err1 + Q2R err1 * Q2R err2)%R
      as sum_to_errsum by lra.
    clear e1_real e1_float e2_real e2_float eval_real eval_float valid_error
      absenv_e1 absenv_e2.
    (* Large case distinction for
         a) different cases of the value of Rabs (...) and
         b) wether arguments of multiplication in (nf1 * nF2) are < or >= 0 *)
    destruct Rcase_abs in err1_bounded; destruct Rcase_abs in err2_bounded.
    + rewrite Rsub_eq_Ropp_Rplus in err1_bounded, err2_bounded.
      rewrite Ropp_plus_distr in err1_bounded, err2_bounded.
      rewrite Ropp_involutive in err1_bounded, err2_bounded.
      assert (nF1 <= Q2R err1 + nR1)%R by lra.
      assert (nF2 <= Q2R err2 + nR2)%R by lra.
      unfold Rabs.
      destruct Rcase_abs.
      * rewrite Rsub_eq_Ropp_Rplus. rewrite Ropp_plus_distr.
        rewrite Ropp_involutive.
        destruct (Rle_lt_dec 0 nF1).
        { (* Upper Bound ... *)
          eapply Rle_trans.
          eapply Rplus_le_compat_l.
          eapply Rmult_le_compat_l; auto.
          apply H1.
          destruct (Rle_lt_dec 0 (Q2R err2 + nR2)).
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            eapply Rmult_le_compat_r; auto.
            apply H0.
            lra.
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l. hnf. left; auto.
            assert (nR1 <= nF1)%R by lra.
            apply H2.
            lra.
        }
        {
          eapply Rle_trans.
          eapply Rplus_le_compat_l.
          eapply Rmult_le_compat_neg_l.
          hnf. left; auto.
          assert (nR2 < nF2)%R by lra.
          apply Rlt_le in H2; apply H2.
          destruct (Rle_lt_dec 0 nR2).
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            eapply Rmult_le_compat_r; auto.
            apply H0.
            lra.
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l.
            hnf. left; auto.
            assert (nR1 < nF1)%R by lra.
            apply Rlt_le in H2; apply H2.
            lra.
        }
      * rewrite Rsub_eq_Ropp_Rplus.
        destruct (Rle_lt_dec 0 nF1).
        {
          rewrite Ropp_mult_distr_r.
          eapply Rle_trans.
          eapply Rplus_le_compat_l.
          eapply Rmult_le_compat_l; auto.
          assert (- nF2 <= - nR2)%R by lra.
          apply H2.
          destruct (Rle_lt_dec 0 (- nR2)).
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            eapply Rmult_le_compat_r; auto.
            apply H0.
            lra.
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l.
            hnf. left; auto.
            assert (nR1 < nF1)%R by lra.
            apply Rlt_le in H2; apply H2.
            lra.
        }
        {
          rewrite Ropp_mult_distr_l.
          eapply Rle_trans.
          eapply Rplus_le_compat_l.
          eapply Rmult_le_compat_l.
          rewrite <- (Ropp_involutive 0).
          apply Ropp_ge_le_contravar.
          apply Rle_ge.
          rewrite Ropp_0.
          hnf. left; auto.
          apply H1.
          destruct (Rle_lt_dec 0 (Q2R err2 + nR2)).
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            eapply Rmult_le_compat_r; auto.
            assert (- nF1 <= -nR1)%R by lra.
            apply H2.
            lra.
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l.
            hnf. left; auto.
            apply Ropp_le_ge_contravar in H0.
            apply Rge_le in H0.
            apply H0.
            lra.
        }
    + rewrite Rsub_eq_Ropp_Rplus in err1_bounded, err2_bounded.
      rewrite Ropp_plus_distr in err1_bounded.
      rewrite Ropp_involutive in err1_bounded.
      assert (nF1 <= Q2R err1 + nR1)%R by lra.
      assert (nF2 <= Q2R err2 + nR2)%R by lra.
      unfold Rabs.
      destruct Rcase_abs.
      * rewrite Rsub_eq_Ropp_Rplus. rewrite Ropp_plus_distr.
        rewrite Ropp_involutive.
        destruct (Rle_lt_dec 0 nF1).
        { (* Upper Bound ... *)
          eapply Rle_trans.
          eapply Rplus_le_compat_l.
          eapply Rmult_le_compat_l; auto.
          apply H1.
          destruct (Rle_lt_dec 0 (Q2R err2 + nR2)).
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            eapply Rmult_le_compat_r; auto.
            apply H0.
            lra.
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l. hnf. left; auto.
            assert (nR1 <= nF1)%R by lra.
            apply H2.
            lra.
        }
        {
          eapply Rle_trans.
          eapply Rplus_le_compat_l.
          eapply Rmult_le_compat_neg_l.
          hnf. left; auto.
          assert (- nF2 <= - (nR2 - Q2R err2))%R by lra.
          apply Ropp_le_ge_contravar in H2.
          repeat rewrite Ropp_involutive in H2.
          apply Rge_le in H2.
          apply H2.
          destruct (Rle_lt_dec 0 (nR2 - Q2R err2)).
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            eapply Rmult_le_compat_r; auto.
            apply H0.
            lra.
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l.
            hnf. left; auto.
            assert (nR1 < nF1)%R by lra.
            apply Rlt_le in H2; apply H2.
            lra.
        }
      * rewrite Rsub_eq_Ropp_Rplus.
        destruct (Rle_lt_dec 0 nF1).
        {
          rewrite Ropp_mult_distr_r.
          eapply Rle_trans.
          eapply Rplus_le_compat_l.
          eapply Rmult_le_compat_l; auto.
          assert (- nF2 <= - nR2 + Q2R err2)%R by lra.
          apply H2.
          destruct (Rle_lt_dec 0 (- nR2 + Q2R err2)).
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            eapply Rmult_le_compat_r; auto.
            apply H0.
            lra.
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l.
            hnf. left; auto.
            assert (nR1 < nF1)%R by lra.
            apply Rlt_le in H2; apply H2.
            lra.
        }
        {
          rewrite Ropp_mult_distr_l.
          eapply Rle_trans.
          eapply Rplus_le_compat_l.
          eapply Rmult_le_compat_l.
          rewrite <- (Ropp_involutive 0).
          apply Ropp_ge_le_contravar.
          apply Rle_ge.
          rewrite Ropp_0.
          hnf. left; auto.
          apply H1.
          destruct (Rle_lt_dec 0 (Q2R err2 + nR2)).
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            eapply Rmult_le_compat_r; auto.
            assert (- nF1 <= -nR1)%R by lra.
            apply H2.
            lra.
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l.
            hnf. left; auto.
            apply Ropp_le_ge_contravar in H0.
            apply Rge_le in H0.
            apply H0.
            lra.
        }
    + rewrite Rsub_eq_Ropp_Rplus in err1_bounded, err2_bounded.
      rewrite Ropp_plus_distr in err2_bounded.
      rewrite Ropp_involutive in err2_bounded.
      assert (nF1 <= Q2R err1 + nR1)%R by lra.
      assert (nF2 <= Q2R err2 + nR2)%R by lra.
      unfold Rabs.
      destruct Rcase_abs.
      * rewrite Rsub_eq_Ropp_Rplus. rewrite Ropp_plus_distr.
        rewrite Ropp_involutive.
        destruct (Rle_lt_dec 0 nF1).
        { (* Upper Bound ... *)
          eapply Rle_trans.
          eapply Rplus_le_compat_l.
          eapply Rmult_le_compat_l; auto.
          apply H1.
          destruct (Rle_lt_dec 0 (Q2R err2 + nR2)).
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            eapply Rmult_le_compat_r; auto.
            apply H0.
            lra.
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l. hnf. left; auto.
            assert (- nF1 <= - (nR1 - Q2R err1))%R by lra.
            apply Ropp_le_ge_contravar in H2.
            repeat rewrite Ropp_involutive in H2.
            apply Rge_le in H2.
            apply H2.
            lra.
        }
        {
          eapply Rle_trans.
          eapply Rplus_le_compat_l.
          eapply Rmult_le_compat_neg_l.
          hnf. left; auto.
          assert (nR2 < nF2)%R by lra.
          apply Rlt_le in H2; apply H2.
          destruct (Rle_lt_dec 0 nR2).
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            eapply Rmult_le_compat_r; auto.
            apply H0.
            lra.
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l.
            hnf. left; auto.
            assert (- nF1 <= - (nR1 - Q2R err1))%R by lra.
            apply Ropp_le_ge_contravar in H2.
            repeat rewrite Ropp_involutive in H2.
            apply Rge_le in H2.
            apply H2.
            lra.
        }
      * rewrite Rsub_eq_Ropp_Rplus.
        destruct (Rle_lt_dec 0 nF1).
        {
          rewrite Ropp_mult_distr_r.
          eapply Rle_trans.
          eapply Rplus_le_compat_l.
          eapply Rmult_le_compat_l; auto.
          assert (- nF2 <= - nR2)%R by lra.
          apply H2.
          destruct (Rle_lt_dec 0 (- nR2)).
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            eapply Rmult_le_compat_r; auto.
            apply H0.
            lra.
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l.
            hnf. left; auto.
            assert (- nF1 <= - (nR1 - Q2R err1))%R by lra.
            apply Ropp_le_ge_contravar in H2.
            repeat rewrite Ropp_involutive in H2.
            apply Rge_le in H2.
            apply H2.
            lra.
        }
        {
          rewrite Ropp_mult_distr_l.
          eapply Rle_trans.
          eapply Rplus_le_compat_l.
          eapply Rmult_le_compat_l.
          lra.
          apply H1.
          destruct (Rle_lt_dec 0 (Q2R err2 + nR2)).
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            eapply Rmult_le_compat_r; auto.
            assert (- nF1 <= - (nR1 - Q2R err1))%R by lra.
            apply H2.
            lra.
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l; try lra.
            apply Ropp_le_ge_contravar in H0.
            apply Rge_le in H0.
            apply H0.
            lra.
        }
    (* All positive *)
    + assert (nF1 <= Q2R err1 + nR1)%R by lra.
      assert (nF2 <= Q2R err2 + nR2)%R by lra.
      unfold Rabs.
      destruct Rcase_abs.
      * rewrite Rsub_eq_Ropp_Rplus. rewrite Ropp_plus_distr.
        rewrite Ropp_involutive.
        destruct (Rle_lt_dec 0 nF1).
        { (* Upper Bound ... *)
          eapply Rle_trans.
          eapply Rplus_le_compat_l.
          eapply Rmult_le_compat_l; auto.
          apply H1.
          destruct (Rle_lt_dec 0 (Q2R err2 + nR2)).
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            eapply Rmult_le_compat_r; auto.
            apply H0.
            lra.
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l. hnf. left; auto.
            assert (nR1 - Q2R err1 <= nF1)%R by lra.
            apply H2.
            lra.
        }
        {
          eapply Rle_trans.
          eapply Rplus_le_compat_l.
          eapply Rmult_le_compat_neg_l.
          hnf. left; auto.
          assert (nR2 - Q2R err2 <= nF2)%R by lra.
          apply H2.
          destruct (Rle_lt_dec 0 (nR2 - Q2R err2)).
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            eapply Rmult_le_compat_r; auto.
            apply H0.
            lra.
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l.
            lra.
            assert (nR1 - Q2R err1 <= nF1)%R by lra.
            apply H2.
            lra.
        }
      * rewrite Rsub_eq_Ropp_Rplus.
        destruct (Rle_lt_dec 0 nF1).
        {
          rewrite Ropp_mult_distr_r.
          eapply Rle_trans.
          eapply Rplus_le_compat_l.
          eapply Rmult_le_compat_l; auto.
          assert (- nF2 <= Q2R err2 - nR2)%R by lra.
          apply H2.
          destruct (Rle_lt_dec 0 (Q2R err2 - nR2)).
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            eapply Rmult_le_compat_r; auto.
            apply H0.
            lra.
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l.
            lra.
            assert (nR1 - Q2R err1 <= nF1)%R by lra.
            apply H2.
            lra.
        }
        {
          rewrite Ropp_mult_distr_l.
          eapply Rle_trans.
          eapply Rplus_le_compat_l.
          eapply Rmult_le_compat_l.
          rewrite <- (Ropp_involutive 0).
          apply Ropp_ge_le_contravar.
          apply Rle_ge.
          rewrite Ropp_0.
          lra.
          apply H1.
          destruct (Rle_lt_dec 0 (Q2R err2 + nR2)).
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            eapply Rmult_le_compat_r; auto.
            assert (- nF1 <= Q2R err1 - nR1)%R by lra.
            apply H2.
            lra.
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l.
            lra.
            apply Ropp_le_ge_contravar in H0.
            apply Rge_le in H0.
            apply H0.
            lra.
        }
  - remember (multIntv (widenIntv (e1lo, e1hi) err1) (widenIntv (e2lo, e2hi) err2)) as iv.
    iv_assert iv iv_unf.
    destruct iv_unf as [ivl [ivh iv_unf]].
    rewrite iv_unf.
    rewrite <- maxAbs_impl_RmaxAbs.
    assert (ivlo iv = ivl) by (rewrite iv_unf; auto).
    assert (ivhi iv = ivh) by (rewrite iv_unf; auto).
    rewrite <- H0, <- H1.
    assert (contained nR1 (Q2R e1lo, Q2R e1hi)) as contained_intv1 by auto.
    pose proof (distance_gives_iv (a:=nR1) _ _ contained_intv1 err1_bounded).
    assert (contained nR2 (Q2R e2lo, Q2R e2hi)) as contained_intv2 by auto.
    pose proof (distance_gives_iv (a:=nR2) _ _ contained_intv2 err2_bounded).
    pose proof (IntervalArith.interval_multiplication_valid _ _ H2 H3).
    simpl in *.
    destruct H4.
    unfold RmaxAbsFun.
    apply Rmult_le_compat_r.
    apply mTypeToQ_pos_R.
    apply RmaxAbs; subst; simpl in *.
    + rewrite Q2R_min4.
      repeat rewrite Q2R_mult;
        repeat rewrite Q2R_minus;
        repeat rewrite Q2R_plus; auto.
    + rewrite Q2R_max4.
      repeat rewrite Q2R_mult;
        repeat rewrite Q2R_minus;
        repeat rewrite Q2R_plus; auto.
Qed.

Lemma validErrorboundCorrectDiv E1 E2 absenv
      (e1:exp Q) (e2:exp Q) (nR nR1 nR2 nF nF1 nF2 :R) (e err1 err2 :error)
      (alo ahi e1lo e1hi e2lo e2hi:Q) dVars (m m1 m2:mType) Gamma defVars:
  m = join m1 m2 ->
  eval_exp E1 (toRMap defVars) (toREval (toRExp e1)) nR1 M0 ->
  eval_exp E1 (toRMap defVars) (toREval (toRExp e2)) nR2 M0 ->
  eval_exp E1 (toRMap defVars) (toREval (toRExp (Binop Div e1 e2))) nR M0 ->
  eval_exp E2 defVars (toRExp e1) nF1 m1 ->
  eval_exp E2 defVars (toRExp e2) nF2 m2 ->
  eval_exp (updEnv 2 nF2 (updEnv 1 nF1 emptyEnv))
           (updDefVars 2 m2 (updDefVars 1 m1 defVars))
           (toRExp (Binop Div (Var Q 1) (Var Q 2))) nF m ->
  typeCheck (Binop Div e1 e2) defVars Gamma = true ->
  validErrorbound (Binop Div e1 e2) Gamma absenv dVars = true ->
  (Q2R e1lo <= nR1 <= Q2R e1hi)%R ->
  (Q2R e2lo <= nR2 <= Q2R e2hi)%R ->
  (Qleb e2hi 0 && negb (Qeq_bool e2hi 0) || Qleb 0 e2lo && negb (Qeq_bool e2lo 0) = true) ->
  absenv e1 = ((e1lo,e1hi),err1) ->
  absenv e2 = ((e2lo, e2hi),err2) ->
  absenv (Binop Div e1 e2) = ((alo,ahi),e)->
  (Rabs (nR1 - nF1) <= (Q2R err1))%R ->
  (Rabs (nR2 - nF2) <= (Q2R err2))%R ->
  (Rabs (nR - nF) <= (Q2R e))%R.
Proof.
  intros mIsJoin e1_real e2_real eval_real e1_float e2_float eval_float
         subexpr_ok valid_error valid_bounds_e1 valid_bounds_e2 no_div_zero_real absenv_e1
         absenv_e2 absenv_div err1_bounded err2_bounded.
  eapply Rle_trans.
  eapply (div_abs_err_bounded e1 e2); eauto.
  unfold validErrorbound in valid_error.
  rewrite absenv_div,absenv_e1,absenv_e2 in valid_error.
  case_eq (Gamma (Binop Div e1 e2)); intros; rewrite H in valid_error; [ | inversion valid_error ].
  simpl in subexpr_ok; rewrite H in subexpr_ok.
  case_eq (Gamma e1); intros; rewrite H0 in subexpr_ok; [ | inversion subexpr_ok ].
  case_eq (Gamma e2); intros; rewrite H1 in subexpr_ok; [ | inversion subexpr_ok ].
  andb_to_prop subexpr_ok.
  apply mTypeEq_compat_eq in L0; subst.
  pose proof (typingSoundnessExp _ _ R0 e1_float).
  pose proof (typingSoundnessExp _ _ R e2_float).
  rewrite H0 in H2; rewrite H1 in H3; inversion H2; inversion H3; subst.
  clear H2 H3 H0 H1.
  andb_to_prop valid_error.
  rename R3 into valid_error.
  rename L0 into no_div_zero_float.
  assert (contained nR1 (Q2R e1lo, Q2R e1hi)) as contained_intv1 by auto.
  pose proof (distance_gives_iv (a:=nR1) _ _ contained_intv1 err1_bounded).
  assert (contained nR2 (Q2R e2lo, Q2R e2hi)) as contained_intv2 by auto.
  pose proof (distance_gives_iv (a:=nR2) _ _ contained_intv2 err2_bounded).
  assert (0 <= Q2R err1)%R as err1_pos by (eapply (err_always_positive e1 Gamma absenv); eauto).
  assert (0 <= Q2R err2)%R as err2_pos by (eapply (err_always_positive e2 Gamma absenv); eauto).
  apply Qle_bool_iff in valid_error.
  apply Qle_Rle in valid_error.
  repeat rewrite Q2R_plus in valid_error.
  repeat rewrite Q2R_mult in valid_error.
  rewrite Q2R_div in valid_error.
  - rewrite Q2R_mult in valid_error.
    repeat rewrite <- maxAbs_impl_RmaxAbs in valid_error.
    rewrite <- maxAbs_impl_RmaxAbs_iv in valid_error.
    repeat rewrite <- minAbs_impl_RminAbs_iv in valid_error.
    apply le_neq_bool_to_lt_prop in no_div_zero_float; apply le_neq_bool_to_lt_prop in no_div_zero_real.
    assert ((IVhi (Q2R e2lo,Q2R e2hi) < 0)%R \/ (0 < IVlo (Q2R e2lo,Q2R e2hi))%R) as no_div_zero_real_R
          by (simpl; rewrite <- Q2R0_is_0; simpl in no_div_zero_real;
              destruct no_div_zero_real as [ndiv | ndiv]; apply Qlt_Rlt in ndiv; auto).
    apply Q_case_div_to_R_case_div in no_div_zero_float; apply Q_case_div_to_R_case_div in no_div_zero_real.
    assert (Q2R e2lo = 0 -> False)%R by (apply (lt_or_gt_neq_zero_lo _ (Q2R e2hi)); try auto; lra).
    assert (Q2R e2hi = 0 -> False)%R by (apply (lt_or_gt_neq_zero_hi (Q2R e2lo)); try auto; lra).
    eapply Rle_trans.
    Focus 2.
    apply valid_error.
    apply Rplus_le_compat.
    (* Error Propagation proof *)
    + clear absenv_e1 absenv_e2 valid_error eval_float eval_real e1_float
            e1_real e2_float e2_real absenv_div
            E1 E2 alo ahi nR nF e L.
      unfold widenInterval in *.
      simpl in *.
      rewrite Q2R_plus, Q2R_minus in no_div_zero_float.
      assert (Q2R e2lo - Q2R err2 = 0 -> False)%R by (apply (lt_or_gt_neq_zero_lo _ (Q2R e2hi + Q2R err2)); try auto; lra).
      assert (Q2R e2hi + Q2R err2 = 0 -> False)%R by (apply (lt_or_gt_neq_zero_hi (Q2R e2lo - Q2R err2)); try auto; lra).
      pose proof (interval_inversion_valid (Q2R e2lo,Q2R e2hi) no_div_zero_real_R valid_bounds_e2) as valid_bounds_inv_e2.
      clear no_div_zero_real_R.
      (* Before doing case distinction, prove bounds that will be used many times: *)
      assert (nR1 <= RmaxAbsFun (Q2R e1lo, Q2R e1hi))%R
        as upperBound_nR1
          by (apply contained_leq_maxAbs_val; auto).
      assert (/ nR2 <= RmaxAbsFun (invertInterval (Q2R e2lo, Q2R e2hi)))%R
        as upperBound_nR2
          by (apply contained_leq_maxAbs_val; auto).
      assert (-nR1 <= RmaxAbsFun (Q2R e1lo, Q2R e1hi))%R
        as upperBound_Ropp_nR1
          by (apply contained_leq_maxAbs_neg_val; auto).
      assert (- / nR2 <= RmaxAbsFun (invertInterval (Q2R e2lo, Q2R e2hi)))%R
        as upperBound_Ropp_nR2
          by (apply contained_leq_maxAbs_neg_val; auto).
      assert (nR1 * Q2R err2 <= RmaxAbsFun (Q2R e1lo, Q2R e1hi) * Q2R err2)%R
        as bound_nR1
          by (apply Rmult_le_compat_r; auto).
      assert (- nR1 * Q2R err2 <= RmaxAbsFun (Q2R e1lo, Q2R e1hi) * Q2R err2)%R
        as bound_neg_nR1
          by (apply Rmult_le_compat_r; auto).
      unfold invertInterval in *; simpl in upperBound_nR2, upperBound_Ropp_nR2.
      (* Case distinction for the divisor range
         positive or negative in float and real valued execution *)
      destruct no_div_zero_real as [ real_iv_neg | real_iv_pos];
        destruct no_div_zero_float as [float_iv_neg | float_iv_pos].
      (* The range of the divisor lies in the range from -infinity until 0 *)
      * unfold widenIntv in *; simpl in *.
        rewrite <- Q2R_plus in float_iv_neg.
        rewrite <- Q2R0_is_0 in float_iv_neg.
        rewrite <- Q2R0_is_0 in real_iv_neg.
        pose proof (err_prop_inversion_neg float_iv_neg real_iv_neg err2_bounded valid_bounds_e2 H1 err2_pos) as err_prop_inv.
        rewrite Q2R_plus in float_iv_neg.
        rewrite Q2R0_is_0 in float_iv_neg.
        rewrite Q2R0_is_0 in real_iv_neg.
        rewrite Q2R_minus, Q2R_plus.
        repeat rewrite minAbs_negative_iv_is_hi; try lra.
        unfold Rdiv.
        repeat rewrite Q2R1.
        rewrite Rmult_1_l.
        (* Prove inequality to 0 in Q *)
        assert (e2lo == 0 -> False)
               by (rewrite <- Q2R0_is_0 in real_iv_neg; apply Rlt_Qlt in real_iv_neg;
                   assert (e2lo < 0) by (apply (Qle_lt_trans _ e2hi); try auto; apply Rle_Qle; lra);
                   lra).
        assert (e2hi == 0 -> False)
          by (rewrite <- Q2R0_is_0 in real_iv_neg; apply Rlt_Qlt in real_iv_neg; lra).
        rewrite Rabs_case_inverted in err1_bounded, err2_bounded.
        assert (nF1 <= Q2R err1 + nR1)%R as ub_nF1 by lra.
        assert (nR1 - Q2R err1 <= nF1)%R as lb_nF1 by lra.
        destruct err2_bounded as [[nR2_sub_pos err2_bounded] | [nR2_sub_neg err2_bounded]].
        (* Positive case for abs (nR2 - nF2) <= err2 *)
        { rewrite <- Ropp_mult_distr_l, <- Ropp_mult_distr_r, Ropp_involutive.
          apply Rgt_lt in nR2_sub_pos.
          assert (nF2 < nR2)%R as nF2_le_nR2 by lra.
          apply Ropp_lt_contravar in nF2_le_nR2.
          apply Rinv_lt_contravar in nF2_le_nR2; [ | apply Rmult_0_lt_preserving; try lra].
          repeat (rewrite <- Ropp_inv_permute in nF2_le_nR2; try lra).
          apply Ropp_lt_contravar in nF2_le_nR2.
          repeat rewrite Ropp_involutive in nF2_le_nR2.
          assert (/ nR2 - / nF2 < 0)%R as abs_inv_neg by lra.
          rewrite Rabs_left in err_prop_inv; try lra.
          rewrite Rsub_eq_Ropp_Rplus in err1_bounded, err2_bounded, err_prop_inv.
          assert (/nF2 <= /nR2 + Q2R err2 * / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))%R as error_prop_inv_up by lra.
          assert (/nR2 - Q2R err2 * / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) <= /nF2)%R as error_prop_inv_down by lra.
          (* Next do a case distinction for the absolute value*)
          unfold Rabs.
          destruct Rcase_abs.
          (* Case 1: Absolute value negative *)
          - rewrite Rsub_eq_Ropp_Rplus, Ropp_plus_distr.
            rewrite Ropp_involutive.
            (* To upper bound terms, do a case distinction for nF1 *)
            destruct (Rle_lt_dec 0 nF1).
            (* 0 <= nF1 *)
            + eapply Rle_trans.
              eapply Rplus_le_compat_l.
              eapply Rmult_le_compat_l; try lra.
              apply error_prop_inv_up.
              destruct (Rle_lt_dec 0 (/ nR2 + Q2R err2 * / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))).
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                apply Rmult_le_compat_r; try lra.
                apply ub_nF1.
                rewrite (Rmult_comm (Q2R err2) _).
                remember (/ ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) * Q2R err2)%R as err_inv.
                assert (- (nR1 * / nR2) + (Q2R err1 + nR1) * (/ nR2 + err_inv) = nR1 * err_inv + / nR2 * Q2R err1 + Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat_r.
                apply Rplus_le_compat; [ apply Rmult_le_compat_r; lra | ].
                apply Rmult_le_compat_r; try lra.
                repeat (rewrite Q2R_inv; try auto).
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                rewrite Rmult_comm.
                apply Rmult_le_compat_neg_l; try lra.
                apply lb_nF1.
                rewrite (Rmult_comm (Q2R err2) _).
                remember (/ ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) * Q2R err2)%R as err_inv.
                assert (- (nR1 * / nR2) + (/ nR2 + err_inv) * (nR1 - Q2R err1) =
                          nR1 * err_inv - / nR2 * Q2R err1 - Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                repeat rewrite Rsub_eq_Ropp_Rplus.
                apply Rplus_le_compat.
                { apply Rplus_le_compat; [ apply Rmult_le_compat_r; lra | ].
                  rewrite Ropp_mult_distr_l.
                  apply Rmult_le_compat_r; try lra.
                  repeat (rewrite Q2R_inv; try auto). }
                { assert (0 <= err_inv)%R.
                  - subst.
                    assert (0 < / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))%R
                      by (apply Rinv_0_lt_compat; apply Rmult_lt_0_inverting; try lra).
                    lra.
                  - assert (0 <= (Q2R err1 * err_inv))%R
                      by (rewrite <- (Rmult_0_l 0); apply Rmult_le_compat; lra).
                    apply (Rle_trans _ 0); lra. }
            (* nF1 < 0 *)
            + eapply Rle_trans.
              eapply Rplus_le_compat_l.
              eapply Rmult_le_compat_neg_l; try lra.
              apply error_prop_inv_down.
              destruct (Rle_lt_dec 0 (/ nR2 - Q2R err2 * / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))).
              * eapply Rle_trans.
                apply Rplus_le_compat_l.
                eapply Rmult_le_compat_r; try lra.
                apply ub_nF1.
                rewrite (Rmult_comm (Q2R err2)).
                remember (/ ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) * Q2R err2)%R as err_inv.
                assert (- (nR1 * / nR2) + (Q2R err1 + nR1) * (/ nR2 - err_inv) = - nR1 * err_inv + / nR2 * Q2R err1 - Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat.
                apply Rplus_le_compat.
                { apply Rmult_le_compat_r; try lra. }
                { apply Rmult_le_compat_r; try lra.
                    repeat (rewrite Q2R_inv; try auto). }
                { assert (0 <= err_inv)%R.
                  - subst.
                    assert (0 < / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))%R
                      by (apply Rinv_0_lt_compat; apply Rmult_lt_0_inverting; try lra).
                    lra.
                  - assert (0 <= (Q2R err1 * err_inv))%R
                      by (rewrite <- (Rmult_0_l 0); apply Rmult_le_compat; lra).
                    apply (Rle_trans _ 0); lra. }
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                rewrite Rmult_comm.
                eapply Rmult_le_compat_neg_l.
                hnf; left; auto.
                apply lb_nF1.
                rewrite Rmult_comm.
                setoid_rewrite (Rmult_comm (Q2R err2)).
                remember (/ ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) * Q2R err2)%R as err_inv.
                assert (- (/ nR2 * nR1) + (/ nR2 - err_inv) * (nR1 - Q2R err1) = - nR1 * err_inv + - / nR2 * Q2R err1 + Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat_r.
                apply Rplus_le_compat.
                { apply Rmult_le_compat_r; try lra. }
                { apply Rmult_le_compat_r; try lra.
                  repeat (rewrite Q2R_inv; try auto). }
          (* Case 2: Absolute value positive *)
          - rewrite Rsub_eq_Ropp_Rplus, Ropp_mult_distr_l.
            destruct (Rle_lt_dec 0 (- nF1)).
            (* 0 <= - nF1 *)
            + eapply Rle_trans.
              eapply Rplus_le_compat_l.
              eapply Rmult_le_compat_l; try lra.
              apply error_prop_inv_up.
              destruct (Rle_lt_dec 0 (/ nR2 + Q2R err2 * / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))).
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                apply Rmult_le_compat_r; try lra.
                apply Ropp_le_contravar.
                apply lb_nF1.
                rewrite (Rmult_comm (Q2R err2) _).
                remember (/ ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) * Q2R err2)%R as err_inv.
                assert (nR1 * / nR2 + - (nR1 - Q2R err1) * (/ nR2 + err_inv) = - nR1 * err_inv + / nR2 * Q2R err1 + Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat_r.
                apply Rplus_le_compat; [ apply Rmult_le_compat_r; lra | ].
                apply Rmult_le_compat_r; try lra.
                repeat (rewrite Q2R_inv; try auto).
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                rewrite Rmult_comm.
                apply Rmult_le_compat_neg_l; try lra.
                apply Ropp_le_contravar.
                apply ub_nF1.
                rewrite (Rmult_comm (Q2R err2) _).
                remember (/ ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) * Q2R err2)%R as err_inv.
                assert (nR1 * / nR2 + (/ nR2 + err_inv) *  - (Q2R err1 + nR1) =  - nR1 * err_inv + - / nR2 * Q2R err1 - Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                repeat rewrite Rsub_eq_Ropp_Rplus.
                apply Rplus_le_compat.
                { apply Rplus_le_compat; [ apply Rmult_le_compat_r; lra | ].
                  apply Rmult_le_compat_r; try lra.
                  repeat (rewrite Q2R_inv; try auto). }
                { assert (0 <= err_inv)%R.
                  - subst.
                    assert (0 < / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))%R
                      by (apply Rinv_0_lt_compat; apply Rmult_lt_0_inverting; try lra).
                    lra.
                  - assert (0 <= (Q2R err1 * err_inv))%R
                      by (rewrite <- (Rmult_0_l 0); apply Rmult_le_compat; lra).
                    apply (Rle_trans _ 0); lra. }
            (* - nF1 <= 0 *)
            + eapply Rle_trans.
              eapply Rplus_le_compat_l.
              eapply Rmult_le_compat_neg_l; try lra.
              apply error_prop_inv_down.
              destruct (Rle_lt_dec 0 (/ nR2 - Q2R err2 * / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))).
              * eapply Rle_trans.
                apply Rplus_le_compat_l.
                rewrite Rmult_comm.
                eapply Rmult_le_compat_l; try lra.
                apply Ropp_le_contravar.
                apply lb_nF1.
                rewrite (Rmult_comm (Q2R err2)).
                remember (/ ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) * Q2R err2)%R as err_inv.
                assert (nR1 * / nR2 + (/ nR2 - err_inv) * - (nR1 - Q2R err1) = nR1 * err_inv + / nR2 * Q2R err1 - Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat.
                apply Rplus_le_compat.
                { apply Rmult_le_compat_r; try lra. }
                { apply Rmult_le_compat_r; try lra.
                    repeat (rewrite Q2R_inv; try auto). }
                { assert (0 <= err_inv)%R.
                  - subst.
                    assert (0 < / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))%R
                      by (apply Rinv_0_lt_compat; apply Rmult_lt_0_inverting; try lra).
                    lra.
                  - assert (0 <= (Q2R err1 * err_inv))%R
                      by (rewrite <- (Rmult_0_l 0); apply Rmult_le_compat; lra).
                    apply (Rle_trans _ 0); lra. }
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                rewrite Rmult_comm.
                eapply Rmult_le_compat_neg_l.
                hnf; left; auto.
                apply Ropp_le_contravar.
                apply ub_nF1.
                setoid_rewrite (Rmult_comm (Q2R err2)).
                remember (/ ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) * Q2R err2)%R as err_inv.
                assert (nR1 * / nR2 + (/ nR2 - err_inv) * - (Q2R err1 + nR1) = nR1 * err_inv + - / nR2 * Q2R err1 + Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat_r.
                apply Rplus_le_compat.
                { apply Rmult_le_compat_r; try lra. }
                { apply Rmult_le_compat_r; try lra.
                  repeat (rewrite Q2R_inv; try auto). }
        }
        { rewrite <- Ropp_mult_distr_l, <- Ropp_mult_distr_r, Ropp_involutive.
          assert (nR2 <= nF2)%R as nR2_le_nF2 by lra.
          apply Ropp_le_contravar in nR2_le_nF2.
          apply Rinv_le_contravar in nR2_le_nF2; [ | lra].
          repeat (rewrite <- Ropp_inv_permute in nR2_le_nF2; try lra).
          apply Ropp_le_contravar in nR2_le_nF2.
          repeat rewrite Ropp_involutive in nR2_le_nF2.
          assert (0 <= / nR2 - / nF2)%R as abs_inv_pos by lra.
          rewrite Rabs_right in err_prop_inv; try lra.
          rewrite Rsub_eq_Ropp_Rplus in err1_bounded, err2_bounded, err_prop_inv.
          assert (/nF2 <= /nR2 + Q2R err2 * / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))%R as error_prop_inv_up by lra.
          assert (/nR2 - Q2R err2 * / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) <= /nF2)%R as error_prop_inv_down by lra.
          (* Next do a case distinction for the absolute value*)
          unfold Rabs.
          destruct Rcase_abs.
          - rewrite Rsub_eq_Ropp_Rplus, Ropp_plus_distr.
            rewrite Ropp_involutive.
            (* To upper bound terms, do a case distinction for nF1 *)
            destruct (Rle_lt_dec 0 nF1).
            + eapply Rle_trans.
              eapply Rplus_le_compat_l.
              eapply Rmult_le_compat_l; try lra.
              apply error_prop_inv_up.
              destruct (Rle_lt_dec 0 (/ nR2 + Q2R err2 * / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))).
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                apply Rmult_le_compat_r; try lra.
                apply ub_nF1.
                rewrite (Rmult_comm (Q2R err2) _).
                remember (/ ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) * Q2R err2)%R as err_inv.
                assert (- (nR1 * / nR2) + (Q2R err1 + nR1) * (/ nR2 + err_inv) = nR1 * err_inv + / nR2 * Q2R err1 + Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat_r.
                apply Rplus_le_compat; [ apply Rmult_le_compat_r; lra | ].
                apply Rmult_le_compat_r; try lra.
                repeat (rewrite Q2R_inv; try auto).
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                rewrite Rmult_comm.
                apply Rmult_le_compat_neg_l; try lra.
                apply lb_nF1.
                rewrite (Rmult_comm (Q2R err2) _).
                remember (/ ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) * Q2R err2)%R as err_inv.
                assert (- (nR1 * / nR2) + (/ nR2 + err_inv) * (nR1 - Q2R err1) = nR1 * err_inv - / nR2 * Q2R err1 - Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                repeat rewrite Rsub_eq_Ropp_Rplus.
                apply Rplus_le_compat.
                { apply Rplus_le_compat; [ apply Rmult_le_compat_r; lra | ].
                  rewrite Ropp_mult_distr_l.
                  apply Rmult_le_compat_r; try lra.
                  repeat (rewrite Q2R_inv; try auto). }
                { assert (0 <= err_inv)%R.
                  - subst.
                    assert (0 < / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))%R
                      by (apply Rinv_0_lt_compat; apply Rmult_lt_0_inverting; try lra).
                    lra.
                  - assert (0 <= (Q2R err1 * err_inv))%R
                      by (rewrite <- (Rmult_0_l 0); apply Rmult_le_compat; lra).
                    apply (Rle_trans _ 0); lra. }
            + eapply Rle_trans.
              eapply Rplus_le_compat_l.
              eapply Rmult_le_compat_neg_l; try lra.
              apply error_prop_inv_down.
              destruct (Rle_lt_dec 0 (/ nR2 - Q2R err2 * / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))).
              * eapply Rle_trans.
                apply Rplus_le_compat_l.
                eapply Rmult_le_compat_r; try lra.
                apply ub_nF1.
                rewrite (Rmult_comm (Q2R err2)).
                remember (/ ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) * Q2R err2)%R as err_inv.
                assert (- (nR1 * / nR2) + (Q2R err1 + nR1) * (/ nR2 - err_inv) = - nR1 * err_inv + / nR2 * Q2R err1 - Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat.
                apply Rplus_le_compat.
                { apply Rmult_le_compat_r; try lra. }
                { apply Rmult_le_compat_r; try lra.
                    repeat (rewrite Q2R_inv; try auto). }
                { assert (0 <= err_inv)%R.
                  - subst.
                    assert (0 < / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))%R
                      by (apply Rinv_0_lt_compat; apply Rmult_lt_0_inverting; try lra).
                    lra.
                  - assert (0 <= (Q2R err1 * err_inv))%R
                      by (rewrite <- (Rmult_0_l 0); apply Rmult_le_compat; lra).
                    apply (Rle_trans _ 0); lra. }
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                rewrite Rmult_comm.
                eapply Rmult_le_compat_neg_l.
                hnf; left; auto.
                apply lb_nF1.
                rewrite Rmult_comm.
                setoid_rewrite (Rmult_comm (Q2R err2)).
                remember (/ ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) * Q2R err2)%R as err_inv.
                assert (- (/ nR2 * nR1) + (/ nR2 - err_inv) * (nR1 - Q2R err1) = - nR1 * err_inv + - / nR2 * Q2R err1 + Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat_r.
                apply Rplus_le_compat.
                { apply Rmult_le_compat_r; try lra. }
                { apply Rmult_le_compat_r; try lra.
                  repeat (rewrite Q2R_inv; try auto). }
          - rewrite Rsub_eq_Ropp_Rplus.
            rewrite Ropp_mult_distr_l.
            destruct (Rle_lt_dec 0 (- nF1)).
            + eapply Rle_trans.
              eapply Rplus_le_compat_l.
              eapply Rmult_le_compat_l; try lra.
              apply error_prop_inv_up.
              destruct (Rle_lt_dec 0 (/ nR2 + Q2R err2 * / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))).
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                apply Rmult_le_compat_r; try lra.
                apply Ropp_le_contravar.
                apply lb_nF1.
                rewrite (Rmult_comm (Q2R err2) _).
                remember (/ ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) * Q2R err2)%R as err_inv.
                assert (nR1 * / nR2 + - (nR1 - Q2R err1) * (/ nR2 + err_inv) = - nR1 * err_inv + / nR2 * Q2R err1 + Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat_r.
                apply Rplus_le_compat; [ apply Rmult_le_compat_r; lra | ].
                apply Rmult_le_compat_r; try lra.
                repeat (rewrite Q2R_inv; try auto).
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                rewrite Rmult_comm.
                apply Rmult_le_compat_neg_l; try lra.
                apply Ropp_le_contravar.
                apply ub_nF1.
                rewrite (Rmult_comm (Q2R err2) _).
                remember (/ ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) * Q2R err2)%R as err_inv.
                assert (nR1 * / nR2 + (/ nR2 + err_inv) *  - (Q2R err1 + nR1) =  - nR1 * err_inv + - / nR2 * Q2R err1 - Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                repeat rewrite Rsub_eq_Ropp_Rplus.
                apply Rplus_le_compat.
                { apply Rplus_le_compat; [ apply Rmult_le_compat_r; lra | ].
                  apply Rmult_le_compat_r; try lra.
                  repeat (rewrite Q2R_inv; try auto). }
                { assert (0 <= err_inv)%R.
                  - subst.
                    assert (0 < / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))%R
                      by (apply Rinv_0_lt_compat; apply Rmult_lt_0_inverting; try lra).
                    lra.
                  - assert (0 <= (Q2R err1 * err_inv))%R
                      by (rewrite <- (Rmult_0_l 0); apply Rmult_le_compat; lra).
                    apply (Rle_trans _ 0); lra. }
            + eapply Rle_trans.
              eapply Rplus_le_compat_l.
              eapply Rmult_le_compat_neg_l; try lra.
              apply error_prop_inv_down.
              destruct (Rle_lt_dec 0 (/ nR2 - Q2R err2 * / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))).
              * eapply Rle_trans.
                apply Rplus_le_compat_l.
                rewrite Rmult_comm.
                eapply Rmult_le_compat_l; try lra.
                apply Ropp_le_contravar.
                apply lb_nF1.
                rewrite (Rmult_comm (Q2R err2)).
                remember (/ ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) * Q2R err2)%R as err_inv.
                assert (nR1 * / nR2 + (/ nR2 - err_inv) * - (nR1 - Q2R err1) = nR1 * err_inv + / nR2 * Q2R err1 - Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat.
                apply Rplus_le_compat.
                { apply Rmult_le_compat_r; try lra. }
                { apply Rmult_le_compat_r; try lra.
                    repeat (rewrite Q2R_inv; try auto). }
                { assert (0 <= err_inv)%R.
                  - subst.
                    assert (0 < / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))%R
                      by (apply Rinv_0_lt_compat; apply Rmult_lt_0_inverting; try lra).
                    lra.
                  - assert (0 <= (Q2R err1 * err_inv))%R
                      by (rewrite <- (Rmult_0_l 0); apply Rmult_le_compat; lra).
                    apply (Rle_trans _ 0); lra. }
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                rewrite Rmult_comm.
                eapply Rmult_le_compat_neg_l.
                hnf; left; auto.
                apply Ropp_le_contravar.
                apply ub_nF1.
                setoid_rewrite (Rmult_comm (Q2R err2)).
                remember (/ ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) * Q2R err2)%R as err_inv.
                assert (nR1 * / nR2 + (/ nR2 - err_inv) * - (Q2R err1 + nR1) = nR1 * err_inv + - / nR2 * Q2R err1 + Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat_r.
                apply Rplus_le_compat.
                { apply Rmult_le_compat_r; try lra. }
                { apply Rmult_le_compat_r; try lra.
                  repeat (rewrite Q2R_inv; try auto). }
        }
      * unfold widenIntv in *; simpl in *.
        exfalso.
        assert (Q2R e2lo - Q2R err2 <= Q2R e2hi)%R by lra.
        assert (Q2R e2hi < Q2R e2lo - Q2R err2)%R by (apply (Rlt_trans _ 0 _); auto).
        lra.
      * unfold widenIntv in *; simpl in *.
        exfalso.
        assert (Q2R e2lo <= Q2R e2hi + Q2R err2)%R by lra.
        assert (Q2R e2hi + Q2R err2 < Q2R e2lo)%R as hierr_lt_lo by (apply (Rlt_trans _ 0 _); auto).
        apply Rlt_not_le in hierr_lt_lo.
        apply hierr_lt_lo; auto.
      * (** FIXME: Get rid of rewrites by fixing Lemma **)
        rewrite <- Q2R_minus in float_iv_pos.
        rewrite <- Q2R0_is_0 in float_iv_pos.
        rewrite <- Q2R0_is_0 in real_iv_pos.
        pose proof (err_prop_inversion_pos float_iv_pos real_iv_pos err2_bounded valid_bounds_e2 H1 err2_pos) as err_prop_inv.
        rewrite Q2R_minus in float_iv_pos.
        rewrite Q2R0_is_0 in float_iv_pos.
        rewrite Q2R0_is_0 in real_iv_pos.
        rewrite Q2R_minus, Q2R_plus.
        repeat rewrite minAbs_positive_iv_is_lo; try lra.
        unfold Rdiv.
        repeat rewrite Q2R1.
        rewrite Rmult_1_l.
        (* Prove inequality to 0 in Q *)
        assert (e2lo == 0 -> False)
          by (rewrite <- Q2R0_is_0 in real_iv_pos; apply Rlt_Qlt in real_iv_pos; lra).
        assert (e2hi == 0 -> False)
          by (rewrite <- Q2R0_is_0 in real_iv_pos; apply Rlt_Qlt in real_iv_pos;
              assert (0 < e2hi) by (apply (Qlt_le_trans _ e2lo); try auto; apply Rle_Qle; lra);
              lra).
        rewrite Rabs_case_inverted in err1_bounded, err2_bounded.
        assert (nF1 <= Q2R err1 + nR1)%R as ub_nF1 by lra.
        assert (nR1 - Q2R err1 <= nF1)%R as lb_nF1 by lra.
        destruct err2_bounded as [[nR2_sub_pos err2_bounded] | [nR2_sub_neg err2_bounded]].
        { apply Rgt_lt in nR2_sub_pos.
          assert (nF2 < nR2)%R as nF2_le_nR2 by lra.
          apply Rinv_lt_contravar in nF2_le_nR2; [ | apply Rmult_0_lt_preserving; try lra].
          assert (/ nR2 - / nF2 <= 0)%R as abs_inv_neg by lra.
          rewrite Rabs_left in err_prop_inv; try lra.
          rewrite Rsub_eq_Ropp_Rplus in err1_bounded, err2_bounded.
          assert (/nR2 - Q2R err2 * / ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)) <= /nF2)%R as error_prop_inv_down by lra.
          assert (/nF2 <= /nR2 + Q2R err2 * / ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)))%R as error_prop_inv_up by lra.
          (* Next do a case distinction for the absolute value*)
          unfold Rabs.
          destruct Rcase_abs.
          - rewrite Rsub_eq_Ropp_Rplus, Ropp_plus_distr.
            rewrite Ropp_involutive.
            (* To upper bound terms, do a case distinction for nF1 *)
            destruct (Rle_lt_dec 0 nF1).
            + eapply Rle_trans.
              eapply Rplus_le_compat_l.
              eapply Rmult_le_compat; try lra.
              hnf. left.
              apply Rinv_0_lt_compat; try lra.
              apply ub_nF1.
              apply error_prop_inv_up.
              rewrite (Rmult_comm (Q2R err2) _).
              remember (/ ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)) * Q2R err2)%R as err_inv.
              assert (- (nR1 * / nR2) + (Q2R err1 + nR1) * (/ nR2 + err_inv) = nR1 * err_inv + / nR2 * Q2R err1 + Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
              rewrite simpl_properr.
              apply Rplus_le_compat_r.
              apply Rplus_le_compat; [ apply Rmult_le_compat_r; lra | ].
              apply Rmult_le_compat; try lra.
              * hnf; left. apply Rinv_0_lt_compat; lra.
              * repeat (rewrite Q2R_inv; try auto).
            + eapply Rle_trans.
              eapply Rplus_le_compat_l.
              eapply Rmult_le_compat_neg_l; try lra.
              apply error_prop_inv_down.
              destruct (Rle_lt_dec 0 (/ nR2 - Q2R err2 * / ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)))).
              * eapply Rle_trans.
                apply Rplus_le_compat_l.
                eapply Rmult_le_compat_r; try lra.
                apply ub_nF1.
                rewrite (Rmult_comm (Q2R err2)).
                remember (/ ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)) * Q2R err2)%R as err_inv.
                assert (- (nR1 * / nR2) + (Q2R err1 + nR1) * (/ nR2 - err_inv) = - nR1 * err_inv + / nR2 * Q2R err1 - Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat.
                apply Rplus_le_compat.
                { apply Rmult_le_compat_r; try lra. }
                { apply Rmult_le_compat_r; try lra.
                    repeat (rewrite Q2R_inv; try auto). }
                { assert (0 <= err_inv)%R.
                  - subst.
                    assert (0 < / ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)))%R
                      by (apply Rinv_0_lt_compat; apply Rmult_0_lt_preserving; try lra).
                    lra.
                  - assert (0 <= (Q2R err1 * err_inv))%R
                      by (rewrite <- (Rmult_0_l 0); apply Rmult_le_compat; lra).
                    apply (Rle_trans _ 0); lra. }
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                rewrite Rmult_comm.
                eapply Rmult_le_compat_neg_l.
                hnf; left; auto.
                apply lb_nF1.
                rewrite Rmult_comm.
                setoid_rewrite (Rmult_comm (Q2R err2)).
                remember (/ ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)) * Q2R err2)%R as err_inv.
                assert (- (/ nR2 * nR1) + (/ nR2 - err_inv) * (nR1 - Q2R err1) = - nR1 * err_inv + - / nR2 * Q2R err1 + Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat_r.
                apply Rplus_le_compat.
                { apply Rmult_le_compat_r; try lra. }
                { apply Rmult_le_compat_r; try lra.
                  repeat (rewrite Q2R_inv; try auto). }
          - rewrite Rsub_eq_Ropp_Rplus.
            rewrite Ropp_mult_distr_l.
            destruct (Rle_lt_dec 0 (- nF1)).
            + eapply Rle_trans.
              eapply Rplus_le_compat_l.
              eapply Rmult_le_compat; try lra.
              hnf; left.
              apply Rinv_0_lt_compat.
              apply (Rlt_le_trans _ (Q2R e2lo - Q2R err2)); try lra.
              apply Ropp_le_contravar.
              apply lb_nF1.
              apply error_prop_inv_up.
              rewrite (Rmult_comm (Q2R err2) _).
              remember (/ ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)) * Q2R err2)%R as err_inv.
              assert ((nR1 * / nR2) + - (nR1 - Q2R err1) * (/ nR2 + err_inv) = - nR1 * err_inv + / nR2 * Q2R err1 + Q2R err1 * err_inv)%R
                as simpl_properr by lra.
              rewrite simpl_properr.
              apply Rplus_le_compat_r.
              apply Rplus_le_compat.
              * apply Rmult_le_compat_r; try lra.
              * apply Rmult_le_compat_r; try lra.
                  repeat (rewrite Q2R_inv; try auto).
            + eapply Rle_trans.
              eapply Rplus_le_compat_l.
              eapply Rmult_le_compat_neg_l; try lra.
              apply error_prop_inv_down.
              destruct (Rle_lt_dec 0 (/ nR2 - Q2R err2 * / ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)))).
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                eapply Rmult_le_compat_r; try lra.
                apply Ropp_le_contravar.
                apply lb_nF1.
                rewrite Rmult_comm.
                setoid_rewrite (Rmult_comm (Q2R err2)).
                remember (/ ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)) * Q2R err2)%R as err_inv.
                assert (/ nR2 * nR1 + - (nR1 - Q2R err1) * (/ nR2 - err_inv) = nR1 * err_inv + / nR2 * Q2R err1 - Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat.
                apply Rplus_le_compat.
                { apply Rmult_le_compat_r; try lra. }
                { apply Rmult_le_compat_r; try lra.
                  repeat (rewrite Q2R_inv; try auto). }
                { assert (0 <= err_inv)%R.
                  - subst.
                    assert (0 < / ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)))%R
                      by (apply Rinv_0_lt_compat; apply Rmult_0_lt_preserving; try lra).
                          lra.
                  - assert (0 <= (Q2R err1 * err_inv))%R
                      by (rewrite <- (Rmult_0_l 0); apply Rmult_le_compat; lra).
                    apply (Rle_trans _ 0); lra. }
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                rewrite Rmult_comm.
                eapply Rmult_le_compat_neg_l.
                hnf; left; auto.
                apply Ropp_le_contravar.
                apply ub_nF1.
                rewrite (Rmult_comm (Q2R err2)).
                remember (/ ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)) * Q2R err2)%R as err_inv.
                assert (nR1 * / nR2 + (/ nR2 - err_inv) * - (Q2R err1 + nR1) = nR1 * err_inv + - / nR2 * Q2R err1 + Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat_r.
                apply Rplus_le_compat.
                { apply Rmult_le_compat_r; try lra. }
                { apply Rmult_le_compat_r; try lra.
                  repeat (rewrite Q2R_inv; try auto). }
        }
        {
          apply Rminus_le in nR2_sub_neg.
          apply Rinv_le_contravar in nR2_sub_neg; [ | lra].
          assert (0 <= / nR2 - / nF2)%R as abs_inv_pos by lra.
          rewrite Rabs_right in err_prop_inv; try lra.
          rewrite Rsub_eq_Ropp_Rplus in err1_bounded, err2_bounded.
          rewrite Ropp_plus_distr in err2_bounded.
          rewrite Ropp_involutive in err2_bounded.
          assert (/nR2 - Q2R err2 * / ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)) <= /nF2)%R as error_prop_inv_down by lra.
          assert (/nF2 <= /nR2 + Q2R err2 * / ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)))%R as error_prop_inv_up by lra.
          (* Next do a case distinction for the absolute value*)
          unfold Rabs.
          destruct Rcase_abs.
          - rewrite Rsub_eq_Ropp_Rplus, Ropp_plus_distr.
            rewrite Ropp_involutive.
            (* To upper bound terms, do a case distinction for nF1 *)
            destruct (Rle_lt_dec 0 nF1).
            + eapply Rle_trans.
              eapply Rplus_le_compat_l.
              eapply Rmult_le_compat; try lra.
              hnf. left.
              apply Rinv_0_lt_compat; try lra.
              apply ub_nF1.
              apply error_prop_inv_up.
              rewrite (Rmult_comm (Q2R err2) _).
              remember (/ ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)) * Q2R err2)%R as err_inv.
              assert (- (nR1 * / nR2) + (Q2R err1 + nR1) * (/ nR2 + err_inv) = nR1 * err_inv + / nR2 * Q2R err1 + Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
              rewrite simpl_properr.
              apply Rplus_le_compat_r.
              apply Rplus_le_compat; [ apply Rmult_le_compat_r; lra | ].
              apply Rmult_le_compat; try lra.
              * hnf; left. apply Rinv_0_lt_compat; lra.
              * repeat (rewrite Q2R_inv; try auto).
            + eapply Rle_trans.
              eapply Rplus_le_compat_l.
              eapply Rmult_le_compat_neg_l; try lra.
              apply error_prop_inv_down.
              destruct (Rle_lt_dec 0 (/ nR2 - Q2R err2 * / ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)))).
              * eapply Rle_trans.
                apply Rplus_le_compat_l.
                eapply Rmult_le_compat_r; try lra.
                apply ub_nF1.
                rewrite (Rmult_comm (Q2R err2)).
                remember (/ ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)) * Q2R err2)%R as err_inv.
                assert (- (nR1 * / nR2) + (Q2R err1 + nR1) * (/ nR2 - err_inv) = - nR1 * err_inv + / nR2 * Q2R err1 - Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat.
                apply Rplus_le_compat.
                { apply Rmult_le_compat_r; try lra. }
                { apply Rmult_le_compat_r; try lra.
                    repeat (rewrite Q2R_inv; try auto). }
                { assert (0 <= err_inv)%R.
                  - subst.
                    assert (0 < / ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)))%R
                      by (apply Rinv_0_lt_compat; apply Rmult_0_lt_preserving; try lra).
                    lra.
                  - assert (0 <= (Q2R err1 * err_inv))%R
                      by (rewrite <- (Rmult_0_l 0); apply Rmult_le_compat; lra).
                    apply (Rle_trans _ 0); lra. }
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                rewrite Rmult_comm.
                eapply Rmult_le_compat_neg_l.
                hnf; left; auto.
                apply lb_nF1.
                rewrite Rmult_comm.
                setoid_rewrite (Rmult_comm (Q2R err2)).
                remember (/ ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)) * Q2R err2)%R as err_inv.
                assert (- (/ nR2 * nR1) + (/ nR2 - err_inv) * (nR1 - Q2R err1) = - nR1 * err_inv + - / nR2 * Q2R err1 + Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat_r.
                apply Rplus_le_compat.
                { apply Rmult_le_compat_r; try lra. }
                { apply Rmult_le_compat_r; try lra.
                  repeat (rewrite Q2R_inv; try auto). }
          - rewrite Rsub_eq_Ropp_Rplus.
            rewrite Ropp_mult_distr_l.
            destruct (Rle_lt_dec 0 (- nF1)).
            + eapply Rle_trans.
              eapply Rplus_le_compat_l.
              eapply Rmult_le_compat; try lra.
              hnf; left.
              apply Rinv_0_lt_compat.
              apply (Rlt_le_trans _ (Q2R e2lo - Q2R err2)); try lra.
              apply Ropp_le_contravar.
              apply lb_nF1.
              apply error_prop_inv_up.
              rewrite (Rmult_comm (Q2R err2) _).
              remember (/ ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)) * Q2R err2)%R as err_inv.
              assert ((nR1 * / nR2) + - (nR1 - Q2R err1) * (/ nR2 + err_inv) = - nR1 * err_inv + / nR2 * Q2R err1 + Q2R err1 * err_inv)%R
                as simpl_properr by lra.
              rewrite simpl_properr.
              apply Rplus_le_compat_r.
              apply Rplus_le_compat.
              * apply Rmult_le_compat_r; try lra.
              * apply Rmult_le_compat_r; try lra.
                  repeat (rewrite Q2R_inv; try auto).
            + eapply Rle_trans.
              eapply Rplus_le_compat_l.
              eapply Rmult_le_compat_neg_l; try lra.
              apply error_prop_inv_down.
              destruct (Rle_lt_dec 0 (/ nR2 - Q2R err2 * / ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)))).
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                eapply Rmult_le_compat_r; try lra.
                apply Ropp_le_contravar.
                apply lb_nF1.
                rewrite Rmult_comm.
                setoid_rewrite (Rmult_comm (Q2R err2)).
                remember (/ ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)) * Q2R err2)%R as err_inv.
                assert (/ nR2 * nR1 + - (nR1 - Q2R err1) * (/ nR2 - err_inv) = nR1 * err_inv + / nR2 * Q2R err1 - Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat.
                apply Rplus_le_compat.
                { apply Rmult_le_compat_r; try lra. }
                { apply Rmult_le_compat_r; try lra.
                  repeat (rewrite Q2R_inv; try auto). }
                { assert (0 <= err_inv)%R.
                  - subst.
                    assert (0 < / ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)))%R
                      by (apply Rinv_0_lt_compat; apply Rmult_0_lt_preserving; try lra).
                          lra.
                  - assert (0 <= (Q2R err1 * err_inv))%R
                      by (rewrite <- (Rmult_0_l 0); apply Rmult_le_compat; lra).
                    apply (Rle_trans _ 0); lra. }
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                rewrite Rmult_comm.
                eapply Rmult_le_compat_neg_l.
                hnf; left; auto.
                apply Ropp_le_contravar.
                apply ub_nF1.
                rewrite (Rmult_comm (Q2R err2)).
                remember (/ ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)) * Q2R err2)%R as err_inv.
                assert (nR1 * / nR2 + (/ nR2 - err_inv) * - (Q2R err1 + nR1) = nR1 * err_inv + - / nR2 * Q2R err1 + Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat_r.
                apply Rplus_le_compat.
                { apply Rmult_le_compat_r; try lra. }
                { apply Rmult_le_compat_r; try lra.
                  repeat (rewrite Q2R_inv; try auto). }
        }
    + remember (divideIntv (widenIntv (e1lo, e1hi) err1) (widenIntv (e2lo, e2hi) err2)) as iv.
      iv_assert iv iv_unf.
      destruct iv_unf as [ivl [ivh iv_unf]].
      rewrite iv_unf.
      assert (ivlo iv = ivl) as ivlo_eq by (rewrite iv_unf; auto).
      assert (ivhi iv = ivh) as ivhi_eq by (rewrite iv_unf; auto).
      rewrite <- ivlo_eq, <- ivhi_eq.
      assert (IVhi (widenInterval (Q2R e2lo, Q2R e2hi) (Q2R err2)) < 0 \/ 0 < IVlo (widenInterval (Q2R e2lo, Q2R e2hi) (Q2R err2)))%R
        as no_div_zero_widen
             by (unfold widenInterval in *; simpl in *; rewrite Q2R_plus, Q2R_minus in no_div_zero_float; auto).
      pose proof (IntervalArith.interval_division_valid _ _ no_div_zero_widen H0 H1) as valid_div_float.
      unfold widenInterval in *; simpl in *.
      assert (e2lo - err2 == 0 -> False).
      * hnf; intros.
        destruct no_div_zero_float as [float_iv | float_iv]; rewrite <- Q2R0_is_0 in float_iv; apply Rlt_Qlt in float_iv; try lra.
        assert (Q2R e2lo - Q2R err2 <= Q2R e2hi + Q2R err2)%R by lra.
        rewrite<- Q2R_minus, <- Q2R_plus in H5.
        apply Rle_Qle in H5. lra.
      * assert (e2hi + err2 == 0 -> False).
        { hnf; intros.
          destruct no_div_zero_float as [float_iv | float_iv]; rewrite <- Q2R0_is_0 in float_iv; apply Rlt_Qlt in float_iv; try lra.
          assert (Q2R e2lo - Q2R err2 <= Q2R e2hi + Q2R err2)%R by lra.
          rewrite<- Q2R_minus, <- Q2R_plus in H6.
          apply Rle_Qle in H6. lra. }
        { destruct valid_div_float.
          unfold RmaxAbsFun.
          apply Rmult_le_compat_r.
          apply mTypeToQ_pos_R.
          rewrite <- maxAbs_impl_RmaxAbs.
          unfold RmaxAbsFun.
          apply RmaxAbs; subst; simpl in *.
          - rewrite Q2R_min4.
            repeat rewrite Q2R_mult.
            + repeat (rewrite Q2R_inv; auto).
              repeat rewrite Q2R_minus, Q2R_plus; auto.
          - rewrite Q2R_max4.
            repeat rewrite Q2R_mult.
            repeat (rewrite Q2R_inv; auto).
            repeat rewrite Q2R_minus.
            repeat rewrite Q2R_plus; auto. }
  - apply le_neq_bool_to_lt_prop in no_div_zero_float.
    unfold widenInterval in *; simpl in *.
    assert (e2lo - err2 == 0 -> False).
    + hnf; intros.
      destruct no_div_zero_float as [float_iv | float_iv]; try lra.
      assert (Q2R e2lo - Q2R err2 <= Q2R e2hi + Q2R err2)%R by lra.
      rewrite<- Q2R_minus, <- Q2R_plus in H3.
      apply Rle_Qle in H3. lra.
    + assert (e2hi + err2 == 0 -> False).
      * hnf; intros.
        destruct no_div_zero_float as [float_iv | float_iv]; try lra.
        assert (Q2R e2lo - Q2R err2 <= Q2R e2hi + Q2R err2)%R by lra.
        rewrite<- Q2R_minus, <- Q2R_plus in H4.
        apply Rle_Qle in H4. lra.
      * unfold widenIntv; simpl.
        hnf. intros.
        assert (forall a, Qabs a == 0 -> a == 0).
        { intros. unfold Qabs in H5. destruct a.
          rewrite <- Z.abs_0 in H5. inversion H5. rewrite Zmult_1_r in *.
          rewrite Z.abs_0_iff in H7.
          rewrite H7. rewrite Z.abs_0. hnf. simpl; auto. }
        { assert (minAbs (e2lo - err2, e2hi + err2) == 0 -> False).
          - unfold minAbs. unfold fst, snd. apply Q.min_case_strong.
            + intros. apply H7; rewrite H6; auto.
            + intros. apply H2. rewrite (H5 (e2lo-err2) H7). hnf. auto.
            + intros. apply H3. rewrite H5; auto. hnf; auto.
          - rewrite <- (Qmult_0_l (minAbs (e2lo - err2, e2hi + err2))) in H4.
            rewrite (Qmult_inj_r) in H4; auto. }
Qed.


Lemma validErrorboundCorrectRounding E1 E2 absenv (e: exp Q) (nR nF nF1: R) (err err':error) (elo ehi alo ahi: Q) dVars (m: mType) (machineEpsilon:mType) Gamma defVars:
  eval_exp E1 (toRMap defVars) (toREval (toRExp e)) nR M0 ->
  eval_exp E2 defVars (toRExp e) nF1 m ->
  eval_exp (updEnv 1 nF1 emptyEnv)
           (updDefVars 1 m defVars)
           (toRExp (Downcast machineEpsilon (Var Q 1))) nF machineEpsilon ->
  typeCheck (Downcast machineEpsilon e) defVars Gamma = true ->
  validErrorbound (Downcast machineEpsilon e) Gamma absenv dVars = true ->
  (Q2R elo <= nR <= Q2R ehi)%R ->
  absenv e = ((elo, ehi), err) ->
  absenv (Downcast machineEpsilon e) = ((alo, ahi), err') ->
  (Rabs (nR - nF1) <= (Q2R err))%R ->
  (Rabs (nR - nF) <= (Q2R err'))%R.
Proof.
  intros eval_real eval_float eval_float_rnd subexpr_ok valid_error valid_intv absenv_e absenv_rnd err_bounded.
  unfold validErrorbound in valid_error.
  rewrite absenv_rnd in valid_error.
  rewrite absenv_e in valid_error.
  case_eq (Gamma (Downcast machineEpsilon e)); intros; rewrite H in valid_error; [ | inversion valid_error ].
  simpl in subexpr_ok; rewrite H in subexpr_ok.
  case_eq (Gamma e); intros; rewrite H0 in subexpr_ok; [ | inversion subexpr_ok ].
  andb_to_prop subexpr_ok.
  apply mTypeEq_compat_eq in L0; subst.
  andb_to_prop valid_error.
  simpl in *.
  eapply Rle_trans.
  eapply round_abs_err_bounded; eauto.
  assert (contained nR (Q2R elo, Q2R ehi)) as valid_intv_c by (auto).
  pose proof (distance_gives_iv _ _ valid_intv_c err_bounded) as dgi.
  destruct dgi as [dgi1 dgi2]; simpl in dgi1; simpl in dgi2.
  apply (Rle_trans _ (Q2R err + Q2R (maxAbs (widenIntv (elo, ehi) err)) * Q2R (mTypeToQ m0)) _).
  + apply Rplus_le_compat_l.
    apply Rmult_le_compat_r.
    * rewrite <- Q2R0_is_0.
      apply Qle_Rle.
      apply mTypeToQ_pos_Q.
    * remember (widenIntv (elo, ehi) err) as iv_widen.
      destruct iv_widen as [eloR ehiR].
      rewrite <- maxAbs_impl_RmaxAbs.
      apply contained_leq_maxAbs.
      unfold widenIntv in Heqiv_widen; simpl in Heqiv_widen.
      inversion Heqiv_widen.
      rewrite Q2R_minus.
      rewrite Q2R_plus.
      split; assumption.
  + apply Qle_bool_iff in R2.
    rewrite <- Q2R_mult.
    rewrite <- Q2R_plus.
    apply Qle_Rle.
    assumption.
Qed.

(**
   Soundness theorem for the error bound validator.
   Proof is by induction on the expression e.
   Each case requires the application of one of the soundness lemmata proven before
 **)
Theorem validErrorbound_sound (e:exp Q):
  forall E1 E2 fVars dVars absenv nR err P elo ehi Gamma defVars,
    typeCheck e defVars Gamma = true ->
    approxEnv E1 defVars absenv fVars dVars E2 ->
    NatSet.Subset (NatSet.diff (Expressions.usedVars e) dVars) fVars ->
    eval_exp E1 (toRMap defVars) (toREval (toRExp e)) nR M0 ->
    validErrorbound e Gamma absenv dVars = true ->
    validIntervalbounds e absenv P dVars = true ->
    (forall v1, NatSet.mem v1 dVars = true ->
              exists vr, E1 v1 = Some vr /\
                    (Q2R (fst (fst (absenv (Var Q v1)))) <= vr <= Q2R (snd (fst (absenv (Var Q v1)))))%R) ->
    (forall v, NatSet.mem v fVars= true ->
          exists vR, E1 v = Some vR /\
                (Q2R (fst (P v)) <= vR <= Q2R (snd (P v)))%R) ->
    (forall v1 : NatSet.elt, (v1) mem (fVars ∪ dVars) = true ->
                        exists m0 : mType, defVars v1 = Some m0) ->
    absenv e = ((elo,ehi),err) ->
    (exists nF m,
      eval_exp E2 defVars (toRExp e) nF m) /\
    (forall nF m,
        eval_exp E2 defVars (toRExp e) nF m ->
    (Rabs (nR - nF) <= (Q2R err))%R).
Proof.
  revert e; induction e;
    intros * typing_ok approxCEnv fVars_subset eval_real valid_error valid_intv
                       valid_dVars P_valid vars_wellTyped absenv_eq.
  - inversion typing_ok; subst.
    split.
    + eapply validErrorboundCorrectVariable_eval; eauto.
    + intros * eval_float. eapply validErrorboundCorrectVariable; eauto.
  - inversion typing_ok; subst.
    case_eq (Gamma (Const m v)); intros; rewrite H in H0; [ | inversion H0 ].
    apply mTypeEq_compat_eq in H0; subst.
    assert (Q2R elo <= nR <= Q2R ehi)%R.
    {
      simpl in valid_intv.
      rewrite absenv_eq in valid_intv.
      simpl in eval_real; inversion eval_real; subst.
      simpl in *; rewrite Q2R0_is_0 in H3.
      rewrite delta_0_deterministic; auto.
      unfold isSupersetIntv in valid_intv; apply andb_true_iff in valid_intv; destruct valid_intv.
      canonize_hyps.
      simpl in H0,H1.
      split; auto. }
    split.
    + eapply validErrorboundCorrectConstant_eval; eauto.
    + intros * eval_float.
      pose proof (typingSoundnessExp _ _ typing_ok eval_float).
      rewrite H in H1.
      inversion H1; subst.
      eapply validErrorboundCorrectConstant; eauto.
  - simpl in *.
    rewrite absenv_eq in valid_error.
    case_eq (Gamma (Unop u e)); intros * type_unop;
      rewrite type_unop in valid_error; [ | inversion valid_error ].
    andb_to_prop valid_error.
    destruct u; try congruence.
    destruct (absenv e) as [[e_lo e_hi] err_e] eqn:absenv_e.
    repeat
      match goal with
      | [H: _ = true |- _] => andb_to_prop H
      end.
    canonize_hyps.
    inversion eval_real; subst.
    destruct (IHe E1 E2 fVars dVars absenv v1 err_e P e_lo e_hi Gamma defVars) as [[nF [mF eval_float]] valid_bounds_e];
      eauto.
    + simpl in typing_ok.
      rewrite type_unop in typing_ok.
      case_eq (Gamma e); intros * type_e; rewrite type_e in typing_ok; [ | inversion typing_ok ].
      andb_to_prop typing_ok; auto.
    + simpl in valid_intv.
      rewrite absenv_eq in valid_intv; andb_to_prop valid_intv.
      auto.
    + split.
      * exists (evalUnop Neg nF); exists mF.
        simpl; eauto.
      * intros * eval_float_op.
        inversion eval_float_op; subst; simpl.
        apply bound_flip_sub.
        rewrite Qeq_bool_iff in R0.
        apply Qeq_eqR in R0; rewrite R0.
        simpl.
        eapply valid_bounds_e; eauto.
  - simpl in valid_error.
    destruct (absenv e1) as [[ivlo1 ivhi1] err1] eqn:absenv_e1;
      destruct (absenv e2) as [[ivlo2 ivhi2] err2] eqn:absenv_e2.
    subst; simpl in *.
    rewrite absenv_eq, absenv_e1, absenv_e2 in *.
    case_eq (Gamma (Binop b e1 e2));
      intros * type_b; rewrite type_b in *; [ | inversion valid_error ].
    case_eq (Gamma e1);
      intros * type_e1; rewrite type_e1 in typing_ok; [ | inversion typing_ok ].
    case_eq (Gamma e2);
      intros * type_e2; rewrite type_e2 in typing_ok; [ | inversion typing_ok ].
    repeat match goal with
           | [H: _ = true |- _] => andb_to_prop H
           end.
    type_conv.
    inversion eval_real; subst.
    assert (m2 = M0 /\ m3 = M0) as [? ?] by (split; eapply toRMap_eval_M0; eauto); subst.
    destruct (IHe1 E1 E2 fVars dVars absenv v1 err1 P ivlo1 ivhi1 Gamma defVars)
      as [[vF1 [mF1 eval_float_e1]] bounded_e1];
      try auto; set_tac.
    destruct (IHe2 E1 E2 fVars dVars absenv v2 err2 P ivlo2 ivhi2 Gamma defVars)
      as [[vF2 [mF2 eval_float_e2]] bounded_e2];
      try auto; set_tac.
    destruct (validIntervalbounds_sound _ _  _ E1 defVars L0 (fVars := fVars) (dVars:=dVars))
      as [v1' [eval_real_e1 bounds_e1]];
      try auto; set_tac.
    rewrite absenv_e1 in bounds_e1.
    pose proof (meps_0_deterministic _ eval_real_e1 H5); subst. clear H5.
    destruct (validIntervalbounds_sound _ _ _ E1 defVars R0 (fVars:= fVars) (dVars:=dVars))
      as [v2' [eval_real_e2 bounds_e2]];
      try auto; set_tac.
    rewrite absenv_e2 in bounds_e2; simpl in *.
    pose proof (meps_0_deterministic _ eval_real_e2 H6); subst; clear H6.
    assert (b = Div -> ~ (vF2 = 0)%R) as no_div_zero.
    { intros; subst; simpl in *.
      andb_to_prop R2.
      apply le_neq_bool_to_lt_prop in L1.
      assert (contained vF1 (widenInterval (Q2R ivlo1, Q2R ivhi1) (Q2R err1)))
        as bounds_float_e1.
      { eapply distance_gives_iv; simpl;
          try eauto. }
      assert (contained vF2 (widenInterval (Q2R ivlo2, Q2R ivhi2) (Q2R err2)))
        as bounds_float_e2.
      { eapply distance_gives_iv; simpl;
          try eauto. }
      simpl in *.
      intro; subst.
      rewrite <- Q2R0_is_0 in bounds_float_e2.
      destruct L1 as [nodivzero | nodivzero];
        apply Qlt_Rlt in nodivzero;
        try rewrite Q2R_plus in *; try rewrite Q2R_minus in *; lra.
    }
    split.
    + repeat eexists; econstructor; eauto.
      rewrite Rabs_right; try lra.
      instantiate (1 := 0%R).
      apply mTypeToQ_pos_R.
      apply Rle_ge.
      hnf; right. reflexivity.
    + intros * eval_float.
      clear eval_float_e1 eval_float_e2.
      inversion eval_float; subst.
      eapply (binary_unfolding _ H4 H5 H9) in eval_float; try auto.
      destruct b.
      * eapply (validErrorboundCorrectAddition (e1:=e1) absenv); eauto.
        { simpl.
          rewrite type_b.
          rewrite type_e1.
          rewrite type_e2.
          rewrite mTypeEq_refl, R3, R4; auto. }
        { simpl.
          rewrite absenv_eq.
          rewrite type_b.
          rewrite L, L2, R1; simpl.
          rewrite absenv_e1, absenv_e2.
          auto. }
      * eapply (validErrorboundCorrectSubtraction (e1:=e1) absenv); eauto.
        { simpl; rewrite type_b, type_e1, type_e2, mTypeEq_refl, R3, R4; auto. }
        { simpl; rewrite absenv_eq, type_b, L, L2, R1; simpl.
          rewrite absenv_e1, absenv_e2; auto. }
      *  eapply (validErrorboundCorrectMult (e1 := e1) absenv); eauto.
         { simpl; rewrite type_b, type_e1, type_e2, mTypeEq_refl, R3, R4; auto. }
         { simpl; rewrite absenv_eq, type_b, L, L2, R1; simpl.
           rewrite absenv_e1, absenv_e2; auto. }
      * eapply (validErrorboundCorrectDiv (e1 := e1) absenv); eauto.
        {  simpl; rewrite type_b, type_e1, type_e2, mTypeEq_refl, R3, R4; auto. }
        { simpl; rewrite absenv_eq, type_b, L, L2, R1; simpl.
          rewrite absenv_e1, absenv_e2; auto. }
        { andb_to_prop R; auto. }
  - destruct (absenv e) as [[ivlo_e ivhi_e] err_e] eqn: absenv_e.
    subst; simpl in *.
    rewrite absenv_eq, absenv_e in *.
    case_eq (Gamma (Downcast m e));
      intros * type_b; rewrite type_b in valid_error; [ | inversion valid_error ].
    rewrite type_b in typing_ok; simpl in typing_ok.
    case_eq (Gamma e);
      intros * type_e; rewrite type_e in typing_ok; [ | inversion typing_ok ].
    andb_to_prop typing_ok.
    apply mTypeEq_compat_eq in L0; subst.
    andb_to_prop valid_error.
    simpl in valid_intv.
    andb_to_prop valid_intv.
    inversion eval_real; subst.
    apply M0_least_precision in H1.
    subst.
    destruct (IHe E1 E2 fVars dVars absenv v1 err_e P ivlo_e ivhi_e Gamma defVars)
      as [[vF [mF eval_float_e]] bounded_e];
      try auto; set_tac.
    pose proof (typingSoundnessExp _ _ R eval_float_e).
    rewrite H in type_e; inversion type_e; subst.
    destruct (validIntervalbounds_sound _ _  _ E1 defVars L1 (fVars := fVars) (dVars:=dVars))
      as [v1' [eval_real_e bounds_e]];
      try auto; set_tac.
    rewrite absenv_e in bounds_e.
    pose proof (meps_0_deterministic _ eval_real_e H4); subst. clear H4.
    split.
    + exists (perturb vF (Q2R (mTypeToQ m0)));
        exists m0.
      econstructor; eauto.
      rewrite Rabs_right; try lra.
      auto using Rle_ge, mTypeToQ_pos_R.
    + rewrite Q2R0_is_0 in *. rewrite (delta_0_deterministic _ delta) in *; try auto.
      intros * eval_float.
      clear eval_float_e.
      inversion eval_float; subst.
      eapply validErrorboundCorrectRounding; eauto.
      * simpl. eapply Downcast_dist'; eauto.
        constructor. unfold updDefVars. rewrite Nat.eqb_refl; auto.
        unfold updEnv; simpl; auto.
      * simpl. rewrite type_b, H.
        rewrite mTypeEq_refl, R0, R; auto.
      * simpl. rewrite absenv_eq, absenv_e, type_b, L; simpl.
        rewrite L0; auto.
Unshelve.
intros. auto.
Qed.

Theorem validErrorboundCmd_gives_eval (f:cmd Q) :
  forall (absenv:analysisResult) E1 E2 outVars fVars dVars vR elo ehi err P Gamma defVars,
    typeCheckCmd f defVars Gamma = true ->
    approxEnv E1 defVars absenv fVars dVars E2 ->
    ssa f (NatSet.union fVars dVars) outVars ->
    NatSet.Subset (NatSet.diff (Commands.freeVars f) dVars) fVars ->
    bstep (toREvalCmd (toRCmd f)) E1 (toRMap defVars) vR M0 ->
    validErrorboundCmd f Gamma absenv dVars = true ->
    validIntervalboundsCmd f absenv P dVars = true ->
    (forall v1, NatSet.mem v1 dVars = true ->
                   exists vR, E1 v1 = Some vR /\
                         (Q2R (fst (fst (absenv (Var Q v1)))) <= vR <= Q2R (snd (fst (absenv (Var Q v1)))))%R) ->
    (forall v, NatSet.mem v fVars= true ->
          exists vR, E1 v = Some vR /\
                (Q2R (fst (P v)) <= vR <= Q2R (snd (P v)))%R) ->
    (forall v1, (v1) mem (fVars ∪ dVars) = true ->
                        exists m0 : mType,
                          defVars v1 = Some m0) ->
    absenv (getRetExp f) = ((elo,ehi),err) ->
    (exists vF m,
        bstep (toRCmd f) E2 defVars vF m).
Proof.
  induction f;
    intros * type_f approxc1c2 ssa_f freeVars_subset eval_real valid_bounds
                    valid_intv fVars_sound P_valid types_defined absenv_res.
  - (* let-binding *)
    simpl in *; inversion eval_real; subst.
    inversion ssa_f; subst.
    repeat match goal with
           | [H: _ = true |- _] => andb_to_prop H
           end.
    destruct (absenv e) as [iv_e err_e] eqn:absenv_e.
    destruct iv_e as [ivlo_e ivhi_e] eqn:iv_eq.
    simpl in freeVars_subset.
    assert (NatSet.Subset (usedVars e -- dVars) fVars) as valid_varset.
    { set_tac.
      split; try auto.
      rewrite NatSet.remove_spec.
      split; try set_tac.
      hnf; intros; subst; set_tac. }
    match goal with
      | [H : validErrorbound _ _ _ _ = true |- _] =>
        eapply validErrorbound_sound
          with (err := err_e) (elo := ivlo_e) (ehi:= ivhi_e) in H; eauto;
          destruct H as [[vF [ mF eval_float_e]] bounded_e]
    end.
    repeat
      match goal with
      | [H : Qeq_bool _ _ = true |- _] => rewrite Qeq_bool_iff in H
      | [H : _ == _ |- _ ] => apply Qeq_eqR in H
      end.
    assert (approxEnv (updEnv n v E1) (updDefVars n mF defVars) absenv fVars
                      (NatSet.add n dVars) (updEnv n vF E2)).
    + eapply approxUpdBound; try auto.
      simpl in *.
      apply Rle_trans with (r2:= Q2R err_e); try lra.
      eapply bounded_e; eauto.
    + rename R into valid_rec.
      rewrite (typingSoundnessExp _ _  L0 eval_float_e) in *;
        simpl in *.
      destruct (Gamma (Var Q n)) eqn:?; try congruence.
      match goal with
      | [ H: _ && _ = true |- _] => andb_to_prop H
      end.
      type_conv.
      destruct (IHf absenv (updEnv n v E1) (updEnv n vF E2) outVars fVars
                    (NatSet.add n dVars) vR elo ehi err P Gamma
                    (updDefVars n m0 defVars))
        as [vF_res [m_res step_res]];
        eauto.
      { eapply ssa_equal_set; eauto.
          hnf. intros a; split; intros in_set.
          - rewrite NatSet.add_spec, NatSet.union_spec;
            rewrite NatSet.union_spec, NatSet.add_spec in in_set.
            destruct in_set as [P1 | [ P2 | P3]]; auto.
          - rewrite NatSet.add_spec, NatSet.union_spec in in_set;
            rewrite NatSet.union_spec, NatSet.add_spec.
            destruct in_set as [P1 | [ P2 | P3]]; auto. }
      { hnf. intros a in_diff.
        rewrite NatSet.diff_spec, NatSet.add_spec in in_diff.
        destruct in_diff as [ in_freeVars no_dVar].
        apply freeVars_subset.
        simpl.
        rewrite NatSet.diff_spec, NatSet.remove_spec, NatSet.union_spec.
        split; try auto. }
      { eapply swap_Gamma_bstep with (Gamma1 := updDefVars n M0 (toRMap defVars));
          eauto using Rmap_updVars_comm. }
      { intros v1 v1_mem.
        unfold updEnv.
        case_eq (v1 =? n); intros v1_eq.
        - apply Nat.eqb_eq in v1_eq; subst.
          exists v; split; try auto.
          rewrite <- R1, <- R0.
          destruct (validIntervalbounds_sound e absenv _ E1 defVars L (fVars:=fVars)) as [vR_e [eval_real_e bounded_real_e]]; eauto.
          pose proof (meps_0_deterministic _ eval_real_e H7); subst.
          rewrite absenv_e in *; simpl in *; auto.
        - rewrite NatSet.mem_spec in v1_mem.
          rewrite NatSet.add_spec in v1_mem.
          rewrite Nat.eqb_neq in v1_eq.
          destruct v1_mem.
          + exfalso; apply v1_eq; auto.
          + apply fVars_sound ; try auto.
              rewrite NatSet.mem_spec; auto. }
        { intros v1 mem_fVars.
          specialize (P_valid v1 mem_fVars).
          unfold updEnv.
          case_eq (v1 =? n); intros case_v1; try rewrite case_v1 in *; try auto.
          rewrite Nat.eqb_eq in case_v1; subst.
          assert (NatSet.In n (NatSet.union fVars dVars))
            as in_union
              by (rewrite NatSet.mem_spec in *; rewrite NatSet.union_spec; auto).
          rewrite <- NatSet.mem_spec in in_union.
          rewrite in_union in *; congruence. }
        { intros v1 v1_mem; set_tac.
          unfold updDefVars.
          case_eq (v1 =? n); intros case_v1; try rewrite case_v1 in *; try eauto.
          apply types_defined; set_tac.
          rewrite NatSet.union_spec in v1_mem; destruct v1_mem as [v_fVar | v_dVar]; try auto.
          rewrite NatSet.add_spec in v_dVar. destruct v_dVar; try auto.
          subst; rewrite Nat.eqb_refl in case_v1; congruence. }
        { exists vF_res; exists m_res; try auto.
          econstructor; eauto. }
  - inversion eval_real; subst.
    unfold updEnv; simpl.
    unfold validErrorboundCmd in valid_bounds.
    simpl in *.
    edestruct validErrorbound_sound as [[vF [mF eval_e]] bounded_e]; eauto.
    exists vF; exists mF; econstructor; eauto.
Qed.

Theorem validErrorboundCmd_sound (f:cmd Q):
  forall absenv E1 E2 outVars fVars dVars vR vF mF elo ehi err P Gamma defVars,
    typeCheckCmd f defVars Gamma = true ->
    approxEnv E1 defVars absenv fVars dVars E2 ->
    ssa f (NatSet.union fVars dVars) outVars ->
    NatSet.Subset (NatSet.diff (Commands.freeVars f) dVars) fVars ->
    bstep (toREvalCmd (toRCmd f)) E1 (toRMap defVars) vR M0 ->
    bstep (toRCmd f) E2 defVars vF mF ->
    validErrorboundCmd f Gamma absenv dVars = true ->
    validIntervalboundsCmd f absenv P dVars = true ->
    (forall v1, NatSet.mem v1 dVars = true ->
                   exists vR, E1 v1 = Some vR /\
                         (Q2R (fst (fst (absenv (Var Q v1)))) <= vR <= Q2R (snd (fst (absenv (Var Q v1)))))%R) ->
    (forall v, NatSet.mem v fVars= true ->
          exists vR, E1 v = Some vR /\
                (Q2R (fst (P v)) <= vR <= Q2R (snd (P v)))%R) ->
    (forall v1, (v1) mem (fVars ∪ dVars) = true ->
                        exists m0 : mType,
                          defVars v1 = Some m0) ->
    absenv (getRetExp f) = ((elo,ehi),err) ->
    (Rabs (vR - vF) <= (Q2R err))%R.
Proof.
  induction f;
    intros * type_f approxc1c2 ssa_f freeVars_subset eval_real eval_float
                    valid_bounds valid_intv fVars_sound P_valid types_defined absenv_res.
  - simpl in *;
      inversion eval_real;
      inversion eval_float;
      subst.
    inversion ssa_f; subst.
    repeat match goal with
           | [H: _ = true |- _] => andb_to_prop H
           end.
    destruct (absenv e) as [iv_e err_e] eqn:absenv_e.
    destruct iv_e as [ivlo_e ivhi_e] eqn:iv_eq.
    simpl in freeVars_subset.
    assert (NatSet.Subset (usedVars e -- dVars) fVars) as valid_varset.
    { set_tac.
      split; try auto.
      rewrite NatSet.remove_spec.
      split; try set_tac.
      hnf; intros; subst; set_tac. }
    match goal with
      | [H : validErrorbound _ _ _ _ = true |- _] =>
        eapply validErrorbound_sound
          with (err := err_e) (elo := ivlo_e) (ehi:= ivhi_e) in H; eauto;
          destruct H as [[vFe [mFe eval_float_e]] bounded_e]
    end.
    repeat
      match goal with
      | [H : Qeq_bool _ _ = true |- _] => rewrite Qeq_bool_iff in H
      | [H : _ == _ |- _ ] => apply Qeq_eqR in H
      end.
    rename R into valid_rec.
    rewrite (typingSoundnessExp _ _  L0 eval_float_e) in *;
      simpl in *.
    destruct (Gamma (Var Q n)); try congruence.
    match goal with
    | [ H: _ && _ = true |- _] => andb_to_prop H
    end.
    type_conv.
    apply (IHf absenv (updEnv n v E1) (updEnv n v0 E2) outVars fVars
               (NatSet.add n dVars) vR vF mF elo ehi err P Gamma
               (updDefVars n m0 defVars));
      eauto.
    + eapply approxUpdBound; try auto.
      simpl in *.
      apply Rle_trans with (r2:= Q2R err_e); try lra.
      eapply bounded_e; eauto.
    + eapply ssa_equal_set; eauto.
      hnf. intros a; split; intros in_set.
      * rewrite NatSet.add_spec, NatSet.union_spec;
          rewrite NatSet.union_spec, NatSet.add_spec in in_set.
        destruct in_set as [P1 | [ P2 | P3]]; auto.
      * rewrite NatSet.add_spec, NatSet.union_spec in in_set;
          rewrite NatSet.union_spec, NatSet.add_spec.
        destruct in_set as [P1 | [ P2 | P3]]; auto.
    + hnf. intros a in_diff.
      rewrite NatSet.diff_spec, NatSet.add_spec in in_diff.
      destruct in_diff as [ in_freeVars no_dVar].
      apply freeVars_subset.
      simpl.
      rewrite NatSet.diff_spec, NatSet.remove_spec, NatSet.union_spec.
      split; try auto.
    + eapply swap_Gamma_bstep with (Gamma1 := updDefVars n M0 (toRMap defVars));
        eauto using Rmap_updVars_comm.
    + intros v1 v1_mem.
      unfold updEnv.
      case_eq (v1 =? n); intros v1_eq.
      * apply Nat.eqb_eq in v1_eq; subst.
        exists v; split; try auto.
        rewrite <- R1, <- R0.
        destruct (validIntervalbounds_sound e absenv _ E1 defVars L (fVars:=fVars)) as [vR_e [eval_real_e bounded_real_e]]; eauto.
        pose proof (meps_0_deterministic _ eval_real_e H7); subst.
        rewrite absenv_e in *; simpl in *; auto.
      * rewrite NatSet.mem_spec in v1_mem.
        rewrite NatSet.add_spec in v1_mem.
        rewrite Nat.eqb_neq in v1_eq.
        destruct v1_mem.
        { exfalso; apply v1_eq; auto. }
        { apply fVars_sound ; try auto.
          rewrite NatSet.mem_spec; auto. }
    + intros v1 mem_fVars.
      specialize (P_valid v1 mem_fVars).
      unfold updEnv.
      case_eq (v1 =? n); intros case_v1; try rewrite case_v1 in *; try auto.
      rewrite Nat.eqb_eq in case_v1; subst.
      assert (NatSet.In n (NatSet.union fVars dVars))
        as in_union
          by (rewrite NatSet.mem_spec in *; rewrite NatSet.union_spec; auto).
      rewrite <- NatSet.mem_spec in in_union.
      rewrite in_union in *; congruence.
    + intros v1 v1_mem; set_tac.
      unfold updDefVars.
      case_eq (v1 =? n); intros case_v1; try rewrite case_v1 in *; try eauto.
      apply types_defined; set_tac.
      rewrite NatSet.union_spec in v1_mem; destruct v1_mem as [v_fVar | v_dVar]; try auto.
      rewrite NatSet.add_spec in v_dVar. destruct v_dVar; try auto.
      subst; rewrite Nat.eqb_refl in case_v1; congruence.
  - inversion eval_real; subst.
    inversion eval_float; subst.
    unfold updEnv; simpl.
    unfold validErrorboundCmd in valid_bounds.
    simpl in *.
    edestruct validErrorbound_sound as [[* [* eval_e]] bounded_e]; eauto.
Qed.