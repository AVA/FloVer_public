Require Import FloVer.CertificateChecker FloVer.floverParser.
Require Import Coq.extraction.ExtrOcamlString Coq.extraction.ExtrOcamlBasic Coq.extraction.ExtrOcamlNatBigInt Coq.extraction.ExtrOcamlZBigInt.

Extraction Language Ocaml.

Extraction "./binary/CoqChecker.ml" runChecker.