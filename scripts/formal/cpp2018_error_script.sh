#!/bin/bash --posix
#
# This script

# 'Standard' benchmark set
declare -a files=("BallBeam" "BatchProcessor" "BatchReactor" "Bicycle" "Bsplines" \
  "DCMotor" "Doppler" "Floudas" "HimmilbeauLet" "InvertedPendulum" "KeplerLet" \
  "RigidBody" "Science" "Traincar1" "Traincar2" "Traincar3" "Traincar4" \
  "TurbineLet")

# Make sure the code is compiled
sbt compile

# generate daisy script
if [ ! -e daisy ]
then
  sbt script
fi



# Run daisy on each testfile
for file in "${files[@]}"
do
  ./daisy --errorMethod=interval --mixed-precision="testcases/mixed-precision-maps/${file}.txt" \
   "testcases/cpp2018/${file}.scala"
done