#!/bin/bash

####################################################################################
#                                                                                  #
# Experiments for ITP/CPP 2017 Paper. Run daisy on all testcases in the "formal"   #
# directory. Then use the provers to get the runtimes and check the certificates   #
#                                                                                  #
####################################################################################

arr=()
while IFS=  read -r -d $'\0'; do
    arr+=("$REPLY")
done < <(find ./testcases/itp2017/ -name "*.scala" -print0)

for file in "${arr[@]}"
do
    echo $file
    echo "Coq certificate"
    /usr/bin/time -o $1 -a -f "%C %e" ./daisy $file --certificate=coq >/dev/null
    echo "HOL4 Certificate"
    /usr/bin/time -o $1 -a -f "%C %e" ./daisy $file --certificate=hol4 >/dev/null
    echo "HOL4 Binary Certificate"
    /usr/bin/time -o $1 -a -f "%C %e" ./daisy $file --certificate=binary >/dev/null
    echo ""
  # echo $file
  # echo "Coq certificate"
  # /usr/bin/time -o $1 -a -f "%C %E" ./daisy $file --certificate=coq --randomMP >/dev/null
  # echo "HOL Certificate"
  # /usr/bin/time -o $1 -a -f "%C %E" ./daisy $file --certificate=hol4 --randomMP >/dev/null
  # echo ""
done

 echo ""
 echo "[Certificate Test - Coq]"
 echo ""

cd coq
  arrCoq=()
  while IFS=  read -r -d $'\0'; do
     arrCoq+=("$REPLY")
  done < <(find ./output/ -name "*.v" -print0)

 for file in "${arrCoq[@]}"
 do
   echo $file
   echo ""
   /usr/bin/time -o $1 -a -f "%C %e" coqc -R ./ Daisy $file
 done

cd ./hol4/output

arrHOL=()
while IFS=  read -r -d $'\0'; do
    arrHOL+=("$REPLY")
done < <(find ./ -name "*Script.sml" -print0)

for file in "${arrHOL[@]}"
do
  echo $file
  echo ""
  /usr/bin/time -o $1 -a -f "%C %E" Holmake ${file/Script.sml/Theory.sig}
done

echo ""
echo "[Certificate Test - HOL4]"
echo ""

arrHOL=()

while IFS=  read -r -d $'\0'; do
    arrHOL+=("$REPLY")
done < <(find ./output/ -name "*.sml" -print0)

for file in "${arrHOL[@]}"
do
  echo $file
  echo ""
  /usr/bin/time -o $1 -a -f "%C %e" $HOLDIR/bin/Holmake ${file/Script.sml/Theory.sig}
done

 cd ../../output/

 echo ""
 echo "[Certificate Test - HOL4  Binary]"
 echo ""

 arrBinary=()
 while IFS=  read -r -d $'\0'; do
     arrBinary+=("$REPLY")
 done < <(find ./ -name "*.txt" -print0)

 for file in "${arrBinary[@]}"
 do
     echo $file
     echo ""
     /usr/bin/time -f "%C %e" sh -c "../hol4/binary/cake_checker <$file"
     echo $?
 done

 echo ""
 echo "[Certificate Test - Coq  Binary - native compiled]"
 echo ""

 for file in "${arrBinary[@]}"
 do
     echo $file
     echo "Native"
     /usr/bin/time -a -o $1 -f "%C %e" sh -c "../coq/binary/coq_checker_native $file"
     echo $?
 done


 echo ""
 echo "[Certificate Test - Coq  Binary - byte compiled]"
 echo ""

 for file in "${arrBinary[@]}"
 do
     echo $file
     /usr/bin/time -a -o $1 -f "%C %e" sh -c "../coq/binary/coq_checker_bytes $file"
     echo $?
 done
