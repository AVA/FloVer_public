#!/usr/bin/env python3

import sys

def parseTime (timeStr):
    timeL = timeStr.split(":")
    time_sec_micro = timeL [len(timeL)-1].split(".")
    timeL.pop(len(timeL)-1)
    #We have already taken away the seoncds and microseconds value --> start with minutes
    multiplier = 60
    res = 0
    for val in reversed(timeL):
        res = res + (int (val) * multiplier)
        multiplier = multiplier * 60

    #Add seconds
    res += int(time_sec_micro[0])

    #Round to next second
    if  (len (time_sec_micro) > 1):
        if (int (time_sec_micro[1]) > 0):
                res += 1

    return res

def convert (fname):
    fileo = open (fname, "r")
    fname_no_suffix = fname.split(".")[0]
    out = open (fname_no_suffix+"converted.txt", "w")
    for line in fileo:
        spl_line = line.split (" ")
        timeInSeconds = parseTime (spl_line[len(spl_line)-1])
        #remove already parsed time from list
        spl_line.pop(len(spl_line)-1)
        curr_line = ""
        for val in spl_line:
            if "certificate" in val:
                val = val.replace(".v","").replace(".sig","")
                lineContent = val.split("certificate_")
                curr_line += str(lineContent[len(lineContent) - 1])

        curr_line += ", " + str(timeInSeconds) + "\n"
        out.write (curr_line)

    fileo.close()
    out.close()

def main():
    #get file name from argument list
    fname = sys.argv[1]
    convert(fname)


if __name__ == "__main__" :
    main()
