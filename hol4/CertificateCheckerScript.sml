(**
   This file contains the HOL4 implementation of the certificate checker as well as its soundness proof.
   The checker is a composition of the range analysis validator and the error bound validator.
   Running this function on the encoded analysis result gives the desired theorem
   as shown in the soundness theorem.
**)
open preamble

open simpLib realTheory realLib RealArith RealSimpsTheory
open AbbrevsTheory ExpressionsTheory FloverTactics ExpressionAbbrevsTheory
     ErrorBoundsTheory IntervalArithTheory IntervalValidationTheory
     ErrorValidationTheory ssaPrgsTheory FPRangeValidatorTheory

val _ = new_theory "CertificateChecker";
val _ = temp_overload_on("abs",``real$abs``);

(** Certificate checking function **)
val CertificateChecker_def = Define
  `CertificateChecker (e:real exp) (absenv:analysisResult) (P:precond) (defVars: num -> mType option)=
   if (typeCheck e defVars (typeMap defVars e)) then
       if (validIntervalbounds e absenv P LN /\ FPRangeValidator e absenv (typeMap defVars e) LN) then
           (validErrorbound e (typeMap defVars e) absenv LN)
       else F
   else F`;

val CertificateCheckerCmd_def = Define
  `CertificateCheckerCmd (f:real cmd) (absenv:analysisResult) (P:precond) defVars =
        if (typeCheckCmd f defVars (typeMapCmd defVars f) /\ validSSA f (freeVars f)) then
            if ((validIntervalboundsCmd f absenv P LN) /\ FPRangeValidatorCmd f absenv (typeMapCmd defVars f) LN) then
                (validErrorboundCmd f (typeMapCmd defVars f) absenv LN)
            else F
        else F`;

(**
   Soundness proof for the certificate checker.
   Apart from assuming two executions, one in R and one on floats, we assume that
   the real valued execution respects the precondition.
**)
val Certificate_checking_is_sound = store_thm ("Certificate_checking_is_sound",
  ``!(e:real exp) (absenv:analysisResult) (P:precond) (E1 E2:env) defVars fVars.
      approxEnv E1 defVars absenv fVars LN E2 /\
      (!v.
          v IN (domain fVars) ==>
          ?vR.
            (E1 v = SOME vR) /\
            FST (P v) <= vR /\ vR <= SND (P v)) /\
      (domain (usedVars e)) SUBSET (domain fVars) /\
      (!v. v IN domain fVars ==> ?m. defVars v = SOME m) /\
      CertificateChecker e absenv P defVars ==>
      ?vR vF m.
        eval_exp E1 (toRMap defVars) (toREval e) vR M0 /\
        eval_exp E2 defVars e vF m /\
        (!vF m.
           eval_exp E2 defVars e vF m ==>
           abs (vR - vF) <= (SND (absenv e)) /\ validFloatValue vF m)``,
(**
   The proofs is a simple composition of the soundness proofs for the range
   validator and the error bound validator.
**)
  simp [CertificateChecker_def]
  \\ rpt strip_tac
  \\ Cases_on `absenv e`
  \\ rename1 `absenv e = (iv, err)`
  \\ Cases_on `iv`
  \\ rename1 `absenv e = ((elo, ehi), err)`
  \\ qspecl_then
       [`e`, `absenv` , `P` , `fVars`, `LN`, `E1`, `defVars`]
       destruct validIntervalbounds_sound
  \\ fs[]
  \\ qspecl_then
       [`e`, `E1`, `E2`, `absenv`, `vR`, `err`, `P`, `elo`, `ehi`, `fVars`,
        `LN`, `typeMap defVars e`, `defVars`]
       destruct validErrorbound_sound
  \\ fs[]
  \\ qexistsl_tac [`vR`, `nF`, `m`] \\ fs[]
  \\ rpt strip_tac
  >- (first_x_assum irule \\ fs[]
      \\ asm_exists_tac \\ fs[])
  \\ irule FPRangeValidator_sound
  \\ qexistsl_tac [`absenv`, `E1`, `E2`, `defVars`, `P`, `LN`,`e`, `fVars`, `typeMap defVars e`]
  \\ fs[]);

val CertificateCmd_checking_is_sound = store_thm ("CertificateCmd_checking_is_sound",
  ``!(f:real cmd) (absenv:analysisResult) (P:precond) defVars
     (E1 E2:env) (fVars:num_set).
      approxEnv E1 defVars absenv (freeVars f) LN E2 /\
      (!v.
          v IN (domain (freeVars f)) ==>
          ?vR.
            (E1 v = SOME vR) /\
            FST (P v) <= vR /\ vR <= SND (P v)) /\
      domain (freeVars f) SUBSET (domain fVars) /\
      (!v. v IN domain (freeVars f) ==> ?m. defVars v = SOME m) /\
      CertificateCheckerCmd f absenv P defVars ==>
      ?vR vF m.
        bstep (toREvalCmd f) E1 (toRMap defVars) vR M0 /\
        bstep f E2 defVars vF m /\
        (!vF m. bstep f E2 defVars vF m ==> abs (vR - vF) <= (SND (absenv (getRetExp f))))``,
  simp [CertificateCheckerCmd_def]
  \\ rpt strip_tac
  \\ `?outVars. ssa f (freeVars f) outVars` by (match_mp_tac validSSA_sound \\ fs[])
  \\ Cases_on `absenv (getRetExp f)`
  \\ rename1 `absenv (getRetExp f) = (iv, err)`
  \\ Cases_on `iv`
  \\ rename1 `absenv (getRetExp f) = ((elo, ehi), err)`
  \\ qspecl_then
       [`f`, `absenv`, `E1`, `freeVars f`, `LN`, `outVars`, `elo`, `ehi`, `err`, `P`, `defVars`]
       destruct validIntervalboundsCmd_sound
  \\ fs[]
  \\ qspecl_then
       [`f`, `absenv`, `E1`, `E2`, `outVars`, `freeVars f`, `LN`, `vR`, `elo`, `ehi`, `err`, `P`, `m`, `typeMapCmd defVars f`, `defVars`]
       destruct validErrorboundCmd_gives_eval
  \\ fs[]
  \\ qexistsl_tac [`vR`, `vF`, `m`] \\ fs[]
  \\ rpt strip_tac
  \\ irule validErrorboundCmd_sound
  \\ rename1 `bstep f E2 defVars vF2 m2`
  \\ qexistsl_tac [`E1`, `E2`, `defVars`, `P`, `absenv`, `LN`, `ehi`, `elo`,`typeMapCmd defVars f`, `f`, `freeVars f`, `m2`, `outVars`]
  \\ fs[]);

val _ = export_theory();
