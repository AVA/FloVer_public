open preamble
open simpLib realTheory realLib RealArith sptreeTheory
open AbbrevsTheory ExpressionAbbrevsTheory RealSimpsTheory CommandsTheory FloverTactics

val _ = new_theory "Environments";

val _ = temp_overload_on("abs",``real$abs``);

val (approxEnv_rules, approxEnv_ind, approxEnv_cases) = Hol_reln `
  (!(defVars: num -> mType option) (A:analysisResult).
      approxEnv emptyEnv defVars A LN LN emptyEnv) /\
  (!(E1:env) (E2:env) (defVars: num -> mType option) (A:analysisResult) (fVars:num_set) (dVars:num_set) v1 v2 x.
      approxEnv E1 defVars A fVars dVars E2 /\
      (defVars x = SOME m) /\
      (abs (v1 - v2) <= abs v1 * (mTypeToQ m)) /\
      (lookup x (union fVars dVars) = NONE) ==>
      approxEnv (updEnv x v1 E1) (updDefVars x m defVars) A (insert x () fVars) dVars (updEnv x v2 E2)) /\
  (!(E1:env) (E2:env) (defVars: num -> mType option) (A:analysisResult) (fVars:num_set) (dVars:num_set) v1 v2 x.
      approxEnv E1 defVars A fVars dVars E2 /\
      (abs (v1 - v2) <= SND (A (Var x))) /\
      (lookup x (union fVars dVars) = NONE) ==>
      approxEnv (updEnv x v1 E1) (updDefVars x m defVars) A fVars (insert x () dVars) (updEnv x v2 E2))`;

val [approxRefl, approxUpdFree, approxUpdBound] = CONJ_LIST 3 approxEnv_rules;
save_thm ("approxRefl", approxRefl);
save_thm ("approxUpdFree", approxUpdFree);
save_thm ("approxUpdBound", approxUpdBound);

val approxEnv_gives_value = store_thm (
  "approxEnv_gives_value",
  ``!E1 E2 x v (fVars:num_set) (dVars:num_set) absenv Gamma.
      approxEnv E1 Gamma absenv fVars dVars E2 /\
      E1 x = SOME v /\
      x IN ((domain fVars) UNION (domain dVars)) ==>
      ?v2.
        E2 x = SOME v2``,
  qspec_then
    `\E1 Gamma absenv fVars dVars E2.
       !x v.
       E1 x = SOME v /\
       x IN ((domain fVars) UNION (domain dVars)) ==>
       ?v2. E2 x = SOME v2` (fn thm => assume_tac (SIMP_RULE std_ss [] thm)) approxEnv_ind
    \\ rpt strip_tac \\ first_x_assum irule
    \\ rpt strip_tac
    >- (fs [domain_union, updEnv_def, lookup_union] \\ rveq
        \\ EVERY_CASE_TAC \\ fs[])
    >- (fs [domain_union, updEnv_def, lookup_union] \\ rveq
        \\ EVERY_CASE_TAC \\ fs[])
    >- (qexistsl_tac [`E1`, `Gamma`, `absenv`, `fVars`, `dVars`] \\ fs []));

val approxEnv_fVar_bounded = store_thm (
  "approxEnv_fVar_bounded",
  ``!E1 Gamma absenv fVars dVars E2 x v v2 m.
      approxEnv E1 Gamma absenv fVars dVars E2 /\
      E1 x = SOME v /\
      E2 x = SOME v2 /\
      x IN (domain fVars) /\
      Gamma x = SOME m ==>
      abs (v - v2) <= (abs v) * (mTypeToQ m)``,
  rpt strip_tac
  \\ qspec_then
      `\E1 Gamma absenv fVars dVars E2.
         !x v v2 m.
         E1 x = SOME v /\
         E2 x = SOME v2 /\
         x IN (domain fVars) /\
         Gamma x = SOME m ==>
         abs (v - v2) <= (abs v) * (mTypeToQ m)`
      (fn thm => irule (SIMP_RULE std_ss [] thm))
      approxEnv_ind
  \\ rpt strip_tac
  >- (fs [emptyEnv_def])
  >- (fs [updEnv_def]
      \\ EVERY_CASE_TAC \\ rveq \\ fs[lookup_union, domain_lookup]
      \\ first_x_assum irule \\ fs[]
      \\ rename1 `updDefVars x1 m1 defVars x2 = SOME m2`
      \\ qexists_tac `x2` \\ fs[updDefVars_def])
  >- (fs [updEnv_def, updDefVars_def]
      \\ rveq \\ fs[]
      \\ EVERY_CASE_TAC
      \\ rveq \\ fs[]
      \\ first_x_assum irule \\ fs[]
      \\ rename1 `defVars x1 = SOME m1` \\ qexists_tac `x1`
      \\ fs[])
  >- (qexistsl_tac [`E1`, `Gamma`, `absenv`, `fVars`, `dVars`, `E2`, `x`]
      \\ fs[]));

val approxEnv_dVar_bounded = store_thm ("approxEnv_dVar_bounded",
  ``!E1 Gamma absenv fVars dVars E2 x v v2 m e.
      approxEnv E1 Gamma absenv fVars dVars E2 /\
      E1 x = SOME v /\
      E2 x = SOME v2 /\
      x IN (domain dVars) /\
      Gamma x = SOME m /\
      SND (absenv (Var x)) = e ==>
      abs (v - v2) <= e``,
  rpt strip_tac
  \\ qspec_then
      `\E1 Gamma absenv fVars dVars E2.
         !x v v2 m.
         E1 x = SOME v /\
         E2 x = SOME v2 /\
         x IN (domain dVars) /\
         Gamma x = SOME m /\
         SND (absenv (Var x)) = e ==>
         abs (v - v2) <= e`
      (fn thm => irule (SIMP_RULE std_ss [] thm))
      approxEnv_ind
  \\ rpt strip_tac
  >- (fs [emptyEnv_def])
  >- (fs [updEnv_def]
      \\ EVERY_CASE_TAC \\ rveq \\ fs[lookup_union, domain_lookup]
      >- (EVERY_CASE_TAC \\ fs[])
      >- (first_x_assum irule \\ fs[updDefVars_def]
          \\ rename1 `defVars x2 = SOME m2` \\ qexistsl_tac [`m2`, `x2`]
          \\ fs[]))
  >- (fs [updEnv_def, updDefVars_def] \\ rveq \\ fs[]
      \\ EVERY_CASE_TAC
      \\ rveq \\ fs[]
      \\ first_x_assum irule \\ fs[]
      \\ rename1 `defVars x1 = SOME m1` \\ qexistsl_tac [`m1`,`x1`]
      \\ fs[])
  >- (qexistsl_tac [`E1`, `Gamma`, `absenv`, `fVars`, `dVars`, `E2`, `m`, `x`]
      \\ fs[]));

val _ = export_theory ();;
