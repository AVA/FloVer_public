(**
  f machine-precision as a datatype for mixed-precision computations

  @author: Raphael Monat
  @maintainer: Heiko Becker
 **)
open preamble miscTheory
open realTheory realLib sptreeTheory

val _ = new_theory "MachineType";

val _ = temp_overload_on("abs",``real$abs``);

val _ = Datatype `
  mType = M0 | M16 |  M32 | M64(* | M128 | M256 *)`;

val mTypeToQ_def = Define `
  mTypeToQ (m:mType) : real =
    case m of
      | M0 => 0
      | M16 => 1 / (2 pow 11)
      | M32 => 1 / (2 pow 24)
      | M64 => 1 / (2 pow 53)
      (* the epsilons below match what is used internally in flover,
         although these value do not match the IEEE standard *)
      (* | M128 => 1 / (2 pow 105) *)
      (* | M256 => 1 / (2 pow 211) *)`;

val meps_def = Define `meps = mTypeToQ`;

val mTypeToQ_pos = store_thm("mTypeToQ_pos",
  ``!e. 0 <= mTypeToQ e``,
  Cases_on `e` \\ EVAL_TAC);

(**
  Check if machine precision m1 is more precise than machine precision m2.
  M0 is real-valued evaluation, hence the most precise.
  All others are compared by
    mTypeToQ m1 <= mTypeToQ m2
 **)
val isMorePrecise_def = Define `
  isMorePrecise (m1:mType) (m2:mType) = (mTypeToQ (m1) <= mTypeToQ (m2))`;

val M0_least_precision = store_thm ("M0_least_precision",
  ``!(m:mType).
      isMorePrecise m M0 ==>
      m = M0``,
  fs [isMorePrecise_def, mTypeToQ_def] \\
  rpt strip_tac \\
  Cases_on `m` \\
  fs []);

val M0_lower_bound  = store_thm ("M0_lower_bound",
  ``! (m:mType).
      isMorePrecise M0 m``,
  Cases_on `m` \\ EVAL_TAC);

(**
  Function computing the join of two precisions, this is the most precise type,
  in which evaluation has to be performed, e.g. addition of 32 and 64 bit floats
  has to happen in 64 bits
**)
val join_def = Define `
  join (m1:mType) (m2:mType) =
    if (isMorePrecise m1 m2) then m1 else m2`;

(* val M0_join_is_M0 = store_thm ("M0_join_is_M0", *)
(*   ``!m1 m2. *)
(*       join m1 m2 = M0 ==> *)
(*       (m1 = M0 /\ m2 = M0)``, *)
(*   fs [join_def, isMorePrecise_def] *)
(*   \\ rpt strip_tac *)
(*   \\ Cases_on `m1 = M0` \\ Cases_on `m2 = M0` \\ fs[] *)
(*   >- (m1 = M0 by (Cases_on `mTypeToQ m1 <= mTypeToQ M0` \\ fs[] *)
(*   \\ fs [ONCE_REWRITE_RULE [isMorePrecise_def] M0_least_precision] *)
(*   \\ Cases_on `m1` \\ fs[mTypeToQ_def] *)
(*   \\ Cases_on `m2` \\ fs[mTypeToQ_def] *)
(*   qpat_x_assum `_ = M0` *)
(*     (fn thm => fs [thm]) *)
(*   >- (Cases_on `m1` \\ fs [mTypeToQ_def]) *)
(*   >- (Cases_on `m2` \\ fs [mTypeToQ_def])); *)

val maxExponent_def = Define `
  (maxExponent (M0) = 0n) /\
  (maxExponent (M16) = 15) /\
  (maxExponent (M32) = 127) /\
  (maxExponent (M64) = 1023)
     (* | M128 => 1023 (** FIXME **) *)
     (* | M256 => 1023 *)`;

val minExponentPos_def = Define `
  (minExponentPos (M0) = 0n) /\
  (minExponentPos (M16) = 14) /\
  (minExponentPos (M32) = 126) /\
  (minExponentPos (M64) = 1022) (*/\ *)
  (* (minExponentPos (M128) = 1022) /\ (* FIXME *) *)
  (* (minExponentPos (M256) = 1022) *)`;

(**
Goldberg - Handbook of Floating-Point Arithmetic: (p.183)
(𝛃 - 𝛃^(1 - p)) * 𝛃^(e_max)

which simplifies to 2^(e_max) for base 2
**)

val maxValue_def = Define `
  maxValue (m:mType) = (&(2n ** (maxExponent m))):real`;

(** Using the sign bit, the very same number is representable as a negative number,
  thus just apply negation here **)
val minValue_def = Define `
 minValue (m:mType) = inv (&(2n ** (minExponentPos m)))`;


(** Goldberg - Handbook of Floating-Point Arithmetic: (p.183)
  𝛃^(e_min -p + 1) = 𝛃^(e_min -1) for base 2
**)

val minDenormalValue_def = Define `
  minDenormalValue (m:mType) = 1 / (2 pow (minExponentPos m - 1))`;

val normal_def = Define `
  normal (v:real) (m:mType) =
    (minValue m <= abs v /\ abs v <= maxValue m)`;

val denormal_def = Define `
  denormal (v:real) (m:mType) =
    case m of
      | M0 => F
      | _ => ((abs v) < (minValue m) /\ v <> 0)`;

(**
  Predicate that is true if and only if the given value v is a valid
  floating-point value according to the the type m.
  Since we use the 1 + 𝝳 abstraction, the value must either be
  in normal range or 0.
**)
val validFloatValue_def = Define `
  validFloatValue (v:real) (m:mType) =
   case m of
    | M0 => T
    | _ => normal v m \/ denormal v m \/ v = 0`

val validValue_def = Define `
  validValue (v:real) (m:mType) =
    case m of
      | M0 => T
      | _ => abs v <= maxValue m`;

val _ = export_theory();
