(*
  Some tactics which ease proving goals
*)

structure FloverTactics =
struct

local open intLib wordsLib in end;
open set_relationTheory;
open BasicProvers Defn HolKernel Parse Tactic monadsyntax
     alistTheory arithmeticTheory bagTheory boolLib boolSimps bossLib
     combinTheory dep_rewrite finite_mapTheory indexedListsTheory lcsymtacs
     listTheory llistTheory markerLib miscTheory
     optionTheory pairLib pairTheory pred_setTheory
     quantHeuristicsLib relationTheory res_quanTheory rich_listTheory
     sortingTheory sptreeTheory stringTheory sumTheory wordsTheory;

fun elim_conj thm =
  let val (hypl, concl) = dest_thm thm in
      if is_conj concl
      then
          let val (thm1, thm2) = CONJ_PAIR thm in
              elim_conj thm1 \\ elim_conj thm2
          end
      else
          ASSUME_TAC thm
  end;

fun elim_exist1 thm =
  let val (hypl, concl) = dest_thm thm in
      if is_exists concl
      then
              CHOOSE_THEN elim_exist thm
      else
          elim_conj thm
  end
and elim_exist thm =
  let val (hypl, concl) = dest_thm thm in
      if is_exists concl
      then
              CHOOSE_THEN elim_exist1 thm
      else
          elim_conj thm
  end;

fun inversion pattern cases_thm =
  qpat_x_assum pattern
    (fn thm => elim_exist (ONCE_REWRITE_RULE [cases_thm] thm));

fun qexistsl_tac termlist =
  case termlist of
      [] => ALL_TAC
    | t::tel => qexists_tac t \\ qexistsl_tac tel;

fun specialize pat_hyp pat_thm =
  qpat_x_assum pat_hyp
    (fn hyp =>
      (qspec_then pat_thm ASSUME_TAC hyp) ORELSE
      (qpat_assum pat_thm
         (fn asm => ASSUME_TAC (MP hyp asm))));

fun rw_asm pat_asm =
  qpat_x_assum pat_asm
    (fn asm =>
      (once_rewrite_tac [asm] \\ ASSUME_TAC asm));

fun rw_asm_star pat_asm =
  qpat_x_assum pat_asm
    (fn asm =>
        fs [Once asm] \\ ASSUME_TAC asm);

fun rw_sym_asm pat_asm =
  qpat_x_assum pat_asm
    (fn asm =>
      (once_rewrite_tac [GSYM asm] \\ ASSUME_TAC asm));

fun rw_thm_asm pat_asm thm =
  qpat_x_assum pat_asm
    (fn asm =>
      (ASSUME_TAC (ONCE_REWRITE_RULE[thm] asm)));


(* Destruct like tactic, by Michael Norrish, see HOL-info **)

(* Given theorem

  P x y z ==> ?w. Q w

  introduce P x y z as a subgoal and Q w for some w as hypothesis *)

fun destruct th =
  let
      val hyp_to_prove = lhand (concl th)
  in
      SUBGOAL_THEN hyp_to_prove (fn thm => STRIP_ASSUME_TAC (MP th thm))
  end

fun impl_subgoal_tac th =
  let
      val hyp_to_prove = lhand (concl th)
  in
      SUBGOAL_THEN hyp_to_prove (fn thm => assume_tac (MP th thm))
  end

fun flover_eval_tac t :tactic=
  let
    val result_thm = computeLib.EVAL_CONV t
  in
    rewrite_tac [result_thm]
    \\ fs[sptreeTheory.lookup_def]
    \\ rpt strip_tac
    \\ fs[sptreeTheory.lookup_def]
end;

end
