(**
 Formalization of the Abstract Syntax Tree of a subset used in the Flover framework
 **)
open preamble
open simpLib realTheory realLib RealArith
open AbbrevsTheory ExpressionsTheory ExpressionAbbrevsTheory MachineTypeTheory

val _ = new_theory "Commands";

(**
  Next define what a program is.
  Currently no loops, or conditionals.
  Only assignments and return statement
**)
val _ = Datatype `
  cmd = Let mType num ('v exp) cmd
       | Ret ('v exp)`;


val toREvalCmd_def = Define `
  toREvalCmd (f:real cmd) : real cmd =
	case f of
	  | Let m x e g => Let M0 x (toREval e) (toREvalCmd g)
	  | Ret e => Ret (toREval e)`;

(**
  Define big step semantics for the Flover language, terminating on a "returned"
  result value
 **)
val (bstep_rules, bstep_ind, bstep_cases) = Hol_reln `
  (!m m' x e s E v res Gamma.
      eval_exp E Gamma e v m /\
      bstep s (updEnv x v E) (updDefVars x m Gamma) res m' ==>
      bstep (Let m x e s) E Gamma res m') /\
  (!m e E v Gamma.
      eval_exp E Gamma e v m ==>
      bstep (Ret e) E Gamma v m)`;

(**
  Generate a better case lemma again
**)
val bstep_cases =
  map (GEN_ALL o SIMP_CONV (srw_ss()) [Once bstep_cases])
    [``bstep (Let m x e s) E defVars vR m'``,
     ``bstep (Ret e) E defVars vR m``]
  |> LIST_CONJ |> curry save_thm "bstep_cases";

val [let_b, ret_b] = CONJ_LIST 2 bstep_rules;
save_thm ("let_b", let_b);
save_thm ("ret_b", ret_b);

(**
  The free variables of a command are all used variables of expressions
  without the let bound variables
**)
val freeVars_def = Define `
  freeVars (f: 'a cmd) :num_set =
    case f of
      |Let m x e g => delete x (union (usedVars e) (freeVars g))
      |Ret e => usedVars e`;

(**
  The defined variables of a command are all let bound variables
**)
val definedVars_def = Define `
  definedVars (f:'a cmd) :num_set =
    case f of
      |Let m (x:num) e g => insert x () (definedVars g)
      |Ret e => LN`;

val bstep_eq_env = store_thm (
  "bstep_eq_env",
  ``!f E1 E2 Gamma v m.
      (!x. E1 x = E2 x) /\
      bstep f E1 Gamma v m ==>
      bstep f E2 Gamma v m``,
  Induct \\ rpt strip_tac \\ fs[bstep_cases]
  >- (qexists_tac `v'` \\ conj_tac
      \\ TRY (drule eval_eq_env \\ disch_then drule \\ fs[] \\ FAIL_TAC"")
      \\ first_x_assum irule \\ qexists_tac `updEnv n v' E1` \\ fs[]
      \\ rpt strip_tac \\ fs[updEnv_def])
  \\ irule eval_eq_env \\ asm_exists_tac \\ fs[]);

val _ = export_theory ();
