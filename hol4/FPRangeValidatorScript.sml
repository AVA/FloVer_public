(**
  Floating-Point range validator

  The Floating-Point range validator computes an overapproximation of what the
  value of an operation may be using its real-valued range +- the error bound.

  This soundly proves that the runtime value of the expression must be a valid
  value according to IEEE 754.

**)
open preamble

open machine_ieeeTheory binary_ieeeTheory lift_ieeeTheory realTheory

open AbbrevsTheory MachineTypeTheory TypingTheory RealSimpsTheory IntervalArithTheory
     ExpressionsTheory FloverTactics IntervalValidationTheory ErrorValidationTheory
     CommandsTheory EnvironmentsTheory ssaPrgsTheory

val _ = new_theory "FPRangeValidator";

val _ = temp_overload_on("abs",``real$abs``);

val FPRangeValidator_def = Define `
  FPRangeValidator (e:real exp) A typeMap dVars =
    case typeMap e of
      | SOME m =>
        let (iv_e, err_e) = A e in
        let iv_e_float = widenInterval iv_e err_e in
        let recRes =
            case e of
              | Binop b e1 e2 =>
                FPRangeValidator e1 A typeMap dVars /\
                FPRangeValidator e2 A typeMap dVars
              | Unop u e =>
                FPRangeValidator e A typeMap dVars
              | Downcast m e => FPRangeValidator e A typeMap dVars
              | _ =>  T
        in
        let normal_or_zero =
              ((normal (IVlo iv_e_float) m \/ (IVlo iv_e_float) = 0) /\
              (normal (IVhi iv_e_float) m \/ (IVhi iv_e_float) = 0))
        in
            (case e of
              | Var v =>
                  if (lookup v dVars = SOME ())
                  then T
                  else
                      if (validValue (IVhi iv_e_float) m /\
                          validValue (IVlo iv_e_float) m)
                      then normal_or_zero /\ recRes
                      else
                          F
              | _ => if (validValue (IVhi iv_e_float) m /\
                          validValue (IVlo iv_e_float) m)
                      then normal_or_zero /\ recRes
                      else
                          F)
      | NONE => F`;

val normalOrZero_def = Define `
  normalOrZero iv_e_float m =
    ((normal (IVlo iv_e_float) m \/ (IVlo iv_e_float) = 0) /\
     (normal (IVhi iv_e_float) m \/ (IVhi iv_e_float) = 0))`;

val FPRangeValidatorCmd_def = Define `
  (FPRangeValidatorCmd ((Let m x e g):real cmd) A typeMap dVars =
     if FPRangeValidator e A typeMap dVars
     then FPRangeValidatorCmd g A typeMap (insert x () dVars)
     else F) /\
  (FPRangeValidatorCmd (Ret e) A typeMap dVars =
   FPRangeValidator e A typeMap dVars)`;

val enclosure_to_abs = store_thm (
  "enclosure_to_abs",
  ``!a b c.
     a <= b /\ b <= c /\
    (0 < a \/ c < 0 ) ==>
    (abs a <= abs b /\ abs b <= abs c) \/
    (abs c <= abs b /\ abs b <= abs a)``,
  rpt strip_tac \\ fs[]
  >- (`0 < b` by RealArith.REAL_ASM_ARITH_TAC
      \\ `0 <= a /\ 0 <= b` by RealArith.REAL_ASM_ARITH_TAC
      \\ `abs a = a` by (fs[ABS_REFL])
      \\ `abs b = b` by (fs[ABS_REFL])
      \\ `0 <= c` by RealArith.REAL_ASM_ARITH_TAC
      \\ `abs c = c` by (fs[ABS_REFL])
      \\ fs[])
  >- (`~ (0 <= b)` by RealArith.REAL_ASM_ARITH_TAC
      \\ `~ (0 <= a)` by RealArith.REAL_ASM_ARITH_TAC
      \\ `~ (0 <= c)` by RealArith.REAL_ASM_ARITH_TAC
      \\ fs[realTheory.abs]));

fun assume_all l =
  case l of
      t :: ls => assume_tac t \\ assume_all ls
    | NIL =>  ALL_TAC;

val normal_enclosing = store_thm (
  "normal_enclosing",
  ``!v m vHi vLo.
      (0 < vLo \/ vHi < 0) /\
      normal vLo m /\
      normal vHi m /\
      vLo <= v /\ v <= vHi ==>
    normal v m``,
  rpt gen_tac
  \\ disch_then (fn thm => assume_all (CONJ_LIST 4  thm))
  \\ `(abs vLo <= abs v /\ abs v <= abs vHi) \/ (abs vHi <= abs v /\ abs v <= abs vLo)`
       by (irule enclosure_to_abs \\ fs[])
  \\ qpat_x_assum `0 < _ \/ _ < 0` kall_tac
  \\ fs[normal_def]
  \\ rveq
  \\ fs[]
  \\ RealArith.REAL_ASM_ARITH_TAC);

val solve_tac =
  rpt (qpat_x_assum `!x. _` kall_tac)
  \\ Cases_on `v = 0` \\ TRY(every_case_tac \\ fs[] \\ FAIL_TAC"")
  \\ Cases_on `denormal v m` \\ TRY (every_case_tac \\ fs[] \\ FAIL_TAC "")
  \\ Cases_on `normal v m` \\ TRY (every_case_tac \\ fs[] \\ FAIL_TAC"")
  \\ fs[normal_def, denormal_def, validValue_def] \\ rveq \\ fs[]
  \\ TRY (RealArith.REAL_ASM_ARITH_TAC)
  \\ fs []
  \\ every_case_tac  \\ fs[]
  \\ `abs v <= abs (FST (widenInterval (e_lo, e_hi) err_e)) \/
        abs v <= abs (SND (widenInterval (e_lo, e_hi) err_e))`
        by (fs[widenInterval_def, IVlo_def, IVhi_def] \\ RealArith.REAL_ASM_ARITH_TAC)
  \\ every_case_tac \\ RealArith.REAL_ASM_ARITH_TAC;

val FPRangeValidator_sound = store_thm (
  "FPRangeValidator_sound",
  ``!e E1 E2 Gamma v m A tMap P fVars (dVars:num_set).
      approxEnv E1 Gamma A fVars dVars E2 /\
      eval_exp E2 Gamma e v m /\
      typeCheck e Gamma tMap /\
      validIntervalbounds e A P dVars /\
      validErrorbound e tMap A dVars /\
      FPRangeValidator e A tMap dVars /\
      domain (usedVars e) DIFF (domain dVars) SUBSET (domain fVars) /\
      (!v. v IN domain fVars ==>
         ?vR. E1 v = SOME vR /\ FST (P v) <= vR /\ vR  <= SND (P v)) /\
      (!v. v IN domain fVars \/ v IN domain dVars ==>
         ?m. Gamma v = SOME m) /\
      (!v. v IN domain dVars ==>
         ?vR. E1 v = SOME vR /\ FST (FST (A (Var v))) <= vR /\ vR <= SND (FST (A (Var v)))) /\
      (!(v:num). v IN domain dVars ==>
        (?vF m. E2 v = SOME vF /\ tMap (Var v) = SOME m /\ validFloatValue vF m)) ==>
      validFloatValue v m``,
  once_rewrite_tac [FPRangeValidator_def] \\ rewrite_tac [GSYM normalOrZero_def]
  \\ rpt strip_tac
  \\`tMap e = SOME m`
      by (drule typingSoundnessExp
          \\ disch_then drule \\ fs[])
  \\ fs[]
  \\ Cases_on `A e` \\ rename1 `A e = (iv_e, err_e)` \\ fs[]
  \\ Cases_on `iv_e` \\ rename1 `A e = ((e_lo, e_hi), err_e)`
  \\ once_rewrite_tac [validFloatValue_def]
  \\ `?vR. eval_exp E1 (toRMap Gamma) (toREval e) vR M0 /\
        FST (FST (A e)) <= vR /\ vR <= SND (FST (A e))`
       by (drule validIntervalbounds_sound
           \\ disch_then (qspecl_then [`fVars`, `E1`, `Gamma`] impl_subgoal_tac)
           \\ fs[] \\ TRY (first_x_assum MATCH_ACCEPT_TAC)
           \\ qexists_tac `vR` \\ rw_asm_star `A _ = _`)
  \\ `abs (vR - v) <= SND (A e)`
       by (drule validErrorbound_sound
           \\ rpt (disch_then drule)
           \\ strip_tac
           \\ qpat_x_assum `eval_exp E2 Gamma e nF _` kall_tac
           \\ first_x_assum drule
           \\ rw_asm_star `A e = _`)
  \\ rw_asm_star `A e = _`
  \\ qspecl_then [`vR`, `v`, `err_e`, `(e_lo,e_hi)`]
       impl_subgoal_tac
       (SIMP_RULE std_ss [contained_def, widenInterval_def] distance_gives_iv)
  \\ fs[IVlo_def, IVhi_def]
  \\ Cases_on `e`
  >- (fs[]
      >- (fs[validFloatValue_def]
          \\ first_x_assum (qspecl_then [`n`] impl_subgoal_tac) \\ fs[domain_lookup]
          \\ `tMap (Var n)  = SOME m` by (drule typingSoundnessExp  \\ rpt (disch_then drule)
                                          \\ fs[])
          \\ qpat_x_assum `tMap (Var n) = SOME _` (fn thm => fs[thm])
          \\ inversion `eval_exp E2 _ _ _ _` eval_exp_cases
          \\ qpat_x_assum `E2 n = SOME v` (fn thm => fs[thm])
          \\ rveq  \\ fs[])
      \\ solve_tac)
  \\ solve_tac);

val FPRangeValidatorCmd_sound = store_thm (
  "FPRangeValidatorCmd_sound",
  ``!f E1 E2 Gamma v vR m A tMap P fVars dVars outVars.
      approxEnv E1 Gamma A fVars dVars E2 ∧
      ssa f (union fVars dVars) outVars /\
      bstep (toREvalCmd f) E1 (toRMap Gamma) vR m /\
      bstep f E2 Gamma v m ∧
      typeCheckCmd f Gamma tMap ∧
      validIntervalboundsCmd f A P dVars ∧
      validErrorboundCmd f tMap A dVars ∧
      FPRangeValidatorCmd f A tMap dVars ∧
      domain (freeVars f) DIFF domain dVars ⊆ domain fVars ∧
      (∀v.
        v ∈ domain fVars ⇒
        ∃vR. E1 v = SOME vR ∧ FST (P v) ≤ vR ∧ vR ≤ SND (P v)) ∧
      (∀v. v ∈ domain fVars ∨ v ∈ domain dVars ⇒ ∃m. Gamma v = SOME m) ∧
      (∀v.
        v ∈ domain dVars ⇒
        ∃vR.
          E1 v = SOME vR ∧ FST (FST (A (Var v))) ≤ vR ∧
          vR ≤ SND (FST (A (Var v)))) ∧
      (∀v.
        v ∈ domain dVars ⇒
        ∃vF m.
          E2 v = SOME vF ∧ tMap (Var v) = SOME m ∧
          validFloatValue vF m) ⇒
      validFloatValue v m``,
  Induct
  \\ simp[Once toREvalCmd_def, Once validIntervalboundsCmd_def,
          Once validErrorboundCmd_def, Once FPRangeValidatorCmd_def,
          Once typeCheckCmd_def, Once freeVars_def, FPRangeValidatorCmd_def]
  \\ rpt strip_tac
  >- (rpt (inversion `bstep (Let _ _ _ _) _ _ _ _` bstep_cases)
      \\ rename1 `bstep (toREvalCmd f) (updEnv n vR_e E1) _ _ mR`
      \\ rename1 `bstep f (updEnv n vF E2) _ _ mF`
      \\ `tMap e = SOME m`
            by (drule typingSoundnessExp
                \\ rpt (disch_then drule)
                \\ fs[])
      \\ fs[]
      \\ inversion `ssa _ _ _` ssa_cases
      \\ drule validErrorbound_sound
      \\ disch_then drule
      \\ disch_then (qspecl_then [`vR_e`, `SND (A e)`, `P`, `FST(FST (A e))`, `SND(FST (A e))`] impl_subgoal_tac)
      >- (fs[] \\ conj_tac \\ TRY (first_x_assum MATCH_ACCEPT_TAC)
          \\ fs[DIFF_DEF, SUBSET_DEF] \\ rpt strip_tac \\ first_x_assum irule
          \\ fs[domain_union]
          \\ CCONTR_TAC \\ fs[] \\ rveq
          \\ first_x_assum (qspec_then `n` assume_tac)
          \\ res_tac)
      \\ fs[]
      \\ drule validIntervalbounds_sound
      \\ rpt (disch_then drule)
      \\ disch_then (qspecl_then [`fVars`, `Gamma`] impl_subgoal_tac)
      >- (fs[] \\ conj_tac \\ TRY (first_x_assum MATCH_ACCEPT_TAC)
          \\ fs[DIFF_DEF, SUBSET_DEF] \\ rpt strip_tac \\ first_x_assum irule
          \\ fs[domain_union]
          \\ CCONTR_TAC \\ fs[] \\ rveq
          \\ first_x_assum (qspec_then `n` assume_tac)
          \\ res_tac)
      \\ Cases_on `tMap (Var n)` \\ fs[]
      \\ `vR_e = vR'` by metis_tac[meps_0_deterministic]
      \\ rveq
      \\ rename1 `vR_e <= SND (FST _)`
      \\ first_x_assum
           (qspecl_then [`updEnv n vR_e E1`, `updEnv n vF E2`,
                         `updDefVars n m Gamma`, `v`, `vR`, `mF`, `A`, `tMap`, `P`,
                         `fVars`, `insert n () dVars`, `outVars`]
              impl_subgoal_tac)
       >- (fs[] \\ rpt conj_tac
           >- (irule approxUpdBound \\ fs[lookup_NONE_domain]
               \\ qpat_x_assum `A e = A (Var n)` (fn thm => once_rewrite_tac [GSYM thm])
               \\ first_x_assum (qspecl_then [`vF`, `m`] irule)
               \\ qexists_tac `m` \\ fs[])
           >- (irule ssa_equal_set
               \\ qexists_tac `insert n () (union fVars dVars)`
               \\ conj_tac \\ TRY (fs[] \\ FAIL_TAC "")
               \\ rewrite_tac [domain_union, domain_insert]
               \\ rewrite_tac [UNION_DEF, INSERT_DEF]
               \\ fs[EXTENSION]
               \\ rpt strip_tac
               \\ metis_tac[])
           >- (irule swap_Gamma_bstep
               \\ qexists_tac `updDefVars n M0 (toRMap Gamma)` \\ fs[]
               \\ MATCH_ACCEPT_TAC Rmap_updVars_comm)
           >- (fs[DIFF_DEF, domain_insert, SUBSET_DEF]
               \\ rpt strip_tac \\ first_x_assum irule
               \\ simp[Once freeVars_def]
               \\ once_rewrite_tac [domain_union]
               \\ fs[]
               \\ rw_thm_asm `x IN domain (freeVars f)` freeVars_def
               \\ fs[])
           >- (rpt strip_tac \\ simp[updEnv_def]
               \\ IF_CASES_TAC \\ fs[]
               \\ rveq
               \\ fs[domain_union])
           >- (rpt strip_tac \\ fs[updDefVars_def]
               \\ TOP_CASE_TAC \\ fs[])
           >- (rpt gen_tac \\ disch_then assume_tac
               \\ fs[updEnv_def] \\ rveq \\ fs[]
               >- (qpat_x_assum `A e = A (Var n)` (fn thm => fs[GSYM thm]))
               \\ TOP_CASE_TAC \\ rveq \\  fs[]
               \\ qpat_x_assum `A e = A (Var n)` (fn thm => fs[GSYM thm]))
           \\ rpt strip_tac
           \\ fs[updEnv_def] \\ rveq \\ fs[]
           >- (qpat_x_assum `eval_exp E2 Gamma e nF _` kall_tac
               \\ drule FPRangeValidator_sound
               \\ rpt (disch_then drule)
               \\ disch_then irule \\ fs[]
               \\ fs[DIFF_DEF, domain_insert, SUBSET_DEF]
               \\ rpt strip_tac \\ first_x_assum irule
               \\ simp[Once freeVars_def]
               \\ once_rewrite_tac [domain_union]
               \\ fs[]
               \\ CCONTR_TAC \\ fs[] \\ rveq
               \\ first_x_assum (qspec_then `n` assume_tac)
               \\ res_tac)
           \\ TOP_CASE_TAC \\ fs[]
           \\ qpat_x_assum `eval_exp E2 Gamma e nF _` kall_tac
           \\ drule FPRangeValidator_sound
           \\ rpt (disch_then drule)
           \\ disch_then irule \\ fs[]
           \\ fs[DIFF_DEF, domain_insert, SUBSET_DEF]
           \\ rpt strip_tac \\ first_x_assum irule
           \\ simp[Once freeVars_def]
           \\ once_rewrite_tac [domain_union]
           \\ fs[]
           \\ CCONTR_TAC \\ fs[] \\ rveq
           \\ first_x_assum (qspec_then `n` assume_tac)
           \\ res_tac)
       \\ fs[])
  \\ rpt (inversion `bstep (Ret _) _ _ _ _` bstep_cases)
  \\ drule FPRangeValidator_sound
  \\ rpt (disch_then drule)
  \\ fs[]);

val _ = export_theory();
