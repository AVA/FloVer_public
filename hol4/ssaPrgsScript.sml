(**
  We define a pseudo SSA predicate.
  The formalization is similar to the renamedApart property in the LVC framework by Schneider, Smolka and Hack
  http://dblp.org/rec/conf/itp/SchneiderSH15
  Our predicate is not as fully fledged as theirs, but we especially borrow the idea of annotating
  the program with the predicate with the set of free and defined variables
**)
open preamble
open pred_setSyntax pred_setTheory
open AbbrevsTheory ExpressionsTheory ExpressionAbbrevsTheory FloverTactics CommandsTheory MachineTypeTheory

val _ = new_theory "ssaPrgs";

val validVars_add = store_thm("validVars_add",
``!(e:'a exp) Vs n.
     domain (usedVars e) ⊆ domain Vs ==>
     domain (usedVars e) ⊆ domain (insert n () Vs)``,
fs [domain_insert, SUBSET_DEF]);

(*
val validVars_non_stuck = store_thm ("validVars_non_stuck",
  ``!(e:real exp) inVars E.
      domain (usedVars e) ⊆ inVars /\
      (!v. v IN (domain (usedVars e)) ==>
         ?r. E v = SOME r) ==>
        ? vR. eval_exp 0 E e vR``,
  Induct_on `e` \\ once_rewrite_tac[usedVars_def]
  \\ rpt strip_tac \\ fs []
  >- (qexists_tac `r` \\ fs[eval_exp_cases])
  >- (qexists_tac `v` \\ fs[eval_exp_cases]
      \\ qexists_tac `0`
      \\ fs[perturb_def])
  >- (`?vR. eval_exp 0 E e vR`
        by (first_x_assum match_mp_tac
            \\ qexists_tac `inVars`
            \\ fs [])
      \\ qexists_tac `evalUnop u vR`
      \\ fs[eval_exp_cases]
      \\ Cases_on `u`
      >- (disj1_tac \\ qexists_tac `vR`
          \\ fs [])
      >- (disj2_tac \\ qexistsl_tac [`vR`, `0`]
          \\ fs [perturb_def]))
  >- (`?vR1. eval_exp 0 E e vR1`
       by (first_x_assum match_mp_tac
           \\ qexists_tac `inVars`
           \\ fs [domain_union])
       \\ `?vR2. eval_exp 0 E e' vR2`
         by (first_x_assum match_mp_tac
           \\ qexists_tac `inVars`
           \\ fs [domain_union])
      \\ qexists_tac `evalBinop b vR1 vR2`
      \\ Cases_on `b` \\ fs[eval_exp_cases]
      \\ qexistsl_tac [`vR1`, `vR2`, `0`]
      \\ fs[perturb_def]));
*)

(**
  Inductive ssa predicate, following the definitions from the LVC framework,
  see top of file for citation
  We maintain as an invariant that if we have
  ssa f
**)
val (ssa_rules, ssa_ind, ssa_cases) = Hol_reln `
  (!m x e s inVars (Vterm:num_set).
     (domain (usedVars e)) SUBSET (domain inVars) /\
     (~ (x IN (domain inVars)))  /\
     ssa s (insert x () inVars) Vterm ==>
     ssa (Let m x e s) inVars Vterm) /\
  (!e inVars Vterm.
     (domain (usedVars e)) SUBSET (domain inVars) /\
     (domain inVars = domain Vterm) ==>
     ssa (Ret e) inVars Vterm)`;

(*
  As usual
*)
val ssa_cases =
  map (GEN_ALL o SIMP_CONV (srw_ss()) [Once ssa_cases])
    [``ssa (Let m x e s) inVars Vterm``,
     ``ssa (Ret e) inVars Vterm``]
  |> LIST_CONJ |> curry save_thm "ssa_cases";

val [ssaLet, ssaRet] = CONJ_LIST 2 ssa_rules;
save_thm ("ssaLet",ssaLet);
save_thm ("ssaRet",ssaRet);

val ssa_subset_freeVars = store_thm ("ssa_subset_freeVars",
  ``!(f:'a cmd) inVars outVars.
      ssa f inVars outVars ==>
      (domain (freeVars f)) SUBSET (domain inVars)``,
  ho_match_mp_tac ssa_ind \\ rw []
  >- (once_rewrite_tac [freeVars_def, domain_insert] \\ simp []
      \\ once_rewrite_tac [domain_union]
      \\ fs[SUBSET_DEF]
      \\ rpt strip_tac
      >- (first_x_assum match_mp_tac
          \\ simp [])
      >- (fs [ domain_insert]
          \\ metis_tac[]))
  >- (once_rewrite_tac[freeVars_def]
      \\ fs []));

val ssa_equal_set_ind = prove(
  ``!(f:'a cmd) inVars outVars.
       ssa f inVars outVars ==>
        (!inVars'.
           (domain inVars' = domain inVars) ==>
           ssa f inVars' outVars)``,
  qspec_then `\f inVars outVars.
    ∀inVars'. (domain inVars' = domain inVars) ==> ssa f inVars' outVars` (fn thm => assume_tac (SIMP_RULE std_ss [] thm)) ssa_ind
  \\ first_x_assum match_mp_tac
  \\ conj_tac \\ rw[]
  >- (match_mp_tac ssaLet \\ fs[]
      \\ first_x_assum match_mp_tac
      \\ simp[ domain_insert])
  >- (match_mp_tac ssaRet \\ fs[]));

val ssa_equal_set = store_thm ("ssa_equal_set",
  ``!(f:'a cmd) inVars outVars inVars'.
       (domain inVars' = domain inVars) /\
       ssa f inVars outVars ==>
       ssa f inVars' outVars``,
  rpt strip_tac
  \\ qspecl_then [`f`, `inVars`, `outVars`] assume_tac ssa_equal_set_ind
  \\ specialize `ssa _ _ _ ==> _` `ssa f inVars outVars`
  \\ specialize `!inVars'. A ==> B` `inVars'`
  \\ first_x_assum match_mp_tac \\ fs[]);

val validSSA_def = Define `
  validSSA (f:real cmd) (inVars:num_set) =
    case f of
      |Let m x e g =>
        ((lookup x inVars = NONE) /\
         (subspt (usedVars e) inVars) /\
         (validSSA g (insert x () inVars)))
      |Ret e =>
       (subspt (usedVars e) inVars)`;

val validSSA_sound = store_thm ("validSSA_sound",
  ``!(f:real cmd) (inVars:num_set).
       (validSSA f inVars) ==>
       ?outVars.
         ssa f inVars outVars``,
  Induct \\ once_rewrite_tac [validSSA_def] \\ rw[]
  >- (specialize `!inVars. _ ==> _` `insert n () inVars`
      \\ `?outVars. ssa f (insert n () inVars) outVars` by (first_x_assum match_mp_tac \\ simp[])
      \\ qexists_tac `outVars`
      \\ match_mp_tac ssaLet
      \\ fs [subspt_def, SUBSET_DEF, lookup_NONE_domain])
  >- (exists_tac ``inVars:num_set``
      \\ match_mp_tac ssaRet
      \\ fs[subspt_def, SUBSET_DEF]));

val ssa_shadowing_free = store_thm ("ssa_shadowing_free",
  ``!m x y v v' e f inVars outVars E defVars.
      ssa (Let m x (toREval e) (toREvalCmd f)) inVars outVars /\
      (y IN (domain inVars)) /\
      eval_exp E defVars (toREval e) v M0 ==>
      !E n. updEnv x v (updEnv y v' E) n = updEnv y v' (updEnv x v E) n``,
  rpt strip_tac
  \\ inversion `ssa (Let m x (toREval e) (toREvalCmd f)) _ _` ssa_cases
  \\ fs[updEnv_def]
  \\ Cases_on `n = x` \\ rveq
  >- (simp[]
      \\ Cases_on `n = y` \\ rveq \\ rw[]
      \\ metis_tac[])
  >- (simp[]));

(* val shadowing_free_rewriting_exp = store_thm ("shadowing_free_rewriting_exp", *)
(*   ``!e v E1 E2 defVars. *)
(*       (!n. E1 n = E2 n) ==> *)
(*       (eval_exp E1 defVars (toREval e) v M0 <=> *)
(*       eval_exp E2 defVars (toREval e) v M0)``, *)
(*   Induct \\ rpt strip_tac \\ fs[eval_exp_cases, EQ_IMP_THM] \\ metis_tac[]); *)

(* val shadowing_free_rewriting_cmd = store_thm ("shadowing_free_rewriting_cmd", *)
(*   ``!f E1 E2 vR defVars. *)
(*       (!n. E1 n = E2 n) ==> *)
(*       (bstep (toREvalCmd f) E1 defVars vR M0 <=> *)
(*        bstep (toREvalCmd f) E2 defVars vR M0)``, *)
(*   Induct \\ rpt strip_tac \\ fs[EQ_IMP_THM] \\ metis_tac[]); *)

(* val dummy_bind_ok = store_thm ("dummy_bind_ok", *)
(*   ``!e v x v' (inVars:num_set) E defVars. *)
(*       (domain (usedVars e)) SUBSET (domain inVars) /\ *)
(*       (~ (x IN (domain inVars))) /\ *)
(*       eval_exp E defVars (toREval e) v M0 ==> *)
(*       eval_exp (updEnv x v' E) defVars (toREval e) v M0``, *)
(*   Induct \\ rpt strip_tac \\ once_rewrite_tac [toREval_def] \\ qpat_x_assum `eval_exp _ _ (toREval _) _ _` (fn thm => assume_tac (ONCE_REWRITE_RULE [toREval_def] thm)) \\  fs [] \\ rveq \\ inversion `eval_exp _ _ _ _ _` eval_exp_cases *)
(*   >- (match_mp_tac Var_load *)
(*       \\ fs[usedVars_def, updEnv_def] *)
(*       \\ Cases_on `n = x` \\ rveq \\ fs[]) *)
(*   >- (qpat_x_assum `v' = perturb _ _` (fn thm => once_rewrite_tac [thm]) *)
(*       \\ match_mp_tac Const_dist \\ simp[]) *)
(*   >- (Cases_on `u` \\ rveq \\ fs[updEnv_def] *)
(*       >- (match_mp_tac Unop_neg *)
(*           \\ first_x_assum match_mp_tac *)
(*           \\ qexists_tac `inVars` *)
(*           \\ qpat_x_assum `domain _ SUBSET _` (fn thm => assume_tac (ONCE_REWRITE_RULE [usedVars_def] thm)) *)
(*           \\ fs[]) *)
(*       >- (match_mp_tac Unop_inv \\ fs[] *)
(*           \\ first_x_assum match_mp_tac *)
(*           \\ qexists_tac `inVars` *)
(*           \\ qpat_x_assum `domain _ SUBSET _` (fn thm => assume_tac (ONCE_REWRITE_RULE [usedVars_def] thm)) *)
(*           \\ fs[])) *)
(*   >- (`(m1 = M0) /\ (m2 = M0)` by (match_mp_tac ifJoinIs0 \\ fs[]) *)
(*       \\ rveq *)
(*       \\ qpat_assum `M0 = _` (fn thm => once_rewrite_tac [thm]) *)
(*       \\ match_mp_tac Binop_dist \\ fs[] *)
(*       \\ qpat_x_assum `domain _ SUBSET _` (fn thm => assume_tac (ONCE_REWRITE_RULE [usedVars_def] thm)) *)
(*       \\ conj_tac \\ first_x_assum match_mp_tac \\ qexists_tac `inVars` \\ fs[domain_union]) *)
(*   >- (qpat_x_assum `domain _ SUBSET _` (fn thm => assume_tac (ONCE_REWRITE_RULE [usedVars_def] thm)) *)
(*       \\ fs [] \\ first_x_assum match_mp_tac \\ qexists_tac `inVars` \\ fs[])); *)

(* val exp_subst_def = Define ` *)
(*   exp_subst (e:real exp) x e_new = *)
(*     case e of *)
(*       |Var v => if v = x then e_new else Var v *)
(*       |Const m v => Const m v *)
(*       |Unop u e => Unop u (exp_subst e x e_new) *)
(*       |Binop b e1 e2 => Binop b (exp_subst e1 x e_new) (exp_subst e2 x e_new) *)
(*       |Downcast m e1 => Downcast m (exp_subst e1 x e_new)`; *)

(* val exp_subst_correct = store_thm ("exp_subst_correct", *)
(*   ``!e e_sub E x v v_res. *)
(*       eval_exp 0 E e_sub v ==> *)
(*       (eval_exp 0 (updEnv x v E) e v_res <=> *)
(*        eval_exp 0 E (exp_subst e x e_sub) v_res)``, *)
(*   Induct \\ rpt strip_tac \\ once_rewrite_tac [EQ_IMP_THM] *)
(*   >- (conj_tac \\ strip_tac \\ inversion `eval_exp _ _ _ _` eval_exp_cases *)
(*       \\ fs [exp_subst_def, updEnv_def] \\ Cases_on `n = x` \\ rveq \\ fs[] *)
(*       >- (match_mp_tac Var_load \\ fs []) *)
(*       >- (match_mp_tac Var_load \\ fs [updEnv_def] *)
(*           \\ match_mp_tac meps_0_deterministic \\ asm_exists_tac \\ simp[]) *)
(*       >- (match_mp_tac Var_load \\ fs[updEnv_def] *)
(*           \\ inversion `eval_exp 0 E (Var n) _` eval_exp_cases \\ fs[])) *)
(*   >- (conj_tac \\ strip_tac \\ inversion `eval_exp _ _ _ _` eval_exp_cases *)
(*       \\ fs [exp_subst_def, updEnv_def] *)
(*       >- (metis_tac [eval_exp_rules]) *)
(*       >- (inversion `eval_exp _ _ (Const v) _` eval_exp_cases \\ rveq *)
(*           \\ match_mp_tac Const_dist \\ fs[])) *)
(*   >- (conj_tac \\ once_rewrite_tac [exp_subst_def, updEnv_def] \\ strip_tac *)
(*       \\ inversion `eval_exp _ _ _ _` eval_exp_cases \\ rveq \\ fs [] *)
(*       >- (`eval_exp 0 (updEnv x v E) e v1 <=> eval_exp 0 E (exp_subst e x e_sub) v1` *)
(*             by (first_x_assum match_mp_tac \\ fs[]) *)
(*           \\ match_mp_tac Unop_neg *)
(*           \\ qpat_x_assum `eval_exp _ _ _ _ <=> eval_exp _ _ _ _` (fn thm => once_rewrite_tac [GSYM thm]) *)
(*           \\ fs[]) *)
(*       >- (`eval_exp 0 (updEnv x v E) e v1 <=> eval_exp 0 E (exp_subst e x e_sub) v1` *)
(*             by (first_x_assum match_mp_tac \\ fs[]) *)
(*           \\ match_mp_tac Unop_inv *)
(*           \\ qpat_x_assum `eval_exp _ _ _ _ <=> eval_exp _ _ _ _` (fn thm => once_rewrite_tac [GSYM thm]) *)
(*           \\ fs[]) *)
(*       >- (inversion `eval_exp 0 E (Unop u _) _` eval_exp_cases *)
(*           \\ fs[] *)
(*           >- (`eval_exp 0 (updEnv x v E) e v1 <=> eval_exp 0 E (exp_subst e x e_sub) v1` *)
(*                 by (first_x_assum match_mp_tac \\ fs[]) *)
(*               \\ match_mp_tac Unop_neg *)
(*               \\ qpat_x_assum `eval_exp _ _ _ _ <=> eval_exp _ _ _ _` (fn thm => once_rewrite_tac [thm]) *)
(*               \\ fs[]) *)
(*           >- (`eval_exp 0 (updEnv x v E) e v1 <=> eval_exp 0 E (exp_subst e x e_sub) v1` *)
(*                 by (first_x_assum match_mp_tac \\ fs[]) *)
(*               \\ match_mp_tac Unop_inv *)
(*               \\ qpat_x_assum `eval_exp _ _ _ _ <=> eval_exp _ _ _ _` (fn thm => once_rewrite_tac [thm]) *)
(*               \\ fs[]))) *)
(*   >- (rename1 `Binop b e1 e2` *)
(*       \\ conj_tac \\ once_rewrite_tac [updEnv_def, exp_subst_def] \\ simp[] *)
(*       \\ strip_tac \\ inversion `eval_exp _ _ _ _` eval_exp_cases \\ rveq *)
(*       >- (`eval_exp 0 (updEnv x v E) e1 v1 ⇔ eval_exp 0 E (exp_subst e1 x e_sub) v1` *)
(*             by (first_x_assum match_mp_tac \\ fs[]) *)
(*           \\ `eval_exp 0 (updEnv x v E) e2 v2 ⇔ eval_exp 0 E (exp_subst e2 x e_sub) v2` *)
(*             by (first_x_assum match_mp_tac \\ fs[]) *)
(*           \\ match_mp_tac Binop_dist *)
(*           \\ fs[]) *)
(*       >- (`eval_exp 0 (updEnv x v E) e1 v1 ⇔ eval_exp 0 E (exp_subst e1 x e_sub) v1` *)
(*             by (first_x_assum match_mp_tac \\ fs[]) *)
(*           \\ `eval_exp 0 (updEnv x v E) e2 v2 ⇔ eval_exp 0 E (exp_subst e2 x e_sub) v2` *)
(*             by (first_x_assum match_mp_tac \\ fs[]) *)
(*           \\ match_mp_tac Binop_dist *)
(*           \\ fs[]))); *)


(* val map_subst_def = Define ` *)
(*   map_subst (f:real cmd) x e = *)
(*     case f of *)
(*       |Let y e_y g => Let y (exp_subst e_y x e) (map_subst g x e) *)
(*       |Ret e_r  => Ret (exp_subst e_r x e)`; *)

(* val stepwise_substitution = store_thm ("stepwise_substitution", *)
(*   ``!x e v f E vR inVars outVars. *)
(*       ssa f inVars outVars /\ *)
(*       x IN (domain inVars) /\ *)
(*       (domain (usedVars e)) SUBSET (domain inVars) /\ *)
(*       eval_exp 0 E e v ==> *)
(*       (bstep f (updEnv x v E) 0 vR <=> *)
(*        bstep (map_subst f x e) E 0 vR)``, *)
(*   Induct_on `f` *)
(*   \\ once_rewrite_tac [map_subst_def] \\ rpt strip_tac *)
(*   \\ once_rewrite_tac [EQ_IMP_THM] \\ simp [] *)
(*   \\ inversion `ssa _ _ _` ssa_cases *)
(*   \\ conj_tac \\ strip_tac \\ inversion `bstep _ _ _ _` bstep_cases *)
(*   >- (match_mp_tac let_b *)
(*       \\ qexists_tac `v'` *)
(*       \\ conj_tac *)
(*       >- (`eval_exp 0 (updEnv x v E) e v' <=> eval_exp 0 E (exp_subst e x e') v'` *)
(*             by (match_mp_tac exp_subst_correct \\ fs[]) *)
(*           \\ fs []) *)
(*       >- (`bstep f (updEnv x v (updEnv n v' E)) 0 vR <=> bstep (map_subst f x e') (updEnv n v' E) 0 vR` *)
(*             by (first_x_assum match_mp_tac \\ qexistsl_tac [`insert n () inVars`, `outVars`] *)
(*                 \\ fs[ domain_insert] \\ conj_tac *)
(*                 >- (match_mp_tac SUBSET_TRANS \\ qexists_tac `domain inVars` *)
(*                     \\ fs []) *)
(*                 >- (match_mp_tac dummy_bind_ok \\ asm_exists_tac \\ fs[])) *)
(*           \\ first_x_assum (fn thm => once_rewrite_tac [GSYM thm]) *)
(*           \\ `bstep f (updEnv n v' (updEnv x v E)) 0 vR <=> *)
(*               bstep f (updEnv x v (updEnv n v' E)) 0 vR` *)
(*                by (match_mp_tac shadowing_free_rewriting_cmd \\ strip_tac \\ fs[updEnv_def] *)
(*                    \\ Cases_on `n' = n` \\ Cases_on `n' = x` \\ fs[]) *)
(*           \\ rw_sym_asm `bstep _ _ _ _ <=> _` *)
(*           \\ fs[])) *)
(*   >- (match_mp_tac let_b *)
(*       \\ qexists_tac `v'` *)
(*       \\ conj_tac *)
(*       >- (`eval_exp 0 (updEnv x v E) e v' <=> eval_exp 0 E (exp_subst e x e') v'` *)
(*             by (match_mp_tac exp_subst_correct \\ fs[]) *)
(*           \\ fs []) *)
(*       >- (`bstep f (updEnv x v (updEnv n v' E)) 0 vR <=> bstep (map_subst f x e') (updEnv n v' E) 0 vR` *)
(*             by (first_x_assum match_mp_tac \\ qexistsl_tac [`insert n () inVars`, `outVars`] *)
(*                 \\ fs[domain_insert] \\ conj_tac *)
(*                 >- (match_mp_tac SUBSET_TRANS \\ qexists_tac `domain inVars` *)
(*                     \\ fs []) *)
(*                 >- (match_mp_tac dummy_bind_ok \\ asm_exists_tac \\ fs[])) *)
(*           \\ `bstep f (updEnv n v' (updEnv x v E)) 0 vR <=> *)
(*               bstep f (updEnv x v (updEnv n v' E)) 0 vR` *)
(*                by (match_mp_tac shadowing_free_rewriting_cmd \\ strip_tac \\ fs[updEnv_def] *)
(*                    \\ Cases_on `n' = n` \\ Cases_on `n' = x` \\ fs[]) *)
(*           \\ rw_asm `bstep _ _ _ _ <=> bstep f (updEnv _ _ _) _ _` *)
(*           \\ first_x_assum (fn thm => once_rewrite_tac [thm]) *)
(*           \\ fs[])) *)
(*   >- (match_mp_tac ret_b *)
(*       \\ `eval_exp 0 (updEnv x v E) e vR <=> eval_exp 0 E (exp_subst e x e') vR` *)
(*            by (match_mp_tac exp_subst_correct \\ fs[]) *)
(*       \\ rw_sym_asm `eval_exp _ _ _ _ <=> eval_exp _ _ _ _` *)
(*       \\ fs[]) *)
(*   >- (match_mp_tac ret_b *)
(*       \\ `eval_exp 0 (updEnv x v E) e vR <=> eval_exp 0 E (exp_subst e x e') vR` *)
(*            by (match_mp_tac exp_subst_correct \\ fs[]) *)
(*       \\ rw_asm `eval_exp _ _ _ _ <=> eval_exp _ _ _ _` *)
(*       \\ fs[])); *)

(* val let_subst_def = Define ` *)
(*   let_subst (f:real cmd) :real exp option= *)
(*     case f of *)
(*       |Let x e g => *)
(*         (case (let_subst g) of *)
(*           |SOME e' => SOME (exp_subst e' x e) *)
(*           |NONE => NONE) *)
(*       |Ret e => SOME e`; *)

(* val let_free_form = store_thm ("let_free_form", *)
(*   ``!(f:real cmd) (E:env) (vR:real) inVars outVars e. *)
(*        ssa f inVars outVars /\ *)
(*        bstep f E 0 vR /\ *)
(*        (let_subst f = SOME e) ==> *)
(*        bstep (Ret e) E 0 vR``, *)
(* cheat *)
(* ); *)

(*
Theorem let_free_form f E vR inVars outVars e:
  ssa f inVars outVars ->
  bstep (toRCmd f) E 0 vR ->
  let_subst f = Some e ->
  bstep (toRCmd (Ret e)) E 0 vR.
Proof.
  revert E vR inVars outVars e;
    induction f;
    intros E vR inVars outVars e_subst ssa_f bstep_f subst_step.
  - simpl.
    inversion bstep_f; subst.
    inversion ssa_f; subst.
    simpl in subst_step.
    case_eq (let_subst f).
    + intros f_subst subst_f_eq.
      specialize (IHf (updEnv n v E) vR (NatSet.add n inVars) outVars f_subst H8 H6 subst_f_eq).
      rewrite subst_f_eq in subst_step.
      inversion IHf; subst.
      constructor.
      inversion subst_step.
      subst.
      rewrite <- exp_subst_correct; eauto.
    + intros subst_eq; rewrite subst_eq in subst_step; inversion subst_step.
  - inversion bstep_f; subst.
    constructor.
    simpl in *.
    inversion subst_step; subst.
    assumption.
Qed.*)


val _ = export_theory ();
